<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstudianteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('estudiantes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned()->unique();
            $table->foreign('persona_id')->references('id')->on('personas');
            
            $table->String('codigo_sis')->unique();

            /*$table->integer('sesion_id')->unsigned();
            $table->foreign('sesion_id')->references('id')->on('sesions');
            */
            /*$table->integer('grupomateria_id')->unsigned();
            $table->foreign('grupomateria_id')->references('id')->on('grupo_materias');*/
            $table->timestamps();
        });
         
        Schema::create('portafolios', function (Blueprint $table) {
            $table->integer('estudiante_id')->unsigned()->primary();
            $table->foreign('estudiante_id')->references('id')->on('estudiantes');
            $table->String('direccion_portafolio');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudiantes');
        Schema::dropIfExists('portafolios');
    }
}
