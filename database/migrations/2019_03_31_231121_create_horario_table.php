<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema:: create('horarios',function(Blueprint $table){
          $table->increments('id');
          $table->enum('dia',['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado']);
          $table->dateTime('hora_entrada');
          $table->dateTime('hora_salida');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horarios');
    }
}
