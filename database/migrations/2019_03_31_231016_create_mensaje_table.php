<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMensajeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema:: create('mensajes',function(Blueprint $table){
          $table->increments('id');
          $table->string('icono')->nullable();
          $table->string('nombre')->unique();
          $table->string('codigo')->nullable();
          $table->longText('descripcion')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('mensajes');
    }
}
