<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSesionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
         Schema::create('sesions', function (Blueprint $table) {
            $table->increments('id');
           
            $table->integer('horario_id')->unsigned();
            $table->foreign('horario_id')->references('id')->on('horarios')->onDelete('cascade');
            $table->date('fecha');
            $table->integer('numero_sesion');
            $table->integer('cupo');
           
            $table->integer('grupomateria_id')->unsigned();
            $table->foreign('grupomateria_id')->references('id')->on('grupo_materias');

            $table->integer('auxiliar_id')->unsigned();
            $table->foreign('auxiliar_id')->references('id')->on('auxiliars');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sesions');

    }
}
