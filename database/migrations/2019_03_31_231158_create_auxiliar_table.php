<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuxiliarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('auxiliars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned()->unique();
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->integer('codigo_sis')->unique();
            $table->boolean('habilitado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auxiliars');
    }
}

