<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSesionProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revision_auxs', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('observacion');
            $table->longText('avances');
            $table->boolean('asistio');
            $table->integer('auxiliar_id')->unsigned();
            $table->foreign('auxiliar_id')->references('id')->on('auxiliars'); 
            $table->timestamps();
        });
        
        Schema::create('sesion_productos', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('portafolio_id')->unsigned();
            $table->foreign('portafolio_id')->references('estudiante_id')->on('portafolios');
            
            $table->integer('sesion_id')->unsigned();
            $table->foreign('sesion_id')->references('id')->on('sesions');
            
            $table->integer('revisionauxiliar_id')->unsigned()->nullable();
            $table->foreign('revisionauxiliar_id')->references('id')->on('revision_auxs');
            
            $table->timestamps();
        });
        
        Schema::create('productos', function (Blueprint $table) {
            $table->dateTime('hora_publicacion')->nullable();
            $table->String('referencia')->nullable();
            $table->integer('sesionproducto_id')->unsigned()->primary();
            $table->foreign('sesionproducto_id')->references('id')->on('sesion_productos');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('revision_auxs');
       Schema::dropIfExists('productos');
       Schema::dropIfExists('sesion_productos');

    }
}
