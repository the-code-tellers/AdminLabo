<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema:: create('gestions',function(Blueprint $table){
          $table->increments('id');
          $table->smallInteger('numero_gestion');
          $table->integer('anho');
          $table->date('fecha_inicio');
          $table->date('fecha_fin');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gestions');
    }
}
