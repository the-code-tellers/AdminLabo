<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevisionDocenteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revision_docentes', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('observacion')->nullable();
            $table->longText('avances')->nullable();
            $table->integer('sesionproducto_id')->unsigned();
            $table->foreign('sesionproducto_id')->references('id')->on('sesion_productos');
            $table->integer('docente_id')->unsigned();
            $table->foreign('docente_id')->references('id')->on('docentes'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revision_docentes');
    }
}
