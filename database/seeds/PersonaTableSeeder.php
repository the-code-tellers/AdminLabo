<?php

use Illuminate\Database\Seeder;

class PersonaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//DB::table('personas')->truncate();
        factory(App\Persona::class,10)->create()->each(function ($persona) {
              if(App\Persona::first()->id > ($persona->id) - 1) {                
                $persona->administrador()->save(factory(App\Administrador::class)->make());   
                $persona->correo = "admin@admin.com";
                $persona->save();
              } else if(App\Persona::first()->id > ($persona->id) - 4) {
                $persona->docente()->save(factory(App\Docente::class)->make()); 
              } else if(App\Persona::first()->id > ($persona->id) - 7) {
                $persona->auxiliar()->save(factory(App\Auxiliar::class)->make()); 
                $persona->auxiliar->grupoMaterias()->save(factory(App\GrupoMateria::class)->make());
                $persona->auxiliar->sesiones()->save(factory(App\Sesion::class)->make());
              } else if(App\Persona::first()->id > ($persona->id) - 10) {
                $persona->estudiante()->save(factory(App\Estudiante::class)->make()); 
                $persona->estudiante->portafolio()->save(factory(App\Portafolio::class)->make());
              }
            });
    }
}
