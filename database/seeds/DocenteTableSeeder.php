<?php

use Illuminate\Database\Seeder;

class DocenteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//DB::table('docentes')->truncate();
        factory(App\Docente::class,2)->create()->each(function ($docente) {
                $docente->persona()->save(factory(App\Persona::class)->make());
            });
    }
}
