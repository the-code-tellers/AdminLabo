<?php

use Illuminate\Database\Seeder;

class EstudianteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//DB::table('estudiantes')->truncate();
        factory(App\Estudiante::class,3)->create()->each(function ($estudiante) {
                $estudiante->persona()->save(factory(App\Persona::class)->make());
                //$estudiante->portafolio()->save(factory(App\Portafolio::class)->make());
            });
    } 
}
