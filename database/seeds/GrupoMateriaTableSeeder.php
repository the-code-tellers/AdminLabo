<?php

use Illuminate\Database\Seeder;

class GrupoMateriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('grupo_materias')->truncate();
        //antes 1
        factory(App\GrupoMateria::class,2)->create();
    }
}
