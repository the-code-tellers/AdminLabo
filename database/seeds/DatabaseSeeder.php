<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        Model::unguard();
        $this->call(MensajeTableSeeder::class);
        $this->call(GestionTableSeeder::class);
        $this->call(MateriaTableSeeder::class);
        $this->call(GrupoTableSeeder::class);
        $this->call(HorarioTableSeeder::class);      
        $this->call(PersonaTableSeeder::class);
        //$this->call(AdministradorTableSeeder::class);
        //$this->call(DocenteTableSeeder::class);
        //$this->call(AuxiliarTableSeeder::class);
        //$this->call(GrupoMateriaTableSeeder::class);
        //$this->call(SesionTableSeeder::class);
        //$this->call(EstudianteTableSeeder::class);
        //$this->call(PortafolioTableSeeder::class);
        $this->call(RevisionAuxiliarTableSeeder::class);
        $this->call(SesionProductoTableSeeder::class);
        //$this->call(ProductoTableSeeder::class);

        Model::reguard();
    }
}
