<?php

use Illuminate\Database\Seeder;

class GestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//DB::table('gestions')->truncate();
        factory(App\Gestion::class,10)->create();
    }
}
