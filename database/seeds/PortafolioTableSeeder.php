<?php

use Illuminate\Database\Seeder;

class PortafolioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//DB::table('portafolios')->truncate();
        factory(App\Portafolio::class,1)->create();
    }
}
