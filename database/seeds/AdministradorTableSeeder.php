<?php

use Illuminate\Database\Seeder;

class AdministradorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
   	//	DB::table('administradors')->truncate();
        factory(App\Administrador::class,1)->create()->each(function ($administrador) {
                $administrador->persona()->save(factory(App\Persona::class)->make());
            });
    }
}
