<?php

use Illuminate\Database\Seeder;

class RevisionAuxiliarTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//DB::table('revision_auxs')->truncate();
        factory(App\RevisionAuxiliar::class,10)->create();
    }
}
