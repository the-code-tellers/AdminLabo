<?php

use Illuminate\Database\Seeder;

class SesionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//DB::table('sesions')->truncate();
        factory(App\Sesion::class,1)->create();
    }
}
