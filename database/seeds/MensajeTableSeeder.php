<?php

use Illuminate\Database\Seeder;

class MensajeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//DB::table('mensajes')->truncate();
        factory(App\Mensaje::class,10)->create();
    }
}
