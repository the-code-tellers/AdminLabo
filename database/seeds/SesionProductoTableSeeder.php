<?php

use Illuminate\Database\Seeder;

class SesionProductoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//DB::table('sesion_productos')->truncate();
        factory(App\SesionProducto::class,10)->create()->each(function ($sesionProducto) {
                $sesionProducto->productos()->save(factory(App\Producto::class)->make());
            });
    }
}
