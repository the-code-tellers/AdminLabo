<?php

use Illuminate\Database\Seeder;

class AuxiliarTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	 //DB::table('auxiliars')->truncate();
        factory(App\Auxiliar::class,3)->create()->each(function ($auxiliar) {
                $auxiliar->persona()->save(factory(App\Persona::class)->make());
            });
    }
}
