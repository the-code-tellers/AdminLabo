<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Mensaje::class, function ($faker) {
    return [
        'icono'=>str_random(10),
        'nombre'=>$faker->unique()->name,
        'codigo'=>str_random(10),
        'descripcion'=>$faker->text
    ];
});


$factory->define(App\Persona::class, function ($faker) {
    return [
        'nombre' => $faker->name,
        'apellido_paterno' => $faker->name,
        'apellido_materno' => $faker->name,
        'alias'=> $faker->unique()->name,
        'correo'=>$faker->unique()->email,
        'contrasenha'=> bcrypt('123456')   
    ];
});


$factory->define(App\Gestion::class, function ($faker) {
    return [
        'numero_gestion'=>$faker->randomNumber($nbDigits = 2, $strict = false),
        'anho'=> $faker->year($max = 'now'),
        'fecha_inicio'=> $faker->date($format = 'Y-m-d', $max = 'now'),
        'fecha_fin'=> $faker->date($format = 'Y-m-d', $max = '2019-12-12')

    ];
});

$factory->define(App\Materia::class, function ($faker) {
    return [
        'codigo_materia'=>$faker->unique()->randomNumber($nbDigits = 6, $strict = false),
        'nombre_materia'=>$faker->unique()-> word
    ];
});


$factory->define(App\Grupo::class, function ($faker) {
    return [
        'numero_grupo'=>$faker-> randomDigitNotNull        
    ];
});


$factory->define(App\Horario::class, function ($faker) {
  $diaSemana =['Lunes', 'Martes','Miercoles','Jueves','Viernes','Sabado'];
    return [
         'dia' => $faker->randomElement($diaSemana),
         'hora_entrada'=>$faker->dateTime($max = 'now', $timezone = 'UTC'),
         'hora_salida'=>$faker->dateTime($max = 'now', $timezone = 'UTC')
    ];
});


$factory->define(App\Administrador::class, function ($faker) {
	//$personaID= App\Persona::all()->pluck('id')->toArray();
    return [       
       //'persona_id'=> $faker->unique()->randomElement($personaID)
    ];
});


$factory->define(App\Docente::class, function ($faker) {
	//$personaID = App\Persona::all()->pluck('id')->toArray();
    return [
       //'persona_id'=> $faker->unique()->randomElement($personaID),
    ];
});


$factory->define(App\Auxiliar::class, function ($faker) {
	$personaID = App\Persona::all()->pluck('id')->toArray();
    return [
    	
    	'persona_id'=> $faker->unique()->randomElement($personaID),
        'codigo_sis'=> $faker->unique()->randomNumber($nbDigits = 9, $strict = true),
        'habilitado'=> true
    ];
});

/*$factory->afterMaking(App\Auxiliar::class, function ($auxiliar, $faker) {
    $auxiliar->grupomateria()->save(factory(App\GrupoMateria::class)->make());
});
*/
$factory->define(App\GrupoMateria::class, function ($faker) {
	    $docenteID =App\Docente::all()->pluck('id')->toArray();
    	//$auxiliarID =App\Auxiliar::all()->pluck('id')->toArray();
    	$gestionID = App\Gestion::all()->pluck('id')->toArray();
    	$grupoID = App\Grupo::all()->pluck('id')->toArray();
    	$materiaID = App\Materia::all()->pluck('id')->toArray();
    return [
    	
        'docente_id'=>$faker->/*unique()->*/randomElement($docenteID),
        //'auxiliar_id'=>$faker->unique()->randomElement($auxiliarID),
        'gestion_id'=>$faker->/*unique()->*/randomElement($gestionID),
        'grupo_id'=>$faker->/*unique()->*/randomElement($grupoID),
        'materia_id'=>$faker->/*unique()->*/randomElement($materiaID)
    ];
});

$factory->define(App\Sesion::class, function ($faker) {
	    $horarioID = App\Horario::all()->pluck('id')->toArray();
    	$grupomateriaID = App\GrupoMateria::all()->pluck('id')->toArray();
    	//$auxiliarID =App\Auxiliar::all()->pluck('id')->toArray();
    return [
      'numero_sesion'=>$faker->randomDigit(),
      'fecha'=>$faker->date($format = 'Y-m-d', $max = 'now'),
    	//'auxiliar_id'=>$faker->randomElement($auxiliarID),
        'horario_id'=>$faker->unique()->randomElement($horarioID), //falta hallar una alternativa a este unique
        'grupomateria_id'=>$faker->randomElement($grupomateriaID),
        'cupo' => mt_rand(3, 30)
    ];
});


$factory->define(App\Estudiante::class, function ($faker) {
        $codigoSis =['201512526', '201721114','201721124','201821114'];
	    //$personaID =App\Persona::all()->pluck('id')->toArray();
    	//$grupomateriaID = App\GrupoMateria::all()->pluck('id')->toArray();
    	//$sesionID = App\Sesion::all()->pluck('id')->toArray();
    return [
        //'persona_id'=> $faker->unique()->randomElement($personaID),
        'codigo_sis'=> $faker->unique()->randomElement($codigoSis),
        //'grupomateria_id'=> $faker->randomElement($grupomateriaID),
        //'sesion_id'=> $faker->randomElement($sesionID)
    ];
});


$factory->define(App\Portafolio::class, function ($faker) {
    //$estudianteID = App\Estudiante::all()->pluck('id')->toArray();
    return [
      //  'estudiante_id'=>$faker->unique()->randomElement($estudianteID),
        'direccion_portafolio'=> $faker->asciify('********************')
    ];
});


$factory->define(App\RevisionAuxiliar::class, function ($faker) {
    $auxiliarID = App\Auxiliar::all()->pluck('id')->toArray();
    return [
        'observacion'=>$faker-> text,
        'avances'=>$faker-> text,
        'asistio'=>false,
        'auxiliar_id'=>$faker->randomElement($auxiliarID)
    ];
});


$factory->define(App\SesionProducto::class, function ($faker) {
        $portafolioID = App\Portafolio::all()->pluck('estudiante_id')->toArray();
        $sesionID = App\Sesion::all()->pluck('id')->toArray();
        $revisionauxiliarID = App\RevisionAuxiliar::all()->pluck('id')->toArray();
    return [
        'portafolio_id'=>$faker->randomElement($portafolioID),
        'sesion_id'=>$faker->randomElement($sesionID),
        'revisionauxiliar_id'=>$faker->randomElement($revisionauxiliarID)
    ];
});


$factory->define(App\Producto::class, function ($faker) {
    //$sesionproductoID = App\SesionProducto::all()->pluck('id')->toArray();
    return [
        'hora_publicacion'=>$faker->dateTime($max = 'now', $timezone = null),
        'referencia'=>$faker->asciify('********************')
        //'sesionproducto_id'=>$faker->unique()->randomElement($sesionproductoID)
    ];
});
