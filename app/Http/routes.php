<?php

use Illuminate\Support\Facades\Response;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*////////////////////////////////PUEDEN VER SIN INICIAR SESION////////////////////////////// */
Route::get('/', function () {
    return view('welcome');
});

Route::get('/masinfo', function () {
    return view('layouts.masinfo');
});
/*Para que sirve esto porfavor */ 
Route::get('login',[
  'uses'=>'Auth\AuthController@getLogin',
  'as'=>'login'
]);

Route::get('/login', function () {
  return view('login.login');
});

/*////////////////////HASTA AQUI SIN INICIAR SESION/////////////////////////// */

Route::post('/login', 'SesionController@postLoginGeneral');

Route::get('/logout', 'CuentaController@logout');
/*////////////////////////////////////PARA ADMINISTRADOR//////////////////////////////////////////////////// */

Route::group(['middleware'=>['auth','administrador'],'prefix' => 'admin'], function($router){
  resource('administradors.horarios','HorarioController');
});

Route::group(['middleware'=>['auth','administrador'],'prefix' => 'admin'], function($router){
  resource('administradors.reporte','AdministradorReporteController');
});

Route::group(['middleware'=>['auth','administrador'],'prefix' => 'admin'], function($router){
  resource('administradors.docentes','AdministradorDocenteController');
});

Route::group(['middleware'=>['auth','administrador'],'prefix' => 'admin'], function($router){
  resource('administradors.sesions','AdministradorSesionController');
});

Route::group(['middleware'=>['auth','administrador'],'prefix' => 'admin'], function($router){
  resource('administradors','AdministradorController');
});

Route::group(['middleware'=>['auth','administrador'],'prefix' => 'admin'], function($router){
  resource('administradors.auxiliares','AdministradorAuxiliarController');
  resource('administradors.auxiliar','AdministradorRegistrarAuxiliarController');
});

Route::group(['middleware'=>['auth','estudiante'],'prefix' => 'estudiante'], function($router){
  resource('estudiantes.portafolio','EstudiantePortafolioController');
});

Route::group(['middleware'=>['auth','estudiante'],'prefix' => 'estudiante'], function($router){
  resource('estudiantes.portafolio.producto','PortafolioProductoController');
});

Route::group(['middleware'=>['auth','estudiante'],'prefix' => 'estudiante'], function($router){
  resource('estudiantes.portafolio.sesionproducto','EstudianteSesionProductosController');
});

Route::group(['middleware'=>['auth','estudiante'],'prefix' => 'estudiante'], function($router){
  resource('estudiantes.inscripcion','EstudianteInscripcionController');
});
Route::group(['middleware'=>['auth','estudiante'],'prefix' => 'estudiante'],function($router){
  resource('estudiantes.horario','EstudianteController');

});

/*//////////////////////////AQUI PARA DOCENTES/////////////////////////////////////////// */
Route::group(['middleware'=>['auth','docente'],'prefix' => 'docente'], function($router){
  resource('docentes','DocenteController');
  resource('docentes.horario','DocenteController');
});

Route::group(['middleware'=>['auth','docente'],'prefix' => 'docente'], function($router){
  resource('docentes.sesions','DocenteSesionController');
});

Route::post('/estudiante/registrado','EstudianteController@store');

Route::group(['middleware'=>['auth','estudiante'],'prefix' => 'estudiante'], function($router){
  resource('estudiantes','EstudianteController');
});
//ruta de portafolios para docente
Route::group(['middleware'=>['auth','docente'],'prefix' => 'docente'], function($router){
  resource('docentes.estudiante.portafolio.','DocenteEstudiantePortafolioController');
});
Route::group(['middleware'=>['auth','docente'],'prefix' => 'docente'], function($router){
  resource('docentes.estudiante.portafolio.sesion.','DocenteEstudianteSesionController');
});
Route::group(['middleware'=>['auth','docente'],'prefix' => 'docente'], function($router){
  resource('docentes.estudiante.portafolio.sesion.down','DocenteEstudianteSesionController');
});
 
//portafolios para docente

//reporte docente
Route::group(['middleware'=>['auth','docente'],'prefix' => 'docente'], function($router){
  resource('docentes.gestion.grupomateria.reporte','DocenteReporteController');
});
//
Route::group(['middleware'=>['auth','docente'],'prefix' => 'docente'], function($router){
  resource('docentes.portafolio','DocenteListaPortafolioController');
});

Route::group(['middleware'=>['auth','docente'],'prefix' => 'docente'], function($router){
  resource('docentes.archivos','ArchivoController');
});

Route::group(['middleware'=>['auth','auxiliar'],'prefix' => 'auxiliar'], function($router){
  resource('auxiliars.revisions','AuxiliarRevisionController');
});

Route::group(['middleware'=>['auth','auxiliar'],'prefix' => 'auxiliar'], function($router){
  resource('auxiliars','AuxiliarController');
  resource('auxiliars.horario', 'AuxiliarController');
});

Route::group(['middleware'=>['auth','auxiliar'],'prefix' => 'auxiliar'], function($router){
  resource('auxiliars.sesions','AuxiliarSesionController');
  resource('auxiliars.sesions', 'AuxiliarSesionController');
});

Route::group(['middleware'=>['auth','administrador'],'prefix' => 'admin'], function($router){
  resource('administradors.grupos','GruposController');
});

Route::group(['middleware'=>['auth','administrador'],'prefix' => 'admin'], function($router){
  resource('administradors.gestiones','GestionesController');
});

Route::group(['middleware'=>['auth','administrador'],'prefix' => 'admin'], function($router){
  resource('administradors.materias.grupomaterias','AdministradorGrupoMateriaController');
});

Route::group(['middleware'=>['auth','administrador'],'prefix' => 'admin'], function($router){
  resource('administradors.materias','MateriaController');
});

Route::post('/docente/docentes/{docenteId}/archivos/archivoSubido','ArchivoController@create')->middleware('auth','docente');

Route::get('/docente/docentes/{docenteId}/archivos/mostrarArchivos/','ArchivoController@show')->middleware('auth','docente');

Route::get('/docente/docentes/{docenteId}/archivos/mostrarArchivos/{archivo}', function ($docenteId,$archivo) {
  $path = storage_path('guiaPractica/').$archivo;
  return Response::make(file_get_contents($path), 200, [
    'Content-Type' => 'application/pdf',
    'Content-Type' => 'application/zip',
    'Content-Type' => 'text/plain',
    'Content-Type' => 'image/jpeg',
    'Content-Disposition' => 'attachment; filename="'.$archivo.'"'
  ]);
})->middleware('auth','docente');

/*///////////////////////////////////////////HASTA AQUI PARA DOCENTES///////////////////////////*/

/*//////////////////////////////AQUI PARA AUXILIARES/////////////////////////////////////////////////// */

Route::group(['middleware'=>['auth','auxiliar'],'prefix' => 'auxiliar'], function($router){
  resource('auxiliars.revisions','AuxiliarRevisionController');
});

Route::group(['middleware'=>['auth','auxiliar'],'prefix' => 'auxiliar'], function($router){
  resource('auxiliars','AuxiliarController');
});

Route::group(['middleware'=>['auth','auxiliar'],'prefix' => 'auxiliar'], function($router){
  resource('auxiliars.sesions','AuxiliarSesionController');
});
   //ruta reporte
Route::group(['middleware'=>['auth','auxiliar'],'prefix' => 'auxiliar'], function($router){
  resource('auxiliars.sesions.reporte','AuxiliarSesionReporteController');
});


Route::get('/auxiliar/auxiliars/{auxiliarId}/archivos/mostrarArchivos/','ArchivoController@showAuxiliar')->middleware('auth','auxiliar');

Route::get('/auxiliar/auxiliars/{auxiliarId}/archivos/mostrarArchivos/{archivo}', function ($auxiliarId,$archivo) {
  $path = storage_path('guiaPractica/').$archivo;
  return Response::make(file_get_contents($path), 200, [
    'Content-Type' => 'application/pdf',
    'Content-Type' => 'application/zip',
    'Content-Type' => 'text/plain',
    'Content-Type' => 'image/jpeg',
    'Content-Disposition' => 'attachment; filename="'.$archivo.'"'
  ]);
})->middleware('auth','auxiliar');
/*////////////////////////////////////////////HASTA AQUI PARA AUXILIARES///////////////////////////////////////////////////////*/
Route::get('/estudiante/estudiantes/{estudianteId}/archivos/mostrarArchivos/','ArchivoController@showEstudiante')->middleware('auth','estudiante');

Route::get('/estudiante/estudiantes/{estudianteId}/archivos/mostrarArchivos/{archivo}', function ($estudianteId,$archivo) {
  $path = storage_path('guiaPractica/').$archivo;
  
  return Response::make(file_get_contents($path), 200, [
    'Content-Type' => 'application/pdf',
    'Content-Type' => 'application/zip',
    'Content-Type' => 'text/plain',
    'Content-Type' => 'image/jpeg',
    'Content-Disposition' => 'attachment; filename="'.$archivo.'"'
  ]);
})->middleware('auth','estudiante');

Route::group(['middleware'=>['auth','auxiliar'],'prefix' => 'auxiliar'], function($router){
  resource('auxiliars.materias','AuxiliarMateriaController');
});