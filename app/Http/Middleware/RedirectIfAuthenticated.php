<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use App\Persona;
use App\Docente;
use App\Administrador;
use App\Auxiliar;
use App\Estudiante;


class RedirectIfAuthenticated
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    ///aqui identificar a donde redirigira dependiendo de lo que sea 
    public function handle($request, Closure $next)
    {
        if ($this->auth->check()) {
            $persona = Auth::user();
            
           // return redirect('/docente/docentes/1/archivos');
        }

        return $next($request);
    }
}
