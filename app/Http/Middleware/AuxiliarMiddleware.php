<?php

namespace App\Http\Middleware;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use Closure;
use App\Persona;


class AuxiliarMiddleware
{
    protected $auth;
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        
        if ($this->auth->check()) {
            $personaID = Auth::user()->id;
            if (Persona::find($personaID)->auxiliar != null){
            } else {
                return redirect()->to('/');
            }
          
        }
        return $next($request);
    }
}
