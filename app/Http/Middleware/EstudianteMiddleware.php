<?php

namespace App\Http\Middleware;
use Request;
use App\Persona;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;


class EstudianteMiddleware
{
    protected $auth;


    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    
    public function handle($request, Closure $next)
    {

        if ($this->auth->check()) {
            $personaID = Auth::user()->id;
            if (Persona::find($personaID)->estudiante != null){
                if ($request->path() === "estudiante/estudiantes/create" ){
                    return redirect()->to('/estudiante/estudiantes/'.(Persona::find($personaID)->estudiante->id)); 
                }
            } else {
               return redirect()->to('/');
            }
          
        } 
        return $next($request);
    }
}
