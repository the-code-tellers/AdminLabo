<?php

namespace App\Http\Middleware;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use Closure;
use App\Persona;
class DocenteMiddleware
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        if ($this->auth->check()) {
            $personaID = Auth::user()->id;
             if (Persona::find($personaID)->docente != null){
            } else {
                return redirect()->to('/');
            }
          
        }
        return $next($request);
    }
}
