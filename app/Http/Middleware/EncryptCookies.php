<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as BaseEncrypter;

class EncryptCookies extends BaseEncrypter
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
    	'cookieGrupoMateria',
        'cookieGestion',
        'cookieAuxiliar',
        'cookieHorario',
        'cookieSesion',
        
        'cookieGrupoMateriaDocSes',
        'cookieGestionDocSes',
        'cookieAuxiliarDocSes',
        'cookieHorarioDocSes',
        'cookieSesionDocSes',
        
        'cookieGrupoMateriaPractica',
        'cookieSesionMostrar',
        'cookieGrupoMateriaPracticaMostrar',
        'grupoMateriaCookie',
        'gestionSeleccionada',
        
        'cookieGrupoMateriaPracticaMostrar1',
        'cookieSesionMostrar1',

        'cookieGrupoMateriaPracticaMostrar2',
        'cookieSesionMostrar2',

        'cookieFechaAuxiliarLista',
        
        'cookieMateriaId',
        'cookieGrupoMateriaDocHora',


        'cookieMateriaPortafolio',
        'cookieMateriaPortafolios',

        'cookieGestion',
        'cookieMateriaDocenteId',
        'cookieGestionSubir',
        'cookieGestionMostrar',
        'cookieGestionMostrar2',
        'cookieGestioMostrar3',
        'cookieGestionHorarios'
    ];
}
