<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Estudiante;
use App\Http\Requests;
use App\Auxiliar;
use App\GrupoMateria;
use App\Materia;
use App\Gestion;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AuxiliarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grupoMaterias = GrupoMateria::all();
        $materia = Materia::all();
        $gestiones = Gestion::all();
        return view('auxiliar.horarios.tablaHorarios')->with('grupoMaterias', $materia)
        ->with('gestiones', $gestiones);   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { 
        return view('auxiliar.inicio.show')->with('id',$id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($auxiliarId)
    {
         $auxiliar = Auxiliar::find($auxiliarId);
        return view('auxiliar.infoAuxiliar.editinfo')->with('auxiliarId', $auxiliarId)->with('auxiliar',$auxiliar);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $auxiliarId)
    {
        $persona = Auxiliar::find($auxiliarId)->persona;
        $mensajes = [
            'nombre.max' =>'El nombre del auxiliar no puede ser mayor a 100 caracteres.',
            'apellido_paterno.max' => 'El apellido paterno del auxiliar no puede ser mayor a 255 caracteres.',
            'apellido_materno.max' => 'El apellido materno del auxiliar no puede ser mayor a 255 caracteres.',
            'correo.email'=>'El correo debe ser una dirección válida',
            'correo.max'=>'El correo del auxiliar no puede ser mas de 255 caracteres.',
                  ];
        
        $validator =  Validator::make($request->all(), [
            'nombre' => 'regex:/^[\pL\s\-]+$/u|required|max:100',
            'apellido_paterno' =>'regex:/^[\pL\s\-]+$/u|required|max:255',
            'apellido_materno' =>'regex:/^[\pL\s\-]+$/u|max:255',
            'correo' => 'required|email|max:255'
        ],$mensajes);

        if($validator -> fails()){
           return back()->withInput()->withErrors($validator);
         } else{

        $persona-> update([
            'apellido_paterno' => $request->get('apellido_paterno'),
            'apellido_materno' => $request->get('apellido_materno'),
            'nombre' => $request->get('nombre'),
            'correo' => $request->get('correo')
        ]);
         }
        $persona->save();
        return back()->with('info',"Se actualizó su información correctamente");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

   /* public function login()
    {
        return view('login');
    }*/
}
