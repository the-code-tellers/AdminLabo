<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gestion;
use App\GrupoMateria;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Carbon\Carbon;

class GestionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($adminId)
    {
        $gestions = Gestion::orderBy('anho','DEC')->paginate(7);
        $gestions->each(function($gestions){
        });
       return view('admin.gestiones.listaGestiones')->with('gestions',$gestions) ->with('adminId',$adminId);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idAdmin)
    {
        return view('gestion.gestionCreate')->with('idAdmin', $idAdmin);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$idAdmin)
    {
        $inicioGestion = $request->input('fecha_inicio');
        $finGestion = $request->input('fecha_fin');
        $numeroGestion =$request->input('numero_gestion');
        $inicioGestionFormat = (new Carbon($inicioGestion));
        $finGestionFormat = (new Carbon($finGestion));
        $diff = $finGestionFormat->diffInDays($inicioGestionFormat);

        if(($inicioGestionFormat -> year) != ($finGestionFormat -> year) ){
             return back() ->withErrors(["El minimo entre inicio y fin de gestión debe ser en el mismo año."])
            ->with('idAdmin', $idAdmin);
        }
        $minimoDeDias = 4 * 30;
        if($numeroGestion == 3 || $numeroGestion == 4){
            $minimoDeDias = 2 * 30;
        }

        if($diff < $minimoDeDias){
             return back() ->withErrors(["El mínimo entre inicio y fin de gestión es de ".($minimoDeDias/30)." meses. "])
            ->with('idAdmin', $idAdmin);
        }
        $gestiones = Gestion:: where('anho','=',$inicioGestionFormat -> year)->get();
        $existeChoque = false;
        
        $choqueFechaInicio = "";
        $choqueFechaFin = "";        
        foreach ($gestiones as $gest) {
            $inicio = (new Carbon($gest-> fecha_inicio));
            $fin = (new Carbon($gest-> fecha_fin));
             if($inicio > $inicioGestionFormat && $inicio < $finGestionFormat
                || $fin > $inicioGestionFormat && $fin < $finGestionFormat
                || $inicio <=  $inicioGestionFormat  && $fin >= $finGestionFormat){
                    $existeChoque = true;
                    $choqueFechaInicio = $inicio->format('Y-m-d');
                    $choqueFechaFin = $fin->format('Y-m-d');
                    break;
                }
        }
        if($existeChoque){
             return back() ->withErrors(["La gestión que trata de crear tiene un choque con una ya existente.
                             El choque ocurre con la gestión que empieza: ".$choqueFechaInicio." y termina: ".$choqueFechaFin."."])
            ->with('idAdmin', $idAdmin);
        }

        $gestion= new Gestion($request->all());
        $gestion->anho = $inicioGestionFormat -> year;
        $gestion->save();

        return redirect('/admin/administradors/'.$idAdmin.'/gestiones/') ->with("info", "La gestión se creo con exito.")
            ->with('idAdmin', $idAdmin);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($adminId)
    {
           
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($adminId, $gestionId)
    {
        $grupoMaterias = GrupoMateria::where('gestion_id', '=', $gestionId)->get();
        foreach($grupoMaterias as $grupoMateria) {
          $nombreMateria = $grupoMateria->materia->nombre_materia;
          $numeroGrupo = $grupoMateria ->grupo->numero_grupo;

          $sesions = $grupoMateria->sesiones;


          foreach($sesions as $sesion) {
              $sesionproductos = $sesion->sesionesProductos;
              $revisionesAuxiliar = array();
              foreach($sesionproductos as $sesionproducto) {
                   
                   
                  $productos = $sesionproducto->productos;
                  foreach($productos as $producto){
                      $producto->delete();
                  }

                  $revisionesDocente = $sesionproducto->revisionDocente;
                  foreach($revisionesDocente as $revisionDocente){
                      $revisionDocente->delete();
                  }

                  array_push($revisionesAuxiliar, $sesionproducto->revisionAuxiliar);
                  $sesionproducto->delete();
              }   

              while (!empty($revisionesAuxiliar)){
                  array_pop($revisionesAuxiliar)->delete();
              }
              $sesion->delete();
          }
          
          $grupoMateria ->delete();
        }
        $gestion = Gestion::find($gestionId);
        $infoGestion = $gestion->numero_gestion." - ".$gestion->anho;
        $gestion -> delete();
        return back()->with('info', 'Se eliminó satisfactoriamente todos los contenidos de la Gestión: '.$infoGestion);
    }
}
