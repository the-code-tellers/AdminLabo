<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CreateSesionRequest;
use App\Http\Controllers\Controller;
use App\Sesion;
use App\GrupoMateria;
use App\Grupo;
use App\Auxiliar;
use App\Horario;
use App\Gestion;
use Carbon\Carbon;

class AdministradorSesionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($adminId)
    {
        $grupoMaterias = GrupoMateria::all();
        if($grupoMaterias->isEmpty()) {
          return view('admin.sesion.noHayGrupos')->withErrors(['Para crear Sesiones es necesario tener registrada al menos una materia con grupo.']);
        }
        $grupos = Grupo::all();
        $auxiliares = Auxiliar::all();
        $horarios = Horario::leftjoin('sesions', 'horarios.id','=','sesions.horario_id')
                            ->whereNull('sesions.id')
                            -> distinct()
                            -> select(['horario_id'])
                            ->addSelect('horarios.id')
                            -> addSelect('hora_entrada')
                            -> addSelect('hora_salida')
                            -> addSelect('dia')->get();
        //dd($horarios);
        $gestiones = Gestion::all();
        $dias = Horario::dias();
        return view('admin.sesion.autocreate')->with('grupoMaterias', $grupoMaterias)
                    -> with ('grupos', $grupos)-> with('adminId', $adminId)
                    -> with(compact('auxiliares'))
                    -> with(compact('horarios'))
                    -> with('dias',$dias)
                    -> with(compact('gestiones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
        //public function store(Request $request)
    public function store(CreateSesionRequest $request)
    {     //dd($request->horario);
        //$gestion = Gestion::find($request->cookie('cookieGestion'));
        $gestion = Gestion::find($request->get('gestion'));
        //$horario_id = $request->cookie("cookieHorario");
        $horario_id = $request->get('horario');
        $sesionesPrueba = Sesion::where('horario_id', '=', $horario_id)->
            join('grupo_materias', 'sesions.grupomateria_id', '=', 'grupo_materias.id')->
            where('gestion_id', '=', $gestion->id)->get();
        if($sesionesPrueba->isEmpty()) {
          $numeroSesion = 1;
          $grupomateria_id = $request -> cookie("cookieGrupoMateria");
          $auxiliar_id = $request->cookie("cookieAuxiliar");
          $fechaFinGestion = new Carbon($gestion->fecha_fin); 
          $fechaInicioGestion = new Carbon($gestion->fecha_inicio); 
          $dia = $request->get('dia'); //get from request
          $diaDeHoy = date('w', strtotime($fechaInicioGestion));
          $arregloDias=['Lunes' => 1,
          'Martes'   => 2,
          'Miercoles'=> 3,
          'Jueves'   => 4,
          'Viernes'  => 5,
          'Sabado'   => 6];
          $cantDias = $arregloDias[$dia] - $diaDeHoy;
          if($cantDias < 0){
              $cantDias = 7 + $cantDias;
          }
          $cupo = $request->get('cupo');
          $fechaDiaSesion = $fechaInicioGestion->copy()->addDays($cantDias);
          $arrayPrueba =[];
          while($fechaDiaSesion <= $fechaFinGestion){
              $nuevaSesion = new Sesion;
              $nuevaSesion ->horario_id = $horario_id;
              $nuevaSesion->numero_sesion = $numeroSesion;
              $nuevaSesion->cupo = $cupo;
              $numeroSesion++;
              $nuevaSesion->grupomateria_id = $grupomateria_id;
              $nuevaSesion->auxiliar_id = $auxiliar_id;
              $nuevaSesion->fecha = $fechaDiaSesion;
              $fechaDiaSesion = $fechaDiaSesion->copy()->addDays(7);
              $nuevaSesion->save();
          }      
          return back()->with("info", "Las sesiones se crearon con éxito!");
        } else {
          return back()->withInput()->withErrors(['El horario escogido para la gestión selecionada ya fue tomado.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idAdmin, $idSesion)
    {
        //
        dd($idAdmin.'hola'.$idSesion);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $ed)
    {
        //
        dd($id.'---'.$ed);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
