<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Materia;
use App\GrupoMateria;
use App\Sesion;
use App\SesionProducto;
use App\RevisionDocente;
use App\RevisionAuxiliar;
use App\Producto;
use App\Portafolio;
use App\Estudiante;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class EstudianteInscripcionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($idEstud)
    {
        $materiasConGrupo = SesionProducto::where('portafolio_id','=', $idEstud)->join("sesions",
            "sesion_productos.sesion_id", '=', "sesions.id")->
            join("grupo_materias", "sesions.grupomateria_id", '=', "grupo_materias.id")->
            join("materias", "grupo_materias.materia_id", '=', "materias.id")->
            join("grupos", "grupo_materias.grupo_id", '=', "grupos.id")->
            select('grupomateria_id')->distinct()->
            addSelect('nombre_materia')-> 
            addSelect('numero_grupo')->
            addSelect('codigo_materia')->
            addSelect('docente_id')->
            
            get();
        return view('estudianteInscripcion.estadoInscripcion')
                    ->with('idEstud', $idEstud)
                    -> with(compact('materiasConGrupo')) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idEstud)
    {
        $materias = Materia::all();
        $grupoMaterias = GrupoMateria::all();
        return view('estudianteInscripcion.inscripcion')
                    ->with('idEstud', $idEstud)
                    ->with('materias', $materias)
                    ->with('grupoMaterias', $grupoMaterias);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $idEstud)
    {

        $checkboxInscripcion = $request ->get('checkboxInscripcion');

        if ($checkboxInscripcion =="true"){
           if($request->get('grupo')) {
              $cupoArriba = $request->get('cupo');
              if ($cupoArriba > 0) { 
                $sesiones = Sesion::all();   
                $inscritoPreviamente = SesionProducto::where('portafolio_id', '=', $idEstud)->
                      join('sesions', 'sesion_productos.sesion_id', '=', 'sesions.id')->
                      where('grupomateria_id', '=', $request->get('grupo'))->get();
                if (!($inscritoPreviamente->isEmpty())) {
                  return back()->withErrors(["Ya estás inscrito en la materia y grupo seleccionado."]);
                }
                foreach ($sesiones as $sesion) {
                    if($sesion->grupomateria_id == $request->get('grupo')){
                        $nuevaRevisionAuxiliar = 
                            new RevisionAuxiliar(['auxiliar_id' 
                                => $sesion->grupoMateria->auxiliar->id]);
                        $nuevaRevisionAuxiliar->save();
                        $nuevaSesionProducto = new SesionProducto(['portafolio_id' => $idEstud, 'revisionauxiliar_id' => $nuevaRevisionAuxiliar->id]);
                        $sesion->sesionesProductos()->save($nuevaSesionProducto);
                        $sesion->cupo = ($sesion->cupo) - 1;
                        $sesion->save();

                        $nuevoProducto = new Producto; 
                        $nuevoProducto->hora_publicacion = null;
                        $nuevoProducto->referencia= null;
                        $nuevoProducto->sesionproducto_id =  $nuevaSesionProducto ->id;
                        $nuevoProducto->save();

                        $nuevaRevisionDocente = 
                            new RevisionDocente(['docente_id' 
                                => $sesion->grupoMateria->docente->id]);
                        $nuevaSesionProducto->revisionDocente()->save($nuevaRevisionDocente); 
                    } 
                }
                return redirect('/estudiante/estudiantes/'.$idEstud.'/inscripcion/')->with("info", "Te inscribiste a la materia y a sus sesiones.");                     
              } else {
                return back()->withErrors(["No quedan cupos restantes para el grupo que escogiste."]);
              }
           }

            else {
                return back()->withErrors(["No existen grupos disponibles para la materia seleccionada, no puedes inscribirte."]);
            }
        }
        else {            
            if( $request->get('grupoOpcionDos')){
              $cupoAbajo = $request->get('cupoA');
              if ($cupoAbajo > 0) {                       
                $sesiones = Sesion::all(); 
                $inscritoPreviamente = SesionProducto::where('portafolio_id', '=', $idEstud)->
                      join('sesions', 'sesion_productos.sesion_id', '=', 'sesions.id')->
                      where('grupomateria_id', '=', $request->get('grupoOpcionDos'))->get();
                if (!($inscritoPreviamente->isEmpty())) {
                  return back()->withErrors(["Ya estás inscrito en la materia y grupo seleccionado."]);
                }
                foreach ($sesiones as $sesion) {
                    if($sesion->grupomateria_id == $request->get('grupoOpcionDos')){
                        $nuevaRevisionAuxiliar = 
                            new RevisionAuxiliar(['auxiliar_id' 
                                => $sesion->grupoMateria->auxiliar->id]);
                        $nuevaRevisionAuxiliar->save();
                        $nuevaSesionProducto = new SesionProducto(['portafolio_id' => $idEstud, 'revisionauxiliar_id' => $nuevaRevisionAuxiliar->id]);
                        $sesion->sesionesProductos()->save($nuevaSesionProducto);
                        $sesion->cupo = ($sesion->cupo) - 1;
                        $sesion->save();

                        $nuevoProducto = new Producto; 
                        $nuevoProducto->hora_publicacion = null;
                        $nuevoProducto->referencia= null;
                        $nuevoProducto->sesionproducto_id =  $nuevaSesionProducto ->id;
                        $nuevoProducto->save();

                        $nuevaRevisionDocente = 
                            new RevisionDocente(['docente_id' 
                                => $sesion->grupoMateria->docente->id]);
                        $nuevaSesionProducto->revisionDocente()->save($nuevaRevisionDocente);                        
                    } 
                }
                return redirect('/estudiante/estudiantes/'.$idEstud.'/inscripcion/')->with("info", "Te inscribiste a la materia y a sus sesiones.");                     
              } else {
                return back()->withErrors(["No quedan cupos restantes para el grupo que escogiste."]);
              }
            }

            else {
                return back()->withErrors(["No existen grupos disponibles para la materia seleccionada, no puedes inscribirte."]);
            }          
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idEstud,$idGrupoMateria)
    {
        $sesionesProducto = Estudiante::find($idEstud)->portafolio->sesionesProductos;


        foreach ($sesionesProducto as $sesionProducto) {
            if ($sesionProducto->sesion->grupomateria_id == $idGrupoMateria ){
             $productos = $sesionProducto->productos;
                foreach ($productos as $producto) {
                    $producto->delete();
                }
            }
        }

        $revisionesAuxiliar = array();

        foreach ($sesionesProducto as $sesionProducto){
            if ($sesionProducto->sesion->grupomateria_id == $idGrupoMateria ){
            array_push($revisionesAuxiliar, $sesionProducto->revisionAuxiliar);
            
                $revisionesDocentes = $sesionProducto ->revisionDocente;
                foreach($revisionesDocentes as $revisionesDocente){
                    $revisionesDocente->delete();
                }
                $sesion= $sesionProducto->sesion;
                $sesion->cupo = ($sesion->cupo) + 1;
                $sesion->save();
                
                $sesionProducto -> delete();
             }
        }

        while (!empty($revisionesAuxiliar)){
           array_pop ( $revisionesAuxiliar)->delete();
        
        }

        return  back()->with("info", "Te desinscribiste a la materia y a sus sesiones.");
    }
}

