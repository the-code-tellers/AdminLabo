<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use App\Docente;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;
use Illuminate\Http\Response;


class AdministradorDocenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($adminId)
    {
        $docentes = Docente::orderBy('id','ASC')->paginate(7);
        $docentes->each(function($docentes){
        $docentes->persona;
        });
       return view('admin.listaDocentes.mostrarDocentes')->with('docentes',$docentes)->with('adminId',$adminId);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($adminId)
    {
        return view('admin.registro.registroDocente')->with('adminId',$adminId);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$adminId)
    {
        $persona = new Persona($request->all()); 
        $mensajes = [
            'nombre.required' => 'Agrega el nombre del docente.',
            'nombre.max' =>'El nombre del docente no puede ser mayor a 255 caracteres.',
            'apellido_paterno.required' => 'El apellido paterno del docente es requerido.',
            'apellido_paterno.max' => 'El apellido paterno del docente no puede ser mayor a 255 caracteres.',
            'apellido_materno.required' => 'El apellido materno del docente es requerido.',
            'apellido_materno.max' => 'El apellido materno del docente no puede ser mayor a 255 caracteres.',
            'alias.max' => 'El alias no debe pasar de 255 caracteres.',
            'alias.unique' => 'El alias debe ser único.',
            'correo.required'=>'El correo es requerido',
            'correo.email'=>'El correo debe ser una dirección válida',
            'correo.max'=>'El correo del docente no puede ser mas de 255 caracteres.',
            'correo.unique'=>'El correo debe ser único.',
            'contrasenha.required' => 'La contraseña es obligatoria.',
            'contrasenha.min' => 'La contraseña debe ser al menos de 6 caracteres.'
            
        ];
        $validator =  Validator::make($request->all(), [
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'apellido_paterno' =>'required|regex:/^[\pL\s\-]+$/u|max:255',
            'apellido_materno' =>'regex:/^[\pL\s\-]+$/u|max:255',
            'alias' => 'max:255|unique:personas',
            'correo' => 'required|email|max:255|unique:personas',
            'contrasenha' => 'required|min:6'
        ],$mensajes);
        if($validator -> fails()){
           return back()->withInput()->withErrors($validator);
         } else{
            $personaContrasenha = $persona->contrasenha;
            $persona->contrasenha = bcrypt($personaContrasenha);
            $persona->save();

            $docente = new Docente($request->all());
            $docente->persona_id = $persona->id;
            $docente->save();
            return back()->with("info", "Ha registrado satisfactoriamente un docente.")->with('adminId',$adminId);  
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
