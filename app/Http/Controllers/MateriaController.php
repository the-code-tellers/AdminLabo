<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Materia;
use App\GrupoMateria;
use App\Gestion;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class MateriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($adminId)
    {
        $grupoMaterias = GrupoMateria::all();
        $gestiones = Gestion::all();
        $materias = Materia::orderBy('nombre_materia','ASC')->paginate(10);
        $materias->each(function($materias){
        $materias->materia;
        });
       return view('admin.materias.listaMaterias')->with('materias',$materias)->with('grupoMaterias',$grupoMaterias)-> with('adminId',$adminId)->with ('gestiones',$gestiones);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($adminId)
    {
        return view ("admin.materias.registroMateria") -> with('adminId',$adminId);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $materia = new Materia($request->all()); 
        $mensajes = [
            'codigo_materia.required'=>'El código de la materia es requerido',
            'codigo_materia.numeric'=>'El código de la materia debe ser de valor númerico',
            'codigo_materia.unique'=>'El código de la materia debe ser único.',
            'nombre_materia.required' => 'Agrega el nombre de la materia.',
            'nombre_materia.max' =>'El nombre de la materia  no puede ser mayor a 255 caracteres.',
            'nombre_materia.unique' => 'El nombre de la materia debe ser único.'
        ];
        $validator =  Validator::make($request->all(), [
            'codigo_materia' => 'required|numeric|unique:materias',
            'nombre_materia' =>'required|max:255|unique:materias',
        ],$mensajes);
        if($validator -> fails()){
           return back()->withInput()->withErrors($validator);
         } else{
            $materia->save();
            return back()->withInput()->with("info","Se ha registrado satisfactoriamente la materia");  
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($adminId)
    {
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
