<?php

namespace App\Http\Controllers;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Sesion;
use App\Gestion;
use App\PracticaLaboratorio;
use App\GrupoMateria;
use App\Estudiante;
use App\Portafolio;
use App\SesionProducto;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class ArchivoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($docenteId)
    {
        $grupoMaterias = GrupoMateria::where("docente_id","=",$docenteId)->get();
        $sesions = Sesion::all();
        
        $grupoMateriasX = GrupoMateria::where("docente_id","=",$docenteId)->get(['gestion_id'])->toArray();
        $arrayX = array_flatten($grupoMateriasX);
        $arrayGestionUnicos = array_unique($arrayX);
        $gestions = Gestion::find($arrayGestionUnicos);
        

        
        if ($grupoMaterias->isEmpty()){
            return view('docentePractica.subirArchivosError')->withErrors(['Aún no tiene una materia asignada, no tiene permitido subir archivos, por favor consulte con el administrador.']); 
        } else {
            return view('docentePractica.subirArchivos')->with('docenteId',$docenteId)->with('grupoMaterias',$grupoMaterias)->with('sesions',$sesions)->with('gestions',$gestions); 
        }
           
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $docenteId)
    {
        $sesionId = $request->cookie('cookieSesion');
        $estaVacio = $request->input('sesion');
      
        if($estaVacio===null){
            return back()->withErrors(["Debe existir una sesion para poder subir el archivo,por favor que el administrador o usted  asigne sesiones."]); 
        } else{

        $file = $request->file('file');
        if(!empty($file)) {
            $nombre = $file->getClientOriginalName();
            $input = array('archivo' => Input::file('file'));
            $reglas = array('archivo' => 'mimes:jpg,png,xls,doc,pdf,zip,rar,txt,docx');
            $validator = Validator::make($input,$reglas);
            if ($validator -> fails()){
                return back()->withErrors(["El formato del archivo no es aceptado."]);
            } else{
          /*\Storage::disk('local')->put($nombre,\File::get($file));*/
                $practicaLaboratorio = new PracticaLaboratorio($request->all()); 
                $practicaLaboratorio->sesion_id = $sesionId;
                $practicaLaboratorio->enlace = $nombre;
                $practicaLaboratorio->save();
                \Storage::disk('local')->put($nombre,\File::get($file));
                return back()->with("info","Su archivo"." ".$nombre." "."se ha guardado satisfactoriamente");
            }
        } else {
          return back()->withErrors(["Por favor ingrese un archivo."]);  
        }
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$docenteId)
    {
        $grupoMaterias1 = GrupoMateria::where("docente_id","=",$docenteId)->get();
        $grupoMateriasX = GrupoMateria::where("docente_id","=",$docenteId)->get(['gestion_id'])->toArray();
        $arrayX = array_flatten($grupoMateriasX);
        $arrayGestionUnicos = array_unique($arrayX);
        $sesions1 = Sesion::all();
        $gestiones = Gestion::find($arrayGestionUnicos);

        if ($grupoMaterias1->isEmpty()){
            return view('docentePractica.mostrarArchivosError')->withErrors(['Aún no tiene una materia asignada, por lo que no puede ver las prácticas, por favor consulte con el administrador.']); 
        } else {
            return view('docentePractica.mostrarArchivos')->with('docenteId',$docenteId)->with('grupoMaterias1',$grupoMaterias1)->with('sesions1',$sesions1)->with('gestiones',$gestiones);
        }
    }

    public function showEstudiante(Request $request,$estudianteId)
    {
        
        $sesionProductoEstudiante = SesionProducto::where("portafolio_id","=", $estudianteId)->get(['sesion_id'])->toArray();
        $array = array_flatten($sesionProductoEstudiante);
        $arrayIds= array_unique($array);
       
        $arrayGrupoMatIds = array();
        
        for($x=0;$x<count($arrayIds);$x=$x+1){
          $grupoMateriaId = Sesion::find($arrayIds[$x])->grupoMateria->id;
          array_push($arrayGrupoMatIds,$grupoMateriaId);
        }
        $grupoMaterias2 = GrupoMateria::find($arrayGrupoMatIds);
       
        $arrayGestionesIds = array();
        for($x=0;$x<count($arrayGrupoMatIds);$x=$x+1){
            $grupoGestionId = GrupoMateria::find($arrayGrupoMatIds[$x])->gestion->id;
            array_push($arrayGestionesIds,$grupoGestionId);
        }
        $arrayG2= array_unique($arrayGestionesIds);
        $gestiones2 = Gestion::find($arrayG2);
        $sesions2 = Sesion::all();
        if ($grupoMaterias2->isEmpty()){
            return view('estudiante.mostrarArchivosEstudianteError')->withErrors(['Aún no tiene una materia asignada por lo que no puede ver las prácticas, por favor inscribase a una materia.']);
        } else {
            return view('estudiante.mostrarArchivosEstudiante')->with('estudianteId',$estudianteId)->with('grupoMaterias2',$grupoMaterias2)->with('sesions2',$sesions2)->with('gestiones2',$gestiones2);
        }    
    }

    public function showAuxiliar(Request $request,$auxiliarId)
    {  
        
        
        $arrayIdGrupoMateria = array();
        
        $sesions3 = Sesion::where('auxiliar_id',"=",$auxiliarId)->get();

        $arraySesionesIdMateria = Sesion::where('auxiliar_id',"=",$auxiliarId)->get(['grupomateria_id'])->toarray();
        $array2 = array_flatten($arraySesionesIdMateria);
        $arrayIdsUnicos = array_unique($array2);
        $grupoMaterias3 = GrupoMateria::find($arrayIdsUnicos);
        
        $arrayGestionesIds2 = array();
        for($x=0;$x<count($arrayIdsUnicos);$x=$x+1){
            $grupoGestionId = GrupoMateria::find($arrayIdsUnicos[$x])->gestion->id;
            array_push($arrayGestionesIds2,$grupoGestionId);
        }
        $arrayG3= array_unique($arrayGestionesIds2);
        $gestiones3 = Gestion::find($arrayG3);


        if($grupoMaterias3->isEmpty()){
            return view('auxiliar.mostrarArchivosAuxiliarError')->withErrors(['Aún no tiene una materia asignada por lo que no puede ver ningún archivo, por favor consulte con el administrador.']);
        } else {
          if($sesions3 ->isEmpty())  {
            return back()->withErrors(['Aún no tienes sesiones asignadas, por lo que no tienes una materia, por favor solicita al administrador que te asigne una materia y sesión.']);
         } else {
            return view('auxiliar.mostrarArchivosAuxiliar')->with('auxiliarId',$auxiliarId)->with('grupoMaterias3',$grupoMaterias3)->with('sesions3',$sesions3)->with('gestiones3',$gestiones3);
         }
    }
        
    }


   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


   


}
