<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use App\Docente;
use App\RevisionDocente;
use App\GrupoMateria;
use App\Estudiante;
use App\Portafolio;
use App\SesionProducto;
use App\Materia;
use App\Gestion;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DocenteListaPortafolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($idDocente)
    {       
        $docente = Docente::find($idDocente); //Docente que ingresa 

        $materias = GrupoMateria::all();

        $grupoMateriasX = GrupoMateria::where("docente_id","=",$idDocente)->get(['gestion_id'])->toArray();
        $arrayX = array_flatten($grupoMateriasX);
        $arrayGestionUnicos = array_unique($arrayX);
        $gestiones = Gestion::find($arrayGestionUnicos);
        
        $arregloMateriasDocente= array();
        foreach ($materias as $materia){
            $grupoMatDocId = $materia -> docente_id;
            if ($grupoMatDocId == $idDocente){
                        array_push ( $arregloMateriasDocente , $materia);               
            }
        }

        $arregloMateriasDelDocente = array_reverse($arregloMateriasDocente);
        //Lista de estudiantes del docente

        $estudiantes = Estudiante::all();
        $sesionesProducto = SesionProducto::all();
        $revisionesDocente = RevisionDocente::all();
        //$gestiones = Gestion::all();

        $arregloRevisionesDocente=array();
        foreach ($revisionesDocente as $revisionDocente) {
            if($revisionDocente->docente_id == $idDocente){
                array_push ( $arregloRevisionesDocente , $revisionDocente); 
            }
        }

        if(empty($arregloMateriasDelDocente)){
           return view('docente.portafoliosOrdenadosError')->withErrors(['Lo sentimos, usted no tiene una materia asignada a la cual los estudiantes puedan inscribirse,consulte con el administrador.']);
        } else {
            return view ('docente.portafoliosOrdenados')
            ->with(compact ('arregloMateriasDelDocente'))
            ->with('docente', $docente)
            ->with('idDocente', $idDocente)
            ->with('sesionesProducto', $sesionesProducto)
            ->with('estudiantes', $estudiantes)
            ->with('arregloRevisionesDocente',$arregloRevisionesDocente)
            ->with('gestiones', $gestiones);
        }

        //array_push ($arregloRevisionesDocente, $gestion)
        
        //sesiones que dio el docente
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $idDocente)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

