<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\GrupoMateria;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdministradorGrupoMateriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($adminId, $materiaId, $grupoMateriaId)
    {
    	  $grupoMateria = GrupoMateria::find($grupoMateriaId);
        return view('admin.materias.grupomaterias.grupoMateria')->with('grupoMateria',$grupoMateria)
        		->with('grupoMateriaId',$grupoMateriaId)->with('adminId',$adminId)->with('materiaId',$materiaId);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($adminId, $materiaId, $grupoMateriaId)
    {
        $grupoMateria = GrupoMateria::find($grupoMateriaId);
        $nombreMateria = $grupoMateria->materia->nombre_materia;
        $numeroGrupo = $grupoMateria ->grupo->numero_grupo;

        $sesions = $grupoMateria->sesiones;


        foreach($sesions as $sesion) {
            $sesionproductos = $sesion->sesionesProductos;

            $revisionesAuxiliar = array();
            foreach($sesionproductos as $sesionproducto) {
                 
                 
                $productos = $sesionproducto->productos;
                foreach($productos as $producto){
                    $producto->delete();
                }

                $revisionesDocente = $sesionproducto->revisionDocente;
                foreach($revisionesDocente as $revisionDocente){
                    $revisionDocente->delete();
                }

                array_push($revisionesAuxiliar, $sesionproducto->revisionAuxiliar);
                //$revAux = $sesionproducto->revisionAuxiliar;
                $sesionproducto->delete();  
                //$revAux->delete();
            }   

            while (!empty($revisionesAuxiliar)){
                array_pop($revisionesAuxiliar)->delete();
            }
            $sesion->delete();
        }

    	$grupoMateria ->delete();
        return redirect('/admin/administradors/'.$adminId.'/materias')
        			->with("info", "Se elimino el Grupo ".$numeroGrupo." de ".$nombreMateria);
    }
}
