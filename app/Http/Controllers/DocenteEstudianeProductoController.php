<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
//use Illuminate\Http\Response;
use App\Http\Requests;
use App\Producto;
use App\SesionProducto;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Storage;


class DocenteEstudianeProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $estudId, $idSesionProducto)
    {   
           
                   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($docId, $estudId, $materiaId, $idSesionProducto,$coincidencia)
    {
        dd('entreee');
        $path = storage_path('ArchivoProductos/').$coincidencia;
        
        return Response::make(file_get_contents($path), 200, [
          'Content-Type' => 'application/pdf',
          'Content-Type' => 'application/zip',
          'Content-Type' => 'application/rar',
          'Content-Type' => 'text/plain',
          'Content-Type' => 'image/jpeg',
          'Content-Type' => 'image/png',
          'Content-Disposition' => 'attachment; filename="'.$coincidencia.'"'
        ]);
      
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($estudId, $idSesionProducto)
    {
        
    }
}
