<?php

namespace App\Http\Controllers;
date_default_timezone_set('America/Santo_Domingo');
use Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Persona;
use App\Auxiliar;
use App\Sesion;
use App\Horario;
use App\Administrador;
use App\Docente;
use App\Estudiante;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

    


class SesionController extends Controller
{

  public function __construct()
  {
      $this->middleware('guest', ['except' => 'getLogout']);
  }
  //protected $redirectTo ='/docente/docentes/1/archivos/';
  protected $loginPath ='/login';

  //use AuthenticatesAndRegistersUsers, ThrottlesLogins;
   public function postLogin(Request $request){
     if(Auth::attempt(['correo'=> $request->email,'password' => $request->password])) {
          print('entro     ');
          $correo = $request->email;
          $personas = Persona::all();
          foreach ($personas as $persona){
            $correos = $persona->correo;
            if ($correos === $correo){
                $idPersona = $persona -> id ;
            }
          }
          $auxiliar = Auxiliar::find($idPersona);
          $idAuxDef = $auxiliar -> id;
          $sesion = Sesion::find($idAuxDef);
          $horarioId = $sesion -> horario_id;
          $horario = Horario::find($horarioId);

          $datoEntrada = $horario -> hora_entrada;
          $datoSalida = $horario -> hora_salida; 
          $datoFecha = Carbon::now();
          $hora = date('H:i:s',strtotime($datoFecha));
          $horaEntrada = date('H:i:s',strtotime($datoEntrada));
          $horaSalida = date('H:i:s',strtotime($datoSalida));
          if( $hora<$horaSalida && $hora>=$horaEntrada){
            return "entro si es tu sesion ";
          }
          else{
            return "no es tu sesion lo sentimos no puedes ingresar al sistema";
          }
          
     }
     else {
      print($request->email);
      return "no es tu sesion";
     }
   }



   public function postLoginGeneral(Request $request){
    $remember = $request->input ('remember');
    if ( empty($request->email) || empty($request->password) ){
      return back()->withErrors(["Ingrese su correo y contraseña"]);
    }
    if(Auth::attempt(['correo'=> $request->email,'password' => $request->password],$remember)) { 
       $idUser=Auth::user()->id;
      /////////////////////////////////////////////////////////////////////////////////////////////
         $correo = $request->email;
         $personas = Persona::all();
         foreach ($personas as $persona){
           $correos = $persona->correo;
           if ($correos === $correo){
              $idPersona = $persona -> id ;
              if (Persona::find($idPersona)->auxiliar != null){                 
                $idAux = Persona::find($idUser)->auxiliar->id;
                return redirect('/auxiliar/auxiliars/'.$idAux);
              }              
              else if (Persona::find($idPersona)->estudiante != null) {
                  $idEst = Persona::find($idUser)->estudiante->id;
                  return redirect('/estudiante/estudiantes/'.$idEst);
                } else if (Persona::find($idPersona)->administrador != null) {
                  $idAdmin = Persona::find($idUser)->administrador->id;
                  return redirect('/admin/administradors/'.$idAdmin);
                } else if(Persona::find($idPersona)->docente != null) {
                  $idDoc = Persona::find($idUser)->docente->id;
                  return redirect ('/docente/docentes/'.$idDoc);
                } else {
                  return back()->withErrors(["Aún no tiene una cuenta"." ".$request->email."."]);
                }
              }
            } 
          } else {
            return back()->withErrors(["Aún no tiene una cuenta"." ".$request->email." "."o su contraseña es incorrecta"]);
          } 
    }
      

}
   