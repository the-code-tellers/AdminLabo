<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Horario;
use App\Sesion;
use App\GrupoMateria;
use App\Gestion;
use App\Materia;
use App\Estudiante;
use App\Persona;
use App\Portafolio;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class EstudianteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grupoMaterias = GrupoMateria::all();
        $materia = Materia::all();
        $gestiones = Gestion::all();
        return view('estudiante.dropDownMaterias')->with('grupoMaterias', $materia)
        ->with('gestiones', $gestiones);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('estudiante.registroEstudiante');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $persona = new Persona($request->all()); 
        
        $mensajes = [
            'nombre.required' => 'Por favor ingresa tu nombre.',
            'nombre.max' =>'Su nombre no puede ser mayor a :max caracteres.',
            'apellido_paterno.required' => 'Su apellido paterno es requerido.',
            'apellido_paterno.max' => 'Su apellido paterno no puede ser mayor a 255 caracteres.',
            'apellido_materno.required' => 'Su apellido materno  es requerido.',
            'apellido_materno.max' => 'Su apellido materno  no puede ser mayor a 255 caracteres.',
            'alias.max' => 'Su alias no debe pasar de 255 caracteres.',
            'alias.unique' => 'Su alias debe ser unico.',
            'correo.required'=>'Su correo es requerido',
            'correo.email'=>'Su correo debe ser una dirección válida',
            'correo.max'=>'Su correo  no puede ser mas de 255 caracteres.',
            'correo.unique'=>'Su correo debe ser único.',
            'codigo_sis.required'=>'Su código sis es requerido.',
            'codigo_sis.numeric'=>'El código sis debe ser un valor numérico.',
            'codigo_sis.min'=>'El código sis debe tener 9 dígitos como mínimo.',
            'codigo_sis.max'=>'El código sis debe tener 9 dígitos como máximo.',
            'codigo_sis.unique'=>'El código sis debe ser único.',
            'contrasenha.required' => 'La contraseña es obligatoria.',
            'contrasenha.min' => 'La contraseña debe ser al menos de 6 caracteres.'
        ];

        $validator =  Validator::make($request->all(), [
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'apellido_paterno' =>'required|regex:/^[\pL\s\-]+$/u|max:255',
            'apellido_materno' =>'regex:/^[\pL\s\-]+$/u|max:255',
            'alias' => 'max:255|unique:personas',
            'correo' => 'required|email|max:255|unique:personas',
            'codigo_sis' => 'required|numeric|min:9|max:300000000|unique:estudiantes',
            'contrasenha' => 'required|min:6'
        ],$mensajes);
         $codigo = $request->input('codigo_sis');

        if(!(
            ($codigo > 199000000) && 
            ($codigo <= (Carbon::now()->year + 1) *100000)&&
                (
                    ($codigo/10000)%10 == 0 || 
                     ($codigo/10000)%10 == 1
                 )
            )
        ){
        return back()->withInput()->withErrors('El campo código Sis no cumple con el formato, dado por la UMSS, que consiste en: primeros cuatro dígitos un año entre 1990 y '.Carbon::now()->year .', el quinto dígito debe ser 1 o 0. Ejemplo: 199011223');
         } 
        if($validator -> fails()){
           return back()->withInput()->withErrors($validator);
         } else{
            $personaContrasenha = $persona->contrasenha;
            $persona->contrasenha = bcrypt($personaContrasenha);
            $persona->save();

            $estudiante = new Estudiante($request->all());
            $estudiante->persona_id = $persona->id;
           /* $estudiante ->sesion_id = 1;
            $estudiante ->grupomateria_id = 1;*/
            $estudiante->save();

            $portafolio = new Portafolio;
            $portafolio -> estudiante_id= $estudiante -> id;
            $portafolio -> direccion_portafolio = "sdfsdfasd";
            $portafolio->save();
               return redirect('/login')->with("info", "Se ha registrado satisfactoriamente como estudiante. Ya puede entrar al sistema");  
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        return view('estudiante.inicio.show')->with('id',$id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($estudianteId)
    {
        $estudiante = Estudiante::find($estudianteId);
        return view('estudiante.infoEstudiante.editarInfoEstudiante')->with('estudianteId', $estudianteId)->with('estudiante',$estudiante);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $estudianteId)
    {
       $persona = Estudiante::find($estudianteId)->persona;
       $mensajes = [
        'nombre.max' =>'Su nombre no puede ser mayor a :max caracteres.',
        'apellido_paterno.max' => 'Su apellido paterno no puede ser mayor a 255 caracteres.',
        'apellido_materno.max' => 'Su apellido materno no puede ser mayor a 255 caracteres.',
        'correo.email'=>'Su correo debe ser una dirección válida',
        'correo.max'=>'Su correo  no puede ser mas de 255 caracteres.',
        ];

    $validator =  Validator::make($request->all(), [
        'nombre' => 'max:100|regex:/^[\pL\s\-]+$/u|required',
        'apellido_paterno' =>'max:255|regex:/^[\pL\s\-]+$/u|required',
        'apellido_materno' =>'max:255|regex:/^[\pL\s\-]+$/u',
        'correo' => 'required|email|max:255'
    ],$mensajes);
    if($validator -> fails()){
       return back()->withInput()->withErrors($validator);
     } else {
        $persona-> update([
            'apellido_paterno' => $request->get('apellido_paterno'),
            'apellido_materno' => $request->get('apellido_materno'),
            'nombre' => $request->get('nombre'),
            'correo' => $request->get('correo')
        ]);
     }

       
        $persona->save();
        return back()->with('info',"Se actualizó su información correctamente");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
