<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
//use Illuminate\Http\Response;
use App\Http\Requests;
use App\Producto;
use App\SesionProducto;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Storage;


class PortafolioProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $estudId, $idSesionProducto)
    {   
        if ($request->file('file') == null){
            return back()->withErrors(["Usted no ha seleccionado ningún archivo para subir,por favor seleccione un archivo."]); 
        }else {
           
                $file = $request->file('file');
                $nombre = $file->getClientOriginalName();
                $input = array('archivo' => Input::file('file'));
                $reglas = array('archivo' => 'mimes:jpg,png,xls,doc,pdf,zip,rar,txt,docx');
                $validator = Validator::make($input,$reglas);
                if ($validator -> fails()) {
                    return back()->withErrors(["info", "La actividad no fue registrada."]);
                } else {
                    $producto = Producto::find($idSesionProducto) ; 
                    $mytime = Carbon::now();
                    $producto->hora_publicacion = $mytime;
                    $producto->referencia= $nombre;
                   
                    $producto->save();
                    Storage::disk('public')->put($nombre,\File::get($file));
                    return back()->with("info", "La actividad fue registrada con exito."); 
                } 
            }    
                   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($estudianteId,$idSesionProducto,$archivo)
    {
        
        $path = storage_path('ArchivoProductos/').$archivo;
        
        return Response::make(file_get_contents($path), 200, [
          'Content-Type' => 'application/pdf',
          'Content-Type' => 'application/zip',
          'Content-Type' => 'application/rar',
          'Content-Type' => 'text/plain',
          'Content-Type' => 'image/jpeg',
          'Content-Type' => 'image/png',
          'Content-Disposition' => 'attachment; filename="'.$archivo.'"'
        ]);
      
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($estudId, $idSesionProducto)
    {
        $producto = Producto::where('sesionproducto_id','=',$idSesionProducto)->get()->first();
        //dd($productos);
       // $cont=0;
        /*        foreach($productos as $producto){
            if( $producto->referencia ='NULL' ){
               $cont=$cont +1;
               
            } else {
               $cont=0;
            }
        }*/
      
        if($producto-> referencia == 'NULL'){ 
            return back()->with("info", "No existe actividad para ser eliminada.");
        }else{
            $producto->hora_publicacion = 'NULL';
            $producto->referencia= 'NULL';
            $producto->save();
            return back()->with("info", "La actividad se ha eliminado.");
        }
                
                   
                    
            
        //return back()->with("info", "La actividad se ha eliminado.");
    }
}
