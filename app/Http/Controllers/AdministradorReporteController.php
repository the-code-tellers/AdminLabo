<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Gestion;
use App\GrupoMateria;
use App\Sesion;

class AdministradorReporteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $gestions = Gestion::orderBy('anho','DEC')->paginate(7);
        $gestions->each(function($gestions){
        });
        return view('admin.reporte.listaGestiones')->with('id',$id)->with('gestions',$gestions);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $idGestion)
    {
        $gestion = Gestion::find($idGestion);
        $grupoMaterias = GrupoMateria::
        join('materias', 'grupo_materias.materia_id', '=', 'materias.id')
        ->orderBy('nombre_materia','DEC')
        ->where('gestion_id','=',$idGestion)
        ->select('grupo_materias.*','materias.nombre_materia')
        ->get();
        $sesions = Sesion::
        distinct()->select(['horario_id'])
        ->addSelect('grupomateria_id')
        ->addSelect('sesions.auxiliar_id') 
        ->addSelect('horario_id')
        ->get();
        return view('admin.reporte.reporteGeneral')->with('gestion',$gestion)
        ->with('grupoMaterias', $grupoMaterias)->with('sesions', $sesions);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
