<?php

namespace App\Http\Controllers;
	
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Persona;
use App\Docente;
use App\GrupoMateria;
use App\Materia;
use App\Gestion;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class DocenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grupoMaterias = GrupoMateria::all();
        $materia = Materia::all();
        $gestiones = Gestion::all();
        return view('docente.horarios.tablaHorarios')->with('grupoMaterias', $materia)
        ->with('gestiones', $gestiones);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($docenteId)
    {
       
        return view('docente.inicio.show')->with('id',$docenteId);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($docenteId)
    {
        $docente = Docente::find($docenteId);
        return view('docente.infoDocente.editarInfoDoc')->with('docenteId', $docenteId)->with('docente',$docente);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $docenteId)
    {
        $persona = Docente::find($docenteId)->persona;
        $mensajes = [
            'nombre.max' =>'El nombre del docente no puede ser mayor a 100 caracteres.',
            'apellido_paterno.max' => 'El apellido paterno del docente no puede ser mayor a 70 caracteres.',
            'apellido_materno.max' => 'El apellido materno del docente no puede ser mayor a 70 caracteres.',
            'correo.email'=>'El correo debe ser una dirección válida',
            'correo.max'=>'El correo del docente no puede ser mas de 255 caracteres.',
        ];
       $validator =  Validator::make($request->all(), [
        'nombre' => 'max:100|regex:/^[\pL\s\-]+$/u|required',
        'apellido_paterno' =>'max:255|regex:/^[\pL\s\-]+$/u|required',
        'apellido_materno' =>'max:255|regex:/^[\pL\s\-]+$/u',
        'correo' => 'required|email|max:255'
    ],$mensajes);
        if($validator -> fails()){
           return back()->withInput()->withErrors($validator);
         } else {
            $persona-> update([
                'apellido_paterno' => $request->get('apellido_paterno'),
                'apellido_materno' => $request->get('apellido_materno'),
                'nombre' => $request->get('nombre'),
                'correo' => $request->get('correo')
            ]);
        
         }

            $persona->save();
        return back()-> with('info', "Se actualizó su información correctamente");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



   

}