<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Materia;
use App\Docente;
use App\Gestion;
use App\Persona;
use App\Grupo;
use App\Gestions;
use App\GrupoMateria;
use App\Auxiliar;
use \Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class GruposController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idAdmin)
    {
     $materias = Materia::all();
     $docentes = Docente::all();
     $auxiliares = Auxiliar::all();
     
     $fechaAhora = Carbon::now();

    // $arregloGestiones = Gestion::get()->first();
     $gestiones = Gestion::all();
    /* foreach ($gestiones as $gestion){
         if (($gestion->fecha_inicio >= $fechaAhora) &&
             ($gestion->fecha_fin <= $fechaAhora)){
                $arregloGestiones = $gestion;
         }
     }*/

     $arrayIds=array(); 
     $arregloAuxiliares = array();

     foreach ($docentes as $docente) {
        $idDocente = $docente->persona_id;
        array_push($arrayIds,$idDocente);
       // $personas = Persona::find($idDocente);
       // array_push ( $arregloDocentesNombres , $personas);
     }
     $personas = Persona::find($arrayIds);

     foreach ($auxiliares as $auxiliar) {
        $idAuxiliar = $auxiliar->persona_id;
        array_push ( $arregloAuxiliares , $idAuxiliar);
     }
     $auxiliares =Persona::find($arregloAuxiliares);
    
     return view ('grupo.registroGrupo', compact('materias'))->with('arregloDocentesNombres',$personas)->with('arregloAuxiliaresNombres',$auxiliares)->with('idAdmin', $idAdmin)->with('arregloGestiones', $gestiones);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$idAdmin)
    {
       // $numGrupoP =  $request->numero_grupo;
        $idMateriaNombre= Materia::where('nombre_materia','=', $request->get('materia'))
                        ->get()->first()->id;
        $grupoMateriaSelects = GrupoMateria::where('gestion_id','=',$request->get('gestion'))
                              ->where ('materia_id','=',$idMateriaNombre)->get();
        
        $cont=0;
        foreach($grupoMateriaSelects as $grupoMateriaSelect){
            if((string)($grupoMateriaSelect->grupo->numero_grupo) != $request->numero_grupo){
               $cont=$cont +1;
            } else {
               $cont=0;
            }
        }

        if($cont == sizeof($grupoMateriaSelects)){      
            $grupoMateria = new GrupoMateria;
            $grupo = new Grupo;
            $grupo -> numero_grupo= $request->numero_grupo;
            $mensajes = [
                    'materia.required' => 'Seleccionar una materia.',
                    'docente.required' => 'Seleccionr un docente.',
                    'auxiliar.required' => 'Seleccionar un auxiliar.',
                    'gestion.required' => 'Seleccionar una gestion.',
                    'numero_grupo.unique'=>'Este numero ya fue seleccionado, por favor elija otro.',
                    'numero_grupo.required' => 'Es obligatorio colocar un numero al grupo.'
                    
                ];
            $validator =  Validator::make($request->all(), [
                    'materia' => 'required',
                    'docente' => 'required',
                    'auxiliar' => 'required',
                    'gestion' => 'required',
                    'numero_grupo' => 'required',
        
                ],$mensajes);
            if($validator -> fails()){
                return redirect('/admin/administradors/'.$idAdmin.'/grupos/create')->withInput()->withErrors($validator);
            }else{
                   
                $grupo->save();
             //   $nombreMateria =  $request->get('materia');
                $materias = Materia::all()->where('nombre_materia', $request->get('materia'));
                foreach($materias as $materia){
                    $idMateria = $materia ->id;
                }
        
                $grupoMateria->materia_id=$idMateria;
        
                $grupoMateria->grupo_id =$grupo ->id;

                
                $grupoMateria->gestion_id=$request->get('gestion');
                
                $nombreAuxiliar = Auxiliar::all();
                $personas = Persona::all();
                 foreach ($nombreAuxiliar as $auxiliar) {
                    foreach ($personas as $persona) {
                        if ($persona->id == $request->get('auxiliar')){
                          if ($persona ->id== $auxiliar -> persona_id){
                            $idAuxiliar = $auxiliar ->id;
                          }  
                        }     
                    }
                }
        
                $grupoMateria->auxiliar_id=$idAuxiliar;
        
                $nombreDocente = Docente::all();
                 foreach ($nombreDocente as $docente) {
                    foreach ($personas as $persona) {
                        if ($persona->id == $request->get('docente')){
                          if ($persona ->id== $docente -> persona_id){
                            $idDocente = $docente ->id;
                          }  
                        }     
                    }
                }
        
                $grupoMateria->docente_id=$idDocente;
        
                $grupoMateria->save();
            }
            return back()-> with("info", "El grupo se registro con exito.");
        
        } else {
                return back()->withErrors(['Ya existe el número de grupo, por favor ingrese otro número de grupo']);
            }
    }
    



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
