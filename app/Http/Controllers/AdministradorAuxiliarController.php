<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Auxiliar;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdministradorAuxiliarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $auxiliares = Auxiliar::join('personas', 'auxiliars.persona_id', '=', 'personas.id')->orderBy('apellido_paterno', 'asc')->paginate(10);
        return view('admin.listaAuxiliares.mostrarAuxiliares')->with('auxiliares', $auxiliares)->with('adminId',$id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idFalso ,$idAuxiliar)
    {
        $auxiliar = Auxiliar::where('persona_id','=',$idAuxiliar)->get()->first();
        $auxiliar->habilitado = (($request->get('habilitado'.$idAuxiliar) == 'on')? 1 :0);
        $auxiliar->save();
        $habilitado = $auxiliar->habilitado;
        if($request->get('habilitado'.$idAuxiliar) == 'on'){

        return redirect()->back()->with('info','El auxiliar se a habilitado')
        ->with('habilitado',$habilitado)->with('adminId',$idFalso);
        } else{

        return redirect()->back()->with('info','El auxiliar se a deshabilitado')
        ->with('habilitado',$habilitado)->with('adminId',$idFalso);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
