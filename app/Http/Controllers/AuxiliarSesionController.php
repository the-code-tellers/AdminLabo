<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Estudiante;
use App\Sesion;
use App\SesionProducto;
use App\Portafolio;
use App\RevisionAuxiliar;
use App\Http\Requests;
use \Carbon\Carbon;
use App\Http\Controllers\Controller;

class AuxiliarSesionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id, Request $request)
    {
        //vista previa lista de estudiantes
        $fechaElegida = $request->cookie('cookieFechaAuxiliarLista');
        //$fechaElegida = new Carbon("1988-11-01");
        $sesionesAux = Sesion::where('fecha', '=', $fechaElegida) 
                            -> where('auxiliar_id', '=', $id)
                            -> get();  
        return view('auxiliar.listaMaterias')->with('id',$id)-> with('sesionesAux',$sesionesAux);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       /* $this->validate($request,[
            'asistio' = 'required'
        ]);

       
        $estudiantes = Sesion::find($idSesion)->estudiantes;
        return view('auxiliar.listaEstudiantes')->with('estudiantes', $estudiantes)->with('aux',
            $idAuxiliar)->with('sesion',$idSesion);
            */
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idAuxiliar, $idSesion)
    {
        $estudiantesIds = 
		  Estudiante::join('sesion_productos', 'sesion_productos.portafolio_id', '=', 'estudiantes.id')
		  						->where('sesion_productos.sesion_id', '=' ,$idSesion)->select('portafolio_id')->distinct()->get();
        $estudiantes = Estudiante::findMany($estudiantesIds->toArray());
        $asistencias =  [];
        $revisiones = [];
        foreach ($estudiantes as $estudiante) {
        		$idRevision = 
                    SesionProducto::where('sesion_id', '=' ,$idSesion)
                    ->where('portafolio_id', '=', $estudiante->id)->get()->first()
                    ->revisionauxiliar_id;
            $revisiones[$estudiante->id] = $idRevision;
            $asistencias[$estudiante->id] = RevisionAuxiliar::find($idRevision)->asistio;
        }
        return view('auxiliar.listaEstudiantes')->with('estudiantes', $estudiantes)->with('aux',
            $idAuxiliar)->with('sesion',$idSesion)->with('asistencias',$asistencias)->with('revisiones',$revisiones);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idEstudiante, $idSesion)
    {                
        $revisionAuxId = SesionProducto::where('sesion_id', '=', $idSesion)->where('portafolio_id', '=', $idEstudiante)
        					->get()->first()->revisionauxiliar_id;
        $revision = RevisionAuxiliar::find($revisionAuxId);
        $revision->asistio = $request->get('asistio') == 'on'? 1 : 0;
        $revision->save();
        $avance = RevisionAuxiliar::find($revisionAuxId)->first()->asistio;
        //dd("idSES:".$idSesion." IDREV:".$revisionAuxId." IDEST:".$idEstudiante."Asistio: ".$request->get('asistio')."Guardo:".$revision->asistio);
        return back()-> with("info", "La asistencia de ".Estudiante::find($idEstudiante)->persona->nombre." se registró con éxito.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
