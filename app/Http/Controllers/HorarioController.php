<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GrupoMateria;
use App\Materia;
use App\Horario;
use App\Gestion;
use App\Http\Requests;
use App\Http\Requests\StoreHorarioRequest;
use App\Http\Controllers\Controller;
use \Carbon\Carbon;

class HorarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $gestiones = Gestion::all();
        $grupoMaterias = GrupoMateria::all();
        $materia = Materia::all();
        return view('admin.horarios.tablaHorarios')->with('grupoMaterias', $materia)
        ->with('adminId',$id)->with('gestiones', $gestiones);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($adminId)
    {
       $horarios = Horario::all()-> groupBy('dia') ;
        $dias = Horario::dias();
       return view('admin.horarios.crearhorario')->with('adminId', $adminId)->with('horarios', $horarios)->with('dias',$dias);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreHorarioRequest $request)
    {
        $horaEntrada = $request->input('hora_entrada');
        $horaSalida = $request->input('hora_salida');
        $nuevoHorario = new Horario($request->all());
        //$nuevoHorario = Horario::create($request->all());
        $nuevoHorario->hora_entrada= (new Carbon($horaEntrada))->format('Y-m-d H:i:s');
        $nuevoHorario->hora_salida= (new Carbon($horaSalida))->format('Y-m-d H:i:s');

        $horaEntradaFormat = (new Carbon($horaEntrada));

        $entradaEntera = ($horaEntradaFormat -> hour) * 100 + ($horaEntradaFormat -> minute);
        $horaSalidaFormat = (new Carbon($horaSalida));
        $salidaEntera = ($horaSalidaFormat -> hour) * 100 + ($horaSalidaFormat -> minute);

        $limiteManhana = 645;
        $limiteNoche = 2145;
        if($request ->dia ==='Sabado'){
            $limiteNoche = 1245;
        }
        if ($entradaEntera >= $salidaEntera) {
            return back()->withInput()->withErrors(['La hora de entrada debe ser menor a la hora de salida']);
        }
        $limite = round($limiteNoche/100).":".($limiteNoche%100);
        if($entradaEntera < $limiteManhana || $salidaEntera > $limiteNoche){
            return back()->withInput()->withErrors(['La hora no es válida ya que la hora de entrada minima es las 06:45 y la máxima hora de salida '.$limite]);
        }

        if($salidaEntera - $entradaEntera > 300 ){
            return back()->withInput()->withErrors(['La cantidad máxima de horas entre entrada y salida es de 3 horas']);
        }
        $horarios = Horario::all();
        $existeChoque = false;
        foreach ($horarios as $horario) {
            if($horario-> dia == $nuevoHorario-> dia) {
                $horaEntradaNueva = (new Carbon($horario -> hora_entrada));
                $horaSalidaNueva = (new Carbon($horario -> hora_salida));
                $entradaNuevaEntera = ($horaEntradaNueva -> hour) * 100 + ($horaEntradaNueva  -> minute);
                $salidaNuevaEntera = ($horaSalidaNueva-> hour) * 100 + ($horaSalidaNueva -> minute);

              if($entradaEntera > $entradaNuevaEntera && $entradaEntera < $salidaNuevaEntera
                || $salidaEntera > $entradaNuevaEntera && $salidaEntera < $salidaNuevaEntera
                || $entradaEntera <=  $entradaNuevaEntera  && $salidaEntera >= $salidaNuevaEntera){
                    $existeChoque = true;
                    break;
                }
            }
        }
        if (!$existeChoque) {
            $nuevoHorario->save();
            return back()->withInput()->with("info", "El horario se registró exitosamente.");  
        } else{
            return back()->withInput()->withErrors(['El horario ingresado no es válido ya que se sobrepone con un existente.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
