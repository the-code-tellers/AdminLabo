<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\RevisionAuxiliar;
use App\SesionProducto;
use App\Estudiante;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AuxiliarRevisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idAux, $idRev)
    {
      $auxiliarId = $idAux;
      $revisionId = $idRev;     
      $sesionId = RevisionAuxiliar::find($idRev)->sesionProducto ->sesion_id;
      
      $estudiante = RevisionAuxiliar::find($idRev)->sesionProducto->portafolio->estudiante;
        return view('auxiliar.registrarActividad')->with('auxiliarId', $auxiliarId)
        ->with('revisionId', $revisionId)->with('sesionId', $sesionId)
        ->with('estudiante', $estudiante);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idAux, $idRev)
    {
        $revision = RevisionAuxiliar::find($idRev);
        $revision->update($request->all());
        $revision->save();
        return back()-> with("info", "Revisión realizada con éxito!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
