<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GrupoMateria;
use App\Http\Requests;
use App\Sesion;
use App\Http\Controllers\Controller;

class AuxiliarMateriaController extends Controller
{
    public function index($auxiliarId)
    {
        $sesiones =Sesion::where("auxiliar_id","=",$auxiliarId) ->get(["grupomateria_id"])-> toArray();
        $array = array_flatten($sesiones);
        $arrayUnique = array_unique($array);
        $grupoMaterias = GrupoMateria::find($arrayUnique);
        
        if($grupoMaterias->isEmpty()){
            return view('auxiliar.listaMateriasAuxiliarError')->withErrors(['No tienes ninguna materia asignada,
             por favor consulte con el administrador para ser asignado a una.']);
        }
        else{
        return view('auxiliar.listaMateriasAuxiliar')->with('auxiliarId',$auxiliarId)->with('grupoMaterias',$grupoMaterias);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($auxiliarId)
    {
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
