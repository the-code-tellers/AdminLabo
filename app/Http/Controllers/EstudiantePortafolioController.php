<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Producto;
use App\Estudiante;
use App\Persona;
use App\Portafolio;
use App\GrupoMateria;
use App\SesionProducto;
use App\Materia;
use App\Sesion;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;

class EstudiantePortafolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($estudId)
    {
        $materiasConGrupo = SesionProducto::where('portafolio_id','=', $estudId)->join("sesions",
            "sesion_productos.sesion_id", '=', "sesions.id")->
            join("grupo_materias", "sesions.grupomateria_id", '=', "grupo_materias.id")->
            join("materias", "grupo_materias.materia_id", '=', "materias.id")->
            join("grupos", "grupo_materias.grupo_id", '=', "grupos.id")->
            select('grupomateria_id')->distinct()->
            addSelect('nombre_materia')-> 
            addSelect('numero_grupo')->
            addSelect('codigo_materia')->
            addSelect('docente_id')->
            get();

       
         $persona= Estudiante::find($estudId);
         $estudianteid = $persona -> persona_id;
         $estudiante = Persona::find ($estudianteid) ;
        /* $arregloSesionesProducto = array() ;*/
         $arregloSesionesProducto = SesionProducto::where('portafolio_id','=',$estudId)->get(); 
        if($materiasConGrupo->isEmpty()){
          return view('portafolio.portafolioerror', compact('estudiante'))-> with('estudId', 
            $estudId)->withErrors(['Aún no tiene una materia asignada, no puede acceder a su portafolio, por favor inscribase a una materia.']);
        }
        else{
        return view('portafolio.portafolio',compact('estudiante'))
         -> with('estudId', $estudId) 
         -> with (compact('sesionProductoNum')) 
         -> with(compact('arregloSesionesProducto'))
         ->with(compact('materiasConGrupo'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
         
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
