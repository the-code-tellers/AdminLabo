<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Auxiliar;
use App\Sesion;
use App\Estudiante;
use App\Materia;
use App\ GrupoMateria;

class AuxiliarSesionReporteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($idAuxiliar, $idSesion)
    {
        $estudiantesIds = 
		  Estudiante::join('sesion_productos', 'sesion_productos.portafolio_id', '=', 'estudiantes.id')
		  						->where('sesion_productos.sesion_id', '=' ,$idSesion)->select('portafolio_id')->distinct()->get();
        $estudiantes = Estudiante::findMany($estudiantesIds->toArray());
        $sesionInf = Sesion::find($idSesion);
        $auxiliar = Auxiliar::find($idAuxiliar);
        
        return view('auxiliar.reporteSesion.reporteAuxiliarSesion') 
        ->with('idAuxiliar', $idAuxiliar)
        ->with('idSesion', $idSesion)
        ->with('estudiantes', $estudiantes)
        ->with('sesionInf', $sesionInf)
        ->with('auxiliar', $auxiliar);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
