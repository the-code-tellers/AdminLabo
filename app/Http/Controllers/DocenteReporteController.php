<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Docente;
use App\GrupoMateria;
use App\Gestion;
use App\Sesion;
use App\SesionProducto;
use App\Producto;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DocenteReporteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($docenteId,$idGestion,$idMateria)
    {
        $docente= Docente::find($docenteId);
        $grupomateria = GrupoMateria::where('gestion_id','=',$idGestion)->where('materia_id','=',$idMateria)->get()->first();
        

        $gestion = Gestion::find($idGestion);
         
        $sesionesExistentes =Sesion:: where('grupomateria_id', '=',$idMateria)-> get();
        if ( empty($grupomateria)){
            return back() -> withErrors(["Lo sentimos no podemos generar reportes debido a que no existen materias/sesiones en la gestion escogida"]);
        }else{
            if($sesionesExistentes->isEmpty()){
            return back() -> withErrors(["Lo sentimos no podemos generar reportes si no existen sesiones para la materia"]);
            } else {
              foreach($sesionesExistentes as $sesionExistente){
                $portafolios = SesionProducto:: where('sesion_id', '=', $sesionExistente->id)
                -> distinct()
                -> select(['portafolio_id'])-> get();
                }
                if ($portafolios->isEmpty()){
                    return back() -> withErrors(["Lo sentimos no podemos generar reportes si no existen alumnos en esta materia."]);
                } else {
                

                    $productos = Producto::all();

                    return view('docente.reporte.reporteDocente',compact('docente'))
                    -> with('grupomateria',$grupomateria)
                    -> with('gestion',$gestion)
                    -> with('docenteId', $docenteId)
                    -> with('idGestion', $idGestion)
                    -> with('idMateria', $idMateria)
                    -> with ('portafolios',$portafolios) 
                    -> with ('productos',$productos);
                
                }  
            }     
        }
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
