<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use App\Auxiliar;
use App\Http\Requests;
use \Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class AdministradorRegistrarAuxiliarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.registro.registroAuxiliar');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.registro.registroAuxiliar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $persona = new Persona($request->all()); 

        $mensajes = [
            'nombre.required' => 'Agrega el nombre del auxiliar.',
            'nombre.max' =>'El nombre del auxiliar no puede ser mayor a 255 caracteres.',
            'apellido_paterno.required' => 'El apellido paterno del auxiliar es requerido.',
            'apellido_paterno.max' => 'El apellido paterno del auxiliar no puede ser mayor a 255 caracteres.',
            'apellido_materno.required' => 'El apellido materno del auxiliar es requerido.',
            'apellido_materno.max' => 'El apellido materno del auxiliar no puede ser mayor a 255 caracteres.',
            'alias.max' => 'El alias no debe pasar de 255 caracteres.',
            'alias.unique' => 'El alias debe ser unico.',
            'correo.required'=>'El correo es requerido',
            'correo.email'=>'El correo debe ser una dirección válida',
            'correo.max'=>'El correo del auxiliar no puede ser mas de 255 caracteres.',
            'correo.unique'=>'El correo debe ser único.',
            'contrasenha.required' => 'La contraseña es obligatoria.',
            'contrasenha.min' => 'La contraseña debe ser al menos de 6 caracteres.',
            'codigo_sis.required'=>'El código sis es requerido.',
            'codigo_sis.min'=>'El código sis debe tener 9 dígitos como mínimo.',
            'codigo_sis.max'=>'El código sis debe tener 9 dígitos como máximo.',
            'coigo_sis.unique' => 'El código sis debe ser unico.'
        ];
        $validator =  Validator::make($request->all(), [
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'apellido_paterno' =>'required|regex:/^[\pL\s\-]+$/u|max:255',
            'apellido_materno' =>'regex:/^[\pL\s\-]+$/u|max:255',
            'alias' => 'max:255|unique:personas',
            'correo' => 'required|email|max:255|unique:personas',
            'codigo_sis' => 'required|numeric|min:9|max:300000000|unique:estudiantes',
            'contrasenha' => 'required|min:6'
        ],$mensajes);

        $codigo = $request->input('codigo_sis');
        if(!(
            ($codigo > 199000000) && 
            ($codigo <= (Carbon::now()->year + 1) *100000)&&
                (
                    ($codigo/10000)%10 == 0 || 
                     ($codigo/10000)%10 == 1
                 )
            )
        ){
        return back()->withInput()->withErrors('El campo código Sis no cumple con el formato, dado por la UMSS, que consiste en: primeros cuatro dígitos un año entre 1990 y '.Carbon::now()->year .', el quinto dígito debe ser 1 o 0. Ejemplo: 199011223');
         } 
        if($validator -> fails()){
           return back()->withInput()->withErrors($validator);
         }else{
            $personaContrasenha = $persona->contrasenha;
            $persona->contrasenha = bcrypt($personaContrasenha);
            $persona->save();

            $auxiliar = new Auxiliar($request->all());
            $auxiliar->persona_id = $persona->id;
            $auxiliar->habilitado = true;
            $auxiliar->save();
            return back()->with("info", "Se ha registrado satisfactoriamente un auxiliar. Ya puede entrar al sistema");  
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
