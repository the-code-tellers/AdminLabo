<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
use App\Estudiante;
use App\Persona;
use App\SesionProducto;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class EstudianteSesionProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($estudId,$idSesionProducto)
    {
        $persona= Estudiante::find($estudId);
        $estudiante = Persona::find ( $persona -> persona_id) ;
        $productos = Producto:: all();
        $sesionProductoFecha = SesionProducto::find($idSesionProducto)->sesion->fecha;
      

        return view ('portafolio.productosSesion',compact('estudiante'))
        -> with('productos', $productos)
        -> with('estudId', $estudId) 
        -> with('sesionProductoFecha',$sesionProductoFecha)
        -> with('idSesionProducto', $idSesionProducto);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
