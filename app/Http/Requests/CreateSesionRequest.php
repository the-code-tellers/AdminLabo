<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateSesionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'materia' => 'required',
            'horario' => 'required',
            'auxiliar' => 'required',
            'cupo' => 'required|numeric|min:5|max:50',
        ];
    }
    
    public function messages() {
      return [
        'cupo' => 'Olvidaste ingresar el cupo de estudiantes permitidos.',
      ];
    }
}
