<?php

namespace App;

use App\SesionProducto;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
	protected $primaryKey = 'sesionproducto_id';
   	protected $hidden = ['created_at', 'updated_at'];

    protected $nullable = ['hora_publicacion', 'referencia'];


	public function sesionProducto()
	{
		return $this->belongsTo(SesionProducto::class, "sesionproducto_id", "sesionproducto_id");
	}
}
