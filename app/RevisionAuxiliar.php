<?php

namespace App;

use App\Auxiliar;
use App\SesionProducto;
use Illuminate\Database\Eloquent\Model;

class RevisionAuxiliar extends Model
{
    protected $table = 'revision_auxs';

    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = ['auxiliar_id','observacion', 'avances','asistio'];


	public function auxiliar()
	{
		return $this->belongsTo(Auxiliar::class);
	}

	public function sesionProducto()
	{
		return $this->hasOne(SesionProducto::class, 'revisionauxiliar_id');
	}
}
