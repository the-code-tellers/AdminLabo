<?php

namespace App;

use App\GrupoMateria;
use App\Auxiliar;
use App\Horario;
use App\SesionProducto;
use App\Estudiante;
use Illuminate\Database\Eloquent\Model;

class Sesion extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    
    protected $fillable = ['grupomateria_id', 'horario_id', 'auxiliar_id','numero_sesion','fecha', 'cupo'];

    public function grupoMateria()
	{
		return $this->belongsTo(GrupoMateria::class, 'grupomateria_id');
	}

	public function auxiliar()
	{
		return $this->belongsTo(Auxiliar::class);
	}

	public function horario()
	{
		return $this->belongsTo(Horario::class);
	}
	
	public function sesionesProductos()
	{
		return $this->hasMany(SesionProducto::class);
	}
	
	/*public function estudiantes()
	{
		return $this->hasMany(Estudiante::class);
	}*/
	
}
