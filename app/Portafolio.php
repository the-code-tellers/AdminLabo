<?php

namespace App;
use App\Estudiante;
use App\SesionProducto;
use Illuminate\Database\Eloquent\Model;

class Portafolio extends Model
{
   protected $hidden = ['created_at', 'updated_at'];

   protected $fillable = ['direccion_portafolio'];

	public function estudiante()
	{
		return $this->belongsTo(Estudiante::class);
	}
	
	public function sesionesProductos()
	{
		return $this->hasMany(SesionProducto::class, "portafolio_id", "estudiante_id");
	}
	
}
