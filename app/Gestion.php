<?php

namespace App;
use App\GrupoMateria;
use Illuminate\Database\Eloquent\Model;

class Gestion extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = ['numero_gestion', 'anho','fecha_inicio','fecha_fin'];

    public function grupoMaterias()
    {
    	return $this->hasMany(GrupoMateria::class);
    }
}
