<?php

namespace App;

use App\Docente;
use App\SesionProducto;
use Illuminate\Database\Eloquent\Model;

class RevisionDocente extends Model
{
    protected $table = 'revision_docentes';

    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = ['observacion', 'avances', 'docente_id'];


	public function docente()
	{
		return $this->belongsTo(Docente::class);
	}

	public function sesionProducto()
	{
		return $this->belongsTo(SesionProducto::class, "sesionproducto_id");
	}
}
