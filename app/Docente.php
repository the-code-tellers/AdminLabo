<?php

namespace App;
use App\Persona;
use App\GrupoMateria;
use Illuminate\Database\Eloquent\Model;

class Docente extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['id'];

    
    public function persona()
    {
        return $this->belongsTo(Persona::class);
    }

	public function grupoMaterias()
	{
		return $this->hasMany(GrupoMateria::class);
	}

    public function revisionesDocentes()
    {
        return $this->hasMany(RevisionDocente::class, "docente_id");
    }
}
