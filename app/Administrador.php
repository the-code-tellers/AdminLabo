<?php

namespace App;
use App\Persona;
use Illuminate\Database\Eloquent\Model;

class Administrador extends Model
{
	protected $hidden = ['created_at', 'updated_at'];

	public function persona()
	{
		return $this->belongsTo(Persona::class);
	}

}
