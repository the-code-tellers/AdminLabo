<?php


namespace App;

use App\Persona;
use App\Portafolio;
use App\GrupoMateria;
use App\Sesion;
use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = ['codigo_sis'];

    public function persona()
    {
        return $this->belongsTo(Persona::class);
    }

    public function portafolio()
    {
    	return $this->hasOne(Portafolio::class);
    }

    /*public function grupoMateria()
    {
    	return $this->hasOne(GrupoMateria::class);
    }*/

	/*public function sesion()
    {
    	return $this->belongsTo(Sesion::class);
    }*/
}
