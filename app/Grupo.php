<?php

namespace App;

use App\GrupoMateria;
use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = ['numero_grupo'];

	public function grupoMateria()
	{
		return $this->hasOne(GrupoMateria::class);
	}

}
