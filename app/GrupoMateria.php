<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Auxiliar;
use App\Docente;
use App\Estudiante;
use App\Gestion;
use App\Grupo;
use App\Materia;
use App\Sesion;
//use SoftDeletes;

class GrupoMateria extends Model
{
    protected $table = 'grupo_materias';

    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = ['docente_id', 'auxiliar_id','gestion_id', 'grupo_id', 'materia_id'];
    
    //protected $dates = ['deleted_at'];


    public function sesiones() {
      return $this->hasMany(Sesion::class,'grupomateria_id');
    }
    
    /*public function estudiantes() {
      return $this->belongsTo(Estudiante::class);
    }*/
    
    //Belongs To
    
    public function gestion() {
      return $this->belongsTo(Gestion::class);
    }
    
    public function docente() {
      return $this->belongsTo(Docente::class);
    }
    
    public function auxiliar() {
      return $this->belongsTo(Auxiliar::class);
    }
       
    public function grupo() {
      return $this->belongsTo(Grupo::class);
    }
    
    public function materia() {
      return $this->belongsTo(Materia::class);
    }
    
}
