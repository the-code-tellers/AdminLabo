<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\RevisionAuxiliar;
use App\Portafolio;
use App\Producto;
use App\Sesion;

class SesionProducto extends Model
{
    protected $table = 'sesion_productos';
   	protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['portafolio_id', 'revisionauxiliar_id'];
    
    public function productos() {
      return $this->hasMany(Producto::class, "sesionproducto_id");
    }
    
    public function revisionAuxiliar() {
      return $this->belongsTo(RevisionAuxiliar::class, 'revisionauxiliar_id');
    }
    
    public function portafolio() {
      return $this->belongsTo(Portafolio::class, "portafolio_id", "estudiante_id");
    }
    
    public function sesion() {
      return $this->belongsTo(Sesion::class);
    }

    public function revisionDocente() {
      return $this->hasMany(RevisionDocente::class, "sesionproducto_id");
    }
    
}
