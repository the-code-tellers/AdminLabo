<?php

namespace App;
use App\Persona;
use App\GrupoMateria;
use App\Sesion;
use App\RevisionAuxiliar;
use Illuminate\Database\Eloquent\Model;

class Auxiliar extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = ['codigo_sis','habilitado'];

    public function persona()
    {
        return $this->belongsTo(Persona::class);
    }

	public function grupoMaterias()
	{
		return $this->hasMany(GrupoMateria::class);
	}

	public function sesiones()
	{
		return $this->hasMany(Sesion::class);
	}

	public function revisionesAuxiliares()
	{
		return $this->hasMany(RevisionAuxiliar::class);
	}
}
