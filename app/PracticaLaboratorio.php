<?php

namespace App;
use App\Sesion;

use Illuminate\Database\Eloquent\Model;

class PracticaLaboratorio extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = ['enlace'];

    public function sesion()
    {
        return $this->belongsTo(Sesion::class);
    }

}
