<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Administrador;
use App\Estudiante;
use App\Auxiliar;
use App\Docente;


use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Persona extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    
    
    protected $hidden = ['created_at', 'updated_at','contrasenha','remember_token'];
    
    protected $fillable = ['nombre','apellido_paterno','apellido_materno','alias','correo', 'contrasenha'];

    public function administrador()
    {
        return $this->hasOne(Administrador::class);
    }

    public function estudiante()
    {
        return $this->hasOne(Estudiante::class);
    }

    public function auxiliar()
    {
        return $this->hasOne(Auxiliar::class);
    }

    public function docente()
    {
        return $this->hasOne(Docente::class);
    }
    
    public function getAuthPassword()
    { 
        return $this->contrasenha;
    }
}

