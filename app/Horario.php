<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Sesion;

class Horario extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = ['dia', 'hora_entrada','hora_salida'];
    
    public function sesion() {
      return $this->hasOne(Sesion::class);
    }
    
    public static function dias(){
    	return ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];
    }
}
