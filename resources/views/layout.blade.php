<!DOCTYPE html>
<html lang = "en">
<head>
    <meta charset="UTF-8">
        <title>App Name - @yield('title')</title>
        <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/style.css')}}">
        <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/owl.carousel.css')}}">
        <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/owl.theme.default.min.css')}}">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <style>
        footer {
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: #2d2d30;
            color: #f5f5f5;
            padding: 32px;
        }
        footer a {
            color: #f5f5f5;
        }
        footer a:hover {
            color: #777;
            text-decoration: none;
        }
    </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header page-scroll">
                <a class="navbar-brand page-scroll" href="#myPage"><img src="/plugins/bootstrap/images/logo3.png" alt="umss logo"></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">Administrador</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">Docente</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="/auxiliar/auxiliars">Auxiliar</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#portfolio">Estudiante</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    @section('sidebar')
                    
    @show

                
    <div class="container">
        @yield('content')
    </div>
    
    <footer>
        <br><br>
        <div class ="row">
            <div class ="col-sm-6">
                <p><strong>CONTACTO</strong></p>
                <p><span class="glyphicon glyphicon-map-marker"></span> Cochabamba, Bolivia</p>
                <p><span class=" glyphicon glyphicon-user"></span> Administrador: {{App\Administrador:: first()->persona -> apellido_paterno}}
                     {{App\Administrador:: first()->persona -> apellido_materno}} 
                     {{App\Administrador:: first()->persona -> nombre}}</p>
                <p><span class="glyphicon glyphicon-envelope"></span> Correo: {{App\Administrador:: first()->persona -> correo}}</p>
                <p><img src="/plugins/bootstrap/images/cs_logo.jpg" class="img-circle" alt="Cinque Terre" width="30" height="30">
                    <a href="http://www.cs.umss.edu.bo" data-toggle="tooltip" title="Visit cs">www.cs.umss.edu.bo</a></p>
                <p><img src="/plugins/bootstrap/images/websis_logo.png" class="img-circle" alt="Cinque Terre" width="30" height="30">
                    <a href="http://websis.umss.edu.bo/" data-toggle="tooltip" title="Visit cs">websis.umss.edu.bo</a></p>
            </div>
            <div class ="col-sm-6">
                <p><img src="/plugins/bootstrap/images/TheCode_logo.png" class="img-circle" alt="Cinque Terre" width="30" height="30"> <strong>The Code Tellers</strong></p>
                <p><span class="glyphicon glyphicon-map-marker"></span> Av.Gualberto Villarroel Esq.Portales #284 Cochabamba, Bolivia</p>
                <p><span class="glyphicon glyphicon-phone"></span> Telefono: +591-69535257 - +591-70355278</p>
                <p><span class="glyphicon glyphicon-envelope"></span> Correo: thecodetellers@gmail.com</p>
            </div>
        </div>

    </footer>
    </body>
</html> 