@extends('auxiliar.template.auxiliarTemplate')

@section('title','Lista Materias')

@section('sidebar')
  @parent
@endsection
@section('content')
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<title>lista Materia</title>
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
						<h2><b>Lista Materias</b></h2>
                    <div class="col-sm-6">
                        <a href="/auxiliar/auxiliars/{{$auxiliarId}}/sesions" class="btn btn-success" ><span>Ver sesiones</span></a>
                    </div>
					</div>
                </div>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Nombre Materia</th>
                        <th>Nro de Grupo</th>
                    </tr>
                </thead>
               
                <tbody>
				  @foreach($grupoMaterias as $grupoMateria)
                    <tr>
                        <td>{{$grupoMateria -> materia -> nombre_materia}}</td>
						<td>{{$grupoMateria -> grupo -> numero_grupo}}</td>
                    </tr>
				  @endforeach
                </tbody>
            </table>
    </div>
	

	
</div>
