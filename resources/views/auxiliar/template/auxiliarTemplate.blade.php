<!DOCTYPE html>
<html lang = "en">
@include('layouts.head')

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">
<br>
<br>
<br>
<br>
<br>
<br>
<p hidden>{{$id = App\Persona::find(\Auth::user()->id)->auxiliar->id}}
{{$sesion = 1}}
</p>
<!--
<nav class="navbar navbar-inverse navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand page-scroll" href="#myPage"><img src="/plugins/bootstrap/images/lab_logo.png" alt="umss logo"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
	                <li class="hidden">
	                    <a href="#page-top"></a>
	                </li>
	                <li>
	                    <a class="page-scroll" href="/auxiliar/auxiliars/{{$id}}">Inicio</a>
	                </li>

	                <li>
	                    <a class="page-scroll" href="/auxiliar/auxiliars/{{$id}}/edit">Inf. Personal</a>
	                </li>

					<li>
	                    <a class="page-scroll" href="/auxiliar/auxiliars/{{$id}}/materias">Materias</a>
	                </li>

	                <li>
	                    <a class="page-scroll" href="/auxiliar/auxiliars/{{$id}}/horario">Horarios</a>
	                </li>
	                <li>
	                    <a class="page-scroll" href="/auxiliar/auxiliars/{{$id}}/sesions">Sesiones</a>
	                </li>
	                <li class="dropdown">
			          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Actividades
			          <span class="caret"></span></a>
			          <ul class="dropdown-menu">
			            <li><a href="/auxiliar/auxiliars/{{$id}}/archivos/mostrarArchivos/">Prácticas a realizar</a></li>
			          </ul>
			        </li>
	                
	                <li>
	                    <a class="page-scroll" href="/logout">Salir</a>
	                </li>
	    </ul>
    </div>
  </div>
</nav>
-->
<nav class="navbar navbar-default navbar-fixed-top">
	    <div class="container">
	        <div class="navbar-header page-scroll">
	            <a class="navbar-brand page-scroll" href="#myPage"><img src="/plugins/bootstrap/images/lab_logo.png" alt="umss logo"></a>
	        </div>
	        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	            <ul class="nav navbar-nav navbar-right">
	                <li class="hidden">
	                    <a href="#page-top"></a>
	                </li>
	                <li>
	                    <a class="page-scroll" href="/auxiliar/auxiliars/{{$id}}">Inicio</a>
	                </li>

	                <li>
	                    <a class="page-scroll" href="/auxiliar/auxiliars/{{$id}}/edit">Inf. Personal</a>
	                </li>

					<li>
	                    <a class="page-scroll" href="/auxiliar/auxiliars/{{$id}}/materias">Materias</a>
	                </li>
	                
	                <li>
	                    <a class="page-scroll" href="/auxiliar/auxiliars/{{$id}}/horario">Horarios</a>
	                </li>

	                <li>
	                    <a class="page-scroll" href="/auxiliar/auxiliars/{{$id}}/sesions">Sesiones</a>
	                </li>
	                <li class="dropdown">
			          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Actividades
			          <span class="caret"></span></a>
			          <ul class="dropdown-menu">
			            <li><a href="/auxiliar/auxiliars/{{$id}}/archivos/mostrarArchivos/">Prácticas a realizar</a></li>
			          </ul>
			        </li>
	                
	                <li>
	                    <a class="page-scroll" href="/logout">Salir</a>
	                </li>
	            </ul>
	        </div>
	    </div>
	</nav>
	@section('sidebar')
		            
    @show

		        
    <div class="container">
    	<br>
    	<br>
    	<br>
    	<br>
        @yield('content')
        <br>
        <br>
        <br>
    </div>
@include('layouts.footer')
</body>
</html> 