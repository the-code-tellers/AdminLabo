@extends('auxiliar.template.auxiliarTemplate')

@section('title','Registrar Actividad')

@section('sidebar')
  @parent
@endsection

@section('content')

  <style type="text/css">
  div.ridge {border-style: ridge;} 
  </style>
        <div class="row">
          <div class="col-sm-6" >
               <h2><b>Registrar actividad </b></h2>
               <br>
                <a href="/auxiliar/auxiliars/{{$auxiliarId}}/sesions/{{$sesionId}}/edit" type="button" class="btn btn-success">Volver Lista</a>
                 <h2>Tarea de la Sesión Numero: {{App\Sesion::find($sesionId)->numero_sesion}}.</h2>
              <h2>Registrar actividad del estudiante: </h2>
              <h3>{{$estudiante->persona->apellido_paterno.' '.$estudiante->persona->apellido_materno.' '.$estudiante->persona->nombre}} </h3>
              <h3>ID:{{$estudiante->id}}</h3>
              
          </div>
          <div class="col-sm-6" >
          @include('layouts.mensajesDeErrorInfo')
            <h2><b>Observaciones Auxiliar</b></h2>
            <div class="card-body">                    
                    <form action="{{route('auxiliar.auxiliars.revisions.update',['idAuxiliar'=>$auxiliarId, 'idRevision'=> $revisionId])}}"
                          method="POST"
                          autocomplete="off">
                        {{method_field('PUT')}}
                        {{ csrf_field() }}
                        <p hidden>@csrf</p>
                          <div class="form-group">
                            <label for="avance">Avances</label>
                            <textarea class="form-control"
                                      name="avances"
                                      id="avance"
                                      placeholder="Avances"
                                      maxlength="200"
                                      rows="6" required>{{App\RevisionAuxiliar::find($revisionId)->avances}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="observacion">Observaciones</label>
                            <textarea class="form-control"
                                      name="observacion"
                                      id="observacion"
                                      placeholder="Observaciones"
                                      maxlength="200"
                                      rows="6" required>{{App\RevisionAuxiliar::find($revisionId)->observacion}}</textarea>
                        </div>
                        <div class="d-flex justify-content-center">
                          <button type="submit" class="btn btn-success">
                                <i class="fas fa-save"></i> Guardar
                            </button>
                        </div>
                    </form>
                </div>
          </div>
      </div>
@endsection