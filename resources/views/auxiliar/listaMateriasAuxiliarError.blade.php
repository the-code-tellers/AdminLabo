@extends('auxiliar.template.auxiliarTemplate')

@section('title','Lista Materias')

@section('sidebar')
  @parent
@endsection

@section('content')
<div class="row">
  <div class="col-md-10 col-md-offset-1">
    <div style=" border: 1px solid #ccc !important; border-radius: 16px; padding: 16px">
      <h2><b>Lista Materias</b></h2>
        <div class="panel-body" >
          @include('layouts.mensajesDeErrorInfo')
        </div>
      </div>
    </div>
</div>
<br>
<br>
<br>
<br><br>
@endsection