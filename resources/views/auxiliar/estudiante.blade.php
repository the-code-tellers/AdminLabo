<tr>
    <td>{{$no++}}</td>
    <td>
    	<a href="/auxiliar/auxiliars/{{$aux}}/revisions/{{$revisiones[$estudiante->id]}}/edit">{{$estudiante->persona->apellido_paterno.' '.$estudiante->persona->apellido_materno.' '.$estudiante->persona->nombre}} </a> 
    </td>
    <td>
        <form action="{{route('auxiliar.auxiliars.sesions.update',['idEstudiante'=>$estudiante->id, 'idSesion'=> $sesion])}}"
              method="POST"
              autocomplete="off">
              {{method_field('PUT')}}
              {{ csrf_field() }}      
            <div class="form-group">
					   <p><input type="checkbox" name="asistio" id="asistio"  onchange="this.form.submit()" {{$asistencias[$estudiante->id] == 1 ? 'checked="true"' : ''}}>
            		</p>
            </div>
        </form>
    </td>
</tr><tr>