@extends('auxiliar.template.auxiliarTemplate')

@section('title','Lista Materias')

@section('sidebar')
  @parent
@endsection
@section('content')

<p hidden>
@if(empty($_COOKIE['cookieFechaAuxiliarLista']))

  {{setcookie('cookieFechaAuxiliarLista',  Carbon\Carbon::now()->toDateString())}}
  {{$_COOKIE['cookieFechaAuxiliarLista'] = Carbon\Carbon::now()->toDateString()}}

@endif
</p>

<br>
<br>
<br>
<h2><b>Lista de Materias y Sesiones</b></h2>
<p>Elige una fecha donde tienes sesiones, en el formato mes/día/año </p>
<form 
                          method="POST"
                          autocomplete="off">
                          {{csrf_field()}}
                          <br>
    <div class="form-group">
        <h4><span class="label label-default" for="name">Elija la fecha de hoy</span></h4>
	    <input id="fecha" onchange="actualizarCookie('cookieFechaAuxiliarLista', 'fecha', true)" type="date"  date-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" maxlength="100"  name= "fecha_elegida"  value ={{$_COOKIE['cookieFechaAuxiliarLista']}} required >
    </div>
    </form>
    <p hidden>
    {{$fechaCookie = $_COOKIE['cookieFechaAuxiliarLista']}}
    {{$sesionesAux = App\Sesion::where('fecha', '=', $fechaCookie) 
                            -> where('auxiliar_id', '=', $id)
                            -> get()
                            }} </p>
  @if(count($sesionesAux) == 0)
   <div class="alert alert-info" role="alert" id="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
  <label>No existen sesiones en la fecha seleccionada.</label>

</div>
  @else
    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th># </th>
          <th>Sesiones disponibles</th>
        </tr>
      </thead>
      <p hidden>{{$no=1}}</p>
      <tbody>
        @foreach($sesionesAux as $sesion)
            <tr>
            <td>{{$no++}}</td>
            <td>
                <a href="/auxiliar/auxiliars/{{$id}}/sesions/{{$sesion->id}}/edit"> Sesion: {{$sesion->numero_sesion}}  {{$sesion->grupoMateria->materia->nombre_materia}} </a>
            </td>
            </tr>
            <p hidden>{{$no = $no + 1}}</p>
        @endforeach
      </tbody>
    </table>
    @endif
    <script type="text/javascript">
  function actualizarCookie(nombreCookie, elementIdd, siReload ) {
    document.cookie = nombreCookie + '=' + document.getElementById(elementIdd).value;
    if(siReload) {
      location.reload();
    }
  }
</script>
@endsection