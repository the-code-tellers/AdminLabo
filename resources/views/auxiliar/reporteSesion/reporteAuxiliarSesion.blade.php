@extends('auxiliar.template.auxiliarTemplate')

@section('title','Reporte auxiliar')

@section('sidebar')
  @parent
@endsection
@section('content')
<br>
<br>
<h2><b>Reporte de la Sesión {{App\Sesion::find($idSesion)->numero_sesion}}.</b></h2>
<h2><b>Materia: {{$sesionInf->grupoMateria->materia->nombre_materia}}</b></h2>
<h3><b>Grupo: {{$sesionInf->grupoMateria->grupo->numero_grupo}}</b></h3>
<h3><b>Horario: </b></h3> 
<h3>Entrada: {{(new \Carbon\Carbon($sesionInf->horario->hora_entrada))->toTimeString()}}  /
Salida: {{(new \Carbon\Carbon($sesionInf->horario->hora_salida))->toTimeString()}}</h2>

<h4><b>Fecha: {{$sesionInf -> fecha }}</b></h4>
<h3><b>Auxiliar: {{$auxiliar->persona->apellido_paterno.' '.$auxiliar->persona->apellido_materno.' '.$auxiliar->persona->nombre}} </b></h3>
<button class="btn btn-info" onclick="window.print()">
    <a  type="button" style="color:white;"> 
    <i class="material-icons">print</i> 
    <span>Imprimir</span>
    </a>
</button>
<div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-1"></div>
    <div class="col-sm-1"></div>
    <div class="col-sm-8">
    	  <table class="table table-striped table-hover">
                <thead>
                    <tr>
                    	<th>Estudiante</th>
                    	<th>Asistencia</th>
                        <th>Observacion </th>
                        <th>Avances</th>
                    </tr>
                </thead>
                <tbody>
                	@foreach($estudiantes as $estudiante)
                   <p hidden> {{$idRevision = App\SesionProducto:: where('portafolio_id', '=', $estudiante->id) 
                                                        -> where('sesion_id', '=', $idSesion) ->pluck('revisionauxiliar_id') }}
                    {{$revision = App\RevisionAuxiliar::find($idRevision)}} </p>
                   
			 			 <tr>
			 			 	<td>{{$estudiante->persona->apellido_paterno}} {{$estudiante->persona->apellido_materno}} {{$estudiante->persona->nombre}} </td>
			 			 	<td>{{$revision->asistio}}</td>
                            <td>{{$revision->observacion}}</td>
                            <td>{{$revision->avances}}</td>
			 			 	
                            <td>  </td>
			 			 </tr>
			    	@endforeach
                </tbody>
            </table>
    </div>
  </div>


<br>

@endsection