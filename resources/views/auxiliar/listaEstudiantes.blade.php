@extends('auxiliar.template.auxiliarTemplate')

@section('title','Lista Estudiantes')

@section('sidebar')
  @parent
@endsection

@section('content')
  <div>
   <h2><b>Lista de Estudiantes de {{App\Sesion::find($sesion)->grupomateria->materia->nombre_materia}}, Grupo: {{App\Sesion::find($sesion)->grupomateria->grupo->numero_grupo}} </b></h2>
    <h3>Sesion {{App\Sesion::find($sesion)->numero_sesion}} del {{App\Sesion::find($sesion)->fecha}}</h3>
    <p>Son tus estudiantes de la sesión de la fecha que elegiste, marca en el checkbox
     si vinieron y luego podrás colocar observaciones.</p>
     <p>Al final podrás generar un reporte con toda esta información. </p>
   @include('layouts.mensajesDeErrorInfo') 
   <br>
     <div class="col-sm-6">
      <a class="btn btn-success" href='/auxiliar/auxiliars/{{$aux}}/sesions/{{$sesion}}/reporte'>
      <i class="material-icons">&#xE147;</i> <span>Reporte de esta sesión</span></a>
  </div>
  <br>
  <br>       
    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th># </th>
          <th>Nombre del Estudiante</th>
          <th>Asistencia</th>
        </tr>
      </thead>
      <p hidden>{{$no=1}}</p>
      <tbody>
        @foreach($estudiantes as $estudiante)
            @include('auxiliar.estudiante')
            <p hidden>{{$no = $no + 1}}</p>
        @endforeach
      </tbody>
    </table>
  </div>
@endsection
