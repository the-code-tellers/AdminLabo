@extends('auxiliar.template.auxiliarTemplate')

@section('title','Tabla Horarios')

@section('sidebar')
  @parent
@endsection

@section('content')
<h2><b>Horarios</b></h2>
@include('tablaHorarios.materiaDropdown')
@include ('tablaHorarios.tablaHorarios')
@endsection