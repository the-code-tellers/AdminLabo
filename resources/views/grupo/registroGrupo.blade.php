@extends('admin.template.adminTemplate')

@section('title','Registrar Grupo')

@section('sidebar')
  @parent
@endsection

@section('content')
<br>
<br>
 <div class="container mt-5">
      <div class="row">
        <div class="col-12 col-md-6 offset-3">
            <div class="card">
                <h2><b>
                    Registrar Grupo
                  </b>
                </h2>
                   @include('layouts.mensajesDeErrorInfo')
                <div class="card-body">
                      <form action="/admin/administradors/{{$idAdmin}}/grupos"
                          method="POST"
                          autocomplete="off">
                          {{csrf_field()}}
                        <div class="form-group">
                            <h4><span class="label label-default" for="name">Materia</span></h4>
                            <select class="form-control" name="materia">
                               @foreach ($materias as $materia)
                                    <option>{{ $materia -> nombre_materia }}</option>
                               @endforeach                       
                            </select>

                        </div>
                        <div class="form-group">
                            <h4><span class="label label-default" for="category">Docente</span></h4>
                             <select class="form-control" name="docente">
                               @foreach ($arregloDocentesNombres as $docente)
                                    <option  value="{{$docente-> id}}">
                                      {{ $docente -> nombre }}
                                      {{ $docente -> apellido_paterno }}
                                      {{ $docente -> apellido_materno }}
                                      
                                    </option>
                               @endforeach                       
                            </select>
                
                        </div>

                        <div class="form-group">
                            <h4><span class="label label-default" for="category">Auxiliar</span></h4>
                             <select class="form-control" name="auxiliar">
                               @foreach ($arregloAuxiliaresNombres as $auxiliar)
                                    <option value="{{$auxiliar-> id}}">
                                      {{ $auxiliar -> nombre}}
                                      {{ $auxiliar -> apellido_paterno }}
                                      {{ $auxiliar -> apellido_materno }}
                                     </option>
                               @endforeach                       
                            </select>
                        </div>

                        <div class="form-group">
                            <h4><span class="label label-default" for="category">Gestion</span></h4>
                             <select class="form-control" name="gestion">
                                @foreach($arregloGestiones as $gestion)
                                    <option value = {{$gestion->id}}>
                                    
                                      {{ $gestion -> numero_gestion}} {{ $gestion -> anho}}

                                     </option>
                                      
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <h4><span class="label label-default" for="description">Nro De Grupo </span></h4>
                            <input type="number"
                                   min="1"
                                   max="10"
                                   class="form-control"
                                   required
                                   id="numero_grupo"
                                   name="numero_grupo" placeholder="numero de grupo" required>
                        </div>
                        <div class="d-flex justify-content-center">
                               <br>
                               <br>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-success">
                                <i class="fas fa-save"></i> Registrar
                            </button>
                            <button type="button" class="btn btn-danger">
                                <a href="/admin/administradors/{{$idAdmin}}/materias" type="button" class="fas fa-save" style="color:white">Cancelar</a>
                            </button>
                        </div>
                        <br>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection