@extends('admin.template.adminTemplate')

@section('title','Page Title')

@section('sidebar')
  @parent
@endsection

@section('content')
    <h1> Crear Sesion </h1>
    <form method="POST" action="/admin/administradors/{{$adminId}}/sesions" style="width:700px;">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group"><span class="label label-default">Materia y Grupo</span>
        <select name="materia" class="form-control" style="width:200px;">
          <optgroup label="Materias por nombre">   
            @if (!(empty($grupoMaterias)))
              @foreach ($grupoMaterias as $grupoMateria)
                <option>
                  {{ $grupoMateria -> materia -> nombre_materia }} 
                  Grupo: {{ $grupoMateria -> grupo -> numero_grupo }}
                </option>
              @endforeach
            @endif
          </optgroup></select>   
          <input type="hidden" name="grupomateria_id" value="{{ $grupoMateria -> id }}">
                       
                <div
                    class="container"><span class="label label-default">Agregar Sesion</span>
                    <div class="row">                        
                        <div
                            class="col-md-12"><span class="label label-default">Horario</span>
                            <select name="horario" class="form-control" style="width:200px;">
                            <optgroup label="Horarios disponibles">
                              <option>
                                @foreach ($horarios as $horario)
                                  <option>
                                     {{ $horario -> hora_entrada }} {{ $horario -> hora_salida }}
                                  </option>                                  

                                @endforeach
                              </option>
                            </optgroup></select>
                              <input type="hidden" name="horario_id" value="{{ $horario -> id }}">
                            </div>
                    <div
                        class="col-md-12"><span class="label label-default">Auxiliar</span><select name="auxiliar" class="form-control" style="width:200px;">
                        <optgroup label="Auxiliares">
                        @if(!(empty($auxiliares)))
                          @foreach ($auxiliares as $auxiliar)
                            @if ($grupoMateria -> auxiliar_id == $auxiliar -> id )                            
                            <option>
                               {{ $auxiliar -> persona -> nombre }} {{ $auxiliar -> persona -> apellido_paterno }} {{ $auxiliar -> persona -> apellido_materno }}                          
                            </option>
                            @endif

                          @endforeach
                          @endif
                        </optgroup></select>
                            <input type="hidden" name="auxiliar_id" value="{{ $auxiliar -> id }}">
                        </div>
        </div>
        <button class="btn btn-default" type="submit">Agregar</button></div>
        </div>
    </form>
    @if ($errors->any())
      <ul class="alert alert-danger">
        @foreach($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    @endif
@endsection