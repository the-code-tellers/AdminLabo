@extends('admin.template.adminTemplate')

@section('title','Crear Sesiones')

@section('sidebar')
  @parent
@endsection

@section('content')

<p hidden>
@if(empty($_COOKIE['cookieGrupoMateria']))
  {{setcookie('cookieGrupoMateria', $grupoMaterias[0] -> id)}}
  {{setcookie('cookieName', $horarios[0]->dia)}}
  {{$perrito = $horarios[0]->dia}}
  {{setcookie('cookieAuxiliar', $auxiliares[0] -> id)}}
  {{setcookie('cookieGestion', $gestiones[0] -> id)}}
  {{ setcookie('cookieHorario', $horarios[0] -> id) }}
  {{$_COOKIE['cookieGrupoMateria'] = $grupoMaterias[0] -> id}}
  {{$_COOKIE['cookieName']= $horarios[0]->dia}}
  {{$_COOKIE['cookieAuxiliar']= $auxiliares[0] -> id}}  
  {{$_COOKIE['cookieGestion']= $gestiones[0] -> id}}
  {{$_COOKIE['cookieHorario']= $horarios[0] -> id }}
@endif
@if(App\Gestion::find($_COOKIE['cookieGestion']) == null))
  {{setcookie('cookieGestion', $gestiones[0] -> id)}}
  {{$_COOKIE['cookieGestion']= $gestiones[0] -> id}}
@endif
@if(App\GrupoMateria::find($_COOKIE['cookieGrupoMateria']) == null)
  {{setcookie('cookieGrupoMateria', $grupoMaterias[0] -> id)}}
  {{$_COOKIE['cookieGrupoMateria']= $grupoMaterias[0] -> id}}
@endif
</p>
<br>
<br>
<div>
<h1 onload="inicio()"> <b>Crear Sesiones por Gestión</b> </h1>
@include('layouts.mensajesDeErrorInfo')
<p>Crea sesiones automáticamante para toda la gestión seleccionada.El número de la sesiones son asignadas semanalmente.</p>
<p>Los horarios disponibles son aquellos que están libres para el laboratorio 1, que es el único espacio que funciona para estas actividades.</p>
<br>
<p> Las sesiones pueden compartirse por varios docentes, por instrucción del docente al estudiante en la inscripción.</p>
<br>
<form method="POST" action="/admin/administradors/{{$adminId}}/sesions">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="form-group">
    <h4><span class="label label-default">Materia y Grupo</span></h4>
    <select name="materia" id="materia" class="form-control" style="width:320px;"
            onchange="a('cookieGrupoMateria','materia',true);">
      <optgroup label="Materias por nombre">  
        @if (!(empty($grupoMaterias)))   
          @foreach ($grupoMaterias as $grupoMateria)
            <option value="{{$grupoMateria-> id}}"
             @if($_COOKIE['cookieGrupoMateria']== $grupoMateria-> id)
                    selected="selected"
                    @endif>
              {{ $grupoMateria -> materia -> nombre_materia }} 
              Grupo: {{ $grupoMateria -> grupo -> numero_grupo }}
            </option>
          @endforeach
        @endif
      </optgroup>
    </select>
    <br>
      <div class="container">
        <h4><b>Agregar Sesión</b></h4>
        <div class="row">                        
          <div class="col-md-3">
            <h4><span class="label label-default">Día</span></h4>
            <select name="horarioDia" id="horarioDia" class="form-control" style="width:300px;"
                    onchange="a('cookieName','horarioDia', true)">
              <optgroup label="Horarios disponibles">
                  @foreach ($dias as $horarioDia)
                    <option value="{{$horarioDia}}" @if(isset($_COOKIE['cookieName']) && $_COOKIE['cookieName']== $horarioDia )
                    selected="selected"
                    @endif>
                       {{ $horarioDia }}
                    </option>                                  
                  @endforeach
              </optgroup>
            </select>
            <p hidden>{{ $perrito = ($_COOKIE['cookieName'])? $_COOKIE['cookieName'] : 'Lunes' }} </p>
            <input type="hidden" name="dia" value="{{$perrito}}">
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <h4><span class="label label-default">Horarios disponibles</span></h4>
            <select  id ="horario" name="horario" class="form-control" style="width:300px;"
            onchange="a('cookieHorario','horario', true);">
              <optgroup label="Horarios disponibles">
              @if (!(empty($horarios)))
                {{ $horarioActual = App\Horario::all()-> where('dia', $perrito)->first() }}
                @if ($horarioActual)
                  {{ setcookie('cookieHorario', $horarioActual -> id) }}
                @endif
                @foreach ($horarios as $horario)
                  @if($perrito == $horario->dia)
                    <option value="{{$horario-> id}}" @if($_COOKIE['cookieHorario'] == 
                    $horario-> id)  selected @endif>
                      {{$horario-> dia}} : {{ Carbon\Carbon::parse($horario -> hora_entrada)->format('H:i:s') }} - {{Carbon\Carbon::parse($horario -> hora_salida)->format('H:i:s') }}
                    </option>
                  @endif
                @endforeach
              @endif
              </optgroup>
            </select>
          </div>    
        </div>
        <div class = "row">
          <div class="col-md-3">
            <h4><span class="label label-default">Auxiliar</span></h4>
            <select name="auxiliar" id="auxiliar" class="form-control" style="width:300px;"
                    onchange="a('cookieAuxiliar','auxiliar', false);">
              <optgroup label="Auxiliares">
                @foreach ($auxiliares as $auxiliar)                           
                  <option value="{{$auxiliar-> id}}">
                     {{ $auxiliar -> persona -> nombre }} {{ $auxiliar -> persona -> apellido_paterno }} {{ $auxiliar -> persona -> apellido_materno }}                          
                  </option>
                @endforeach
              </optgroup>
            </select>
          </div>
        </div>
        <div class ="row">
          <div class="col-md-3">
            <h4><span class="label label-default">Gestión</span></h4>
            <select name="gestion" id="gestion" class="form-control" style="width:300px;"
                    onchange="a('cookieGestion','gestion', false);">
              <optgroup label="Auxiliares">
                @if(!empty($_COOKIE['cookieGrupoMateria']))
                <p hidden> {{$gestionGrupoMateria = App\GrupoMateria::find($_COOKIE['cookieGrupoMateria'])->gestion}} </p>
                <option value="{{$gestionGrupoMateria-> id}}"> 
                         Gestión {{$gestionGrupoMateria -> numero_gestion }} - {{$gestionGrupoMateria->anho}} 
                </option>
                @endif
              </optgroup>
            </select>
          </div>
        </div>
        <div class ="row">
          <div class="col-md-3">
            <h4><span class="label label-default">Cupo</span></h4>
              <input class="form-control" name="cupo" type="number" min=5 max=50 style="width:125px;">
          </div>
        </div>
        <br>
        <button class="btn btn-success" type="submit">Generar Sesiones</button>
      </div>
  </div>
</form>
</div>
<script type="text/javascript">
  function a(nombreCookie, elementIdd, siReload ) {
    document.cookie = nombreCookie + '=' + document.getElementById(elementIdd).value;
    if(siReload) {
      location.reload();
    }
  }
</script>
@endsection