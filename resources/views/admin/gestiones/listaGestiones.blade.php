@extends('admin.template.adminTemplate')

@section('title','Lista Gestiones')

@section('sidebar')
  @parent
@endsection

@section('content')
<br>
<br>

<title>lista Gestiones</title>
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
						<h2><b>Lista Gestiones</b></h2>
                         @include('layouts.mensajesDeErrorInfo')
            <div class="d-flex justify-content-center">
                <a class="btn btn-success" href="/admin/administradors/{{$adminId}}/gestiones/create" type="button" style="color:white;"> 
                <i class="material-icons">&#xE147;</i> 
                <span>Agregar Gestión </span>
                </a>
               <br>
               <br>
            </div> 
					</div>
                

                 </div>
			</div>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Num. de Gesti&oacuten</th>
                        <th>Año</th>
                        <th>Fecha Inicio (dd/mm/aa)</th>
						<th>Fecha de Culminaci&oacuten (dd/mm/aa)</th>
                    </tr>
                </thead>
                <tbody>
				  @foreach($gestions as $gestion)
                    <tr>
						<td>{{$gestion ->numero_gestion}}</td>
                        <td>{{$gestion ->anho }}</td>
						<td>{{(new \Carbon\Carbon($gestion->fecha_inicio))->format('d-m-Y')}}</td>
						<td>{{(new \Carbon\Carbon($gestion->fecha_fin))->format('d-m-Y')}}</td>
                        <td>
                            <form class="d-inline-flex" method="POST" action="/admin/administradors/{{$adminId}}/gestiones/{{$gestion ->id}}">
										{{csrf_field()}}
										{{method_field('DELETE')}}			
								<button type="submit" class="btn btn-default">
								<i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
								</button>
							</form>
                        </td>
                    </tr>
				  @endforeach
                </tbody>
            </table>
			{!!$gestions->render()!!}
    </div>
	<!-- Delete Modal HTML -->
 
</div>
@endsection
