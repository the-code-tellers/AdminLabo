@extends('admin.template.adminTemplate')

@section('title','Registrar Materia')

@section('sidebar')
  @parent
@endsection

@section('content')
<br>


<form method= 'POST'   action="/admin/administradors/{{$adminId}}/materias" style="max-width:500px;margin:auto">
	{{ csrf_field() }} 
  <h2><b>Registrar Materia</b></h2>
 @include('layouts.mensajesDeErrorInfo')
  <div class="input-container">
  <h4><span class="label label-default"  for="category">Código materia</span></h4>
    <i class="fa fa-user icon"></i>
    <input class="form-control"  type="text" placeholder="Codigo Materia" name="codigo_materia">
  </div>
  
  <div class="input-container">
  <h4><span class="label label-default"  for="category">Nombre materia</span></h4>
    <i class="fa fa-user icon"></i>
    <input class="form-control"  type="text" placeholder="Nombre Materia" name="nombre_materia">
  </div>
  <br>
  
  <button type="submit" class="btn btn-success">Registrar</button>
</form>
<br>
@endsection

