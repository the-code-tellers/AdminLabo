@extends('admin.template.adminTemplate')

@section('title','Materia')

@section('sidebar')
  @parent
@endsection

@section('content')

<h2> <b>Materia:</b> {{$grupoMateria -> materia -> nombre_materia}}. Grupo {{$grupoMateria-> grupo -> numero_grupo}} </h2>
@include('layouts.mensajesDeErrorInfo')

<div class="container" style="padding-bottom: 100px">
<table class="table-condensed" style="width:66%; margin-bottom: 20px;">
  <tr>
    <th><h4><b>Docente:<b></h4></th>
    <td><h4>{{$grupoMateria-> docente -> persona -> nombre}} {{$grupoMateria-> docente -> persona -> apellido_paterno}}
	{{$grupoMateria-> docente -> persona -> apellido_materno}}</h4></td>
  </tr>
  <tr>
    <th><h4><b>Auxiliar:</b></h4></th>
    <td><h4>{{$grupoMateria-> auxiliar -> persona -> nombre}} {{$grupoMateria-> auxiliar -> persona -> apellido_paterno}} 
	{{$grupoMateria-> auxiliar -> persona -> apellido_materno}}</h4></td>
  </tr>
  <tr>
    <th><h4><b>Gestion:</b></h4></th>
    <td><h4>{{$grupoMateria-> gestion -> numero_gestion}} - {{$grupoMateria-> gestion -> anho}}</h4></td>
  </tr>
</table>
<form class="d-inline-flex" method="POST" 
		action="/admin/administradors/{{$adminId}}/materias/{{$materiaId}}/grupomaterias/{{$grupoMateriaId}}">
			{{csrf_field()}}
			{{method_field('DELETE')}}		
			<a href="/admin/administradors/{{$adminId}}/materias"> <button class="btn btn-success" type="button">Volver a la lista de materias</button> </a>
			<button class="btn btn-danger" type="submit">Borrar</button>
</form>
</div>
@endsection