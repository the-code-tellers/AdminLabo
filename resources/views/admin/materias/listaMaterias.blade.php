@extends('admin.template.adminTemplate')

@section('title','Lista Materias')

@section('sidebar')
  @parent
@endsection

@section('content')
<p hidden>
@if(empty($_COOKIE['cookieGestionListaMaterias']))
  {{setcookie('cookieGestionListaMaterias', $gestiones[0] -> id )}}
  {{$_COOKIE['cookieGestionListaMaterias'] = $gestiones[0] -> id}}
@endif
</p>
<br>
<br>
<div class="container">
    <div class="table-wrapper">
        <div class="table-title">
            <div class="row">
	            <div class="col-sm-6">
					<h2><b>Lista Materias</b></h2>
          <p>Elegir la gestión afecta en la selección de los grupos materias de las materias. </p>
					@include('layouts.mensajesDeErrorInfo')
                    <div class="col-sm-6">
				        <a class="btn btn-success" onclick="location.href='/admin/administradors/{{$adminId}}/materias/create'" ><i class="material-icons">&#xE147;</i> <span>Agregar Nueva Materia</span></a>
			        </div>
            
                    <div class="col-sm-6">
				        <a class="btn btn-success" onclick="location.href='/admin/administradors/{{$adminId}}/grupos/create'" ><i class="material-icons">&#xE147;</i> <span>Agregar Nuevo Grupo</span></a>
			        </div>

				</div>
             
		    	
            
        </div>
    </div>

    <div class="form-group" >

          <label for="sel1">Seleccione una gestion:</label>
          <select class="form-control" id="gestion" name="gestion"
          onchange="actualizarCookie('cookieGestionListaMaterias', 'gestion', true)">

          <optgroup label="gestion">
            <option disabled selected value></option>
            @foreach ($gestiones as $gestion)
              <option value={{$gestion->id}}
               @if($_COOKIE['cookieGestionListaMaterias'] == $gestion->id)  selected="selected" @endif>
               Gestion: {{ $gestion -> numero_gestion }}-{{ $gestion -> anho}}</option>
            @endforeach                        
          </optgroup>
         </select>

    </div>


			
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Codigo Materia</th>
            <th>Nombre Materia</th>
            <th>Grupo Materias</th>
        </tr>
        </thead>
        <tbody>
    @foreach($materias as $materia)
        <tr>
            <td>{{$materia ->codigo_materia}}</td>
			<td>{{$materia ->nombre_materia}}</td>
            <td>
               <!-- <a href="#eliminarMateriaModal" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>-->
				
               <select name="grupoMateria" id="grupoMateria" class="form-control" onchange="location = this.value;">
                <optgroup label="Grupo Materias">  
                <option disabled selected value>
                Elija un grupo de esta materia
                </option>
                {{$materiaId = $materia-> id}}
                {{$listaGrupos = App\grupoMateria:: where('materia_id', '=', $materiaId) -> get() }}
                @if(count($listaGrupos) == 0)
                    
                @else
                    @foreach ($listaGrupos as $grupoMateria)
                    @if($grupoMateria->gestion_id == $_COOKIE['cookieGestionListaMaterias'])
                     <option value="/admin/administradors/{{$adminId}}/materias/{{$materia->id}}/grupomaterias/{{$grupoMateria-> id}}"
                        <thead>
                               {{ $grupoMateria -> materia -> nombre_materia }}
                               Grupo: {{ $grupoMateria -> grupo -> numero_grupo }}
                        </thead>     
                    </option>
                    @endif
                    @endforeach 
                @endif
                </optgroup>

            </select>
            </td>
        </tr>
		@endforeach
        </tbody>
    </table>
	{!! $materias->render()!!}

	</div>
</div>


 <script type="text/javascript">
  function actualizarCookie(nombreCookie, elementIdd, siReload ) {
    document.cookie = nombreCookie + '=' + document.getElementById(elementIdd).value;
    if(siReload) {
      location.reload();
    }
  }
</script>

@endsection