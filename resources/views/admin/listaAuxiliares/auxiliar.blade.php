<tr>
  <td>{{$index}}</td>
  <td>{{$auxiliar->persona->apellido_paterno}}</td>
  <td>{{$auxiliar->persona->apellido_materno}}</td>
  <td>{{$auxiliar->persona->nombre}}</td>
  <td>{{$auxiliar->codigo_sis}}</td>
  <td>
    <form 
    action="{{route('admin.administradors.auxiliares.update',
     ['idFalso'=>1,'idAuxiliar'=>$auxiliar->id])}}"
              method="POST"
              autocomplete="off">
              {{method_field('PUT')}}
              {{csrf_field() }}      
            <div class="form-group">
             <p><input type="checkbox" name="habilitado{{$auxiliar->id}}" id="{{$auxiliar->id}}" onchange="this.form.submit()" @if($auxiliar->habilitado == 1) checked @endif>
                </p>
                
            </div>
        </form>
       </form>
  </td>
</tr>