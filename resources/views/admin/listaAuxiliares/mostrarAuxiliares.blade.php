@extends('admin.template.adminTemplate')

@section('title','Mostrar Auxiliares')

@section('sidebar')
  @parent
@endsection

@section('content')
<br>
<p hidden>{{$index = 1}}</p>
	<h2><b>Lista Auxiliares</b></h2>
 @include('layouts.mensajesDeErrorInfo')

<div class="col-sm-6">
  <a class="btn btn-success" href='/admin/administradors/{{$adminId}}/auxiliar/create'>
    <i class="material-icons">&#xE147;</i> <span>Agregar Nuevo Auxiliar</span></a>
</div>
<br>
<br>
	<table class="table table-striped table-hover">
    <thead>
      <tr>
      	<th>#</th>
        <th>Apellido Paterno</th>
        <th>Apellido Materno</th>
        <th>Nombre</th>
        <th>Código sis</th>
        <th>Estado habilitado</th>
      </tr>
    </thead>
    <tbody>
    	@foreach($auxiliares as $auxiliar)
    		@include('admin.listaAuxiliares.auxiliar')
    		<p hidden> {{$index++}}</p>
    	@endforeach
    </tbody>
  </table>
  {!! $auxiliares->render() !!}
@endsection