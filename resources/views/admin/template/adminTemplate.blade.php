<!DOCTYPE html>
<html lang = "en">

@include('layouts.head')
<body>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<p hidden>{{$id = App\Persona::find(\Auth::user()->id)->administrador->id}}
	{{$idGestion=1}} </p>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">
	<nav class="navbar navbar-default navbar-fixed-top">
	    <div class="container">
	        <div class="navbar-header page-scroll">
	            <a class="navbar-brand page-scroll" href="#myPage"><img src="/plugins/bootstrap/images/lab_logo.png" alt="umss logo"></a>
	        </div>
	        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	            <ul class="nav navbar-nav navbar-right">
	                <li class="hidden">
	                    <a href="#page-top"></a>
	                </li>
	                <li>
	                    <a class="page-scroll" href="/admin/administradors/{{$id}}">Inicio</a>
	                </li>
	                <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Horario
                      <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                         <li>
                            <a class="page-scroll" href="/admin/administradors/{{$id}}/horarios/create">Registrar Horario disponible en Laboratorio</a>
                        </li>
                      </ul>
                    </li>

                    <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Auxiliar
                      <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li>
                            <a class="page-scroll" href="/admin/administradors/{{$id}}/auxiliar/create">Registrar Auxiliar</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="/admin/administradors/{{$id}}/auxiliares"> Lista Auxiliares</a>
                        </li>
                        
                      </ul>
                    </li>

                    <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Docente
                      <span class="caret"></span></a>
                      <ul class="dropdown-menu">                        
                        <li>
                            <a class="page-scroll" href="/admin/administradors/{{$id}}/docentes/create">Registrar Docente</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="/admin/administradors/{{$id}}/docentes/"> Lista Docentes</a>
                        </li>
                        
                      </ul>
                    </li>

	                <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Sesiones
                      <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                         <li>
                            <a class="page-scroll" href="/admin/administradors/{{$id}}/sesions/create">Crear Sesión</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="/admin/administradors/{{$id}}/horarios">Horarios de Sesiones</a>
                          </li>
                      </ul>
                    </li>
                     <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Gestiones
                      <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li>
                            <a class="page-scroll" href="/admin/administradors/{{$id}}/gestiones/create">Crear Gestión</a>
                        </li> 
                          <li>
                            <a class="page-scroll" href="/admin/administradors/{{$id}}/gestiones">Lista de gestiones</a>
                        </li>
                        
                      </ul>
                    </li>

				          <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Materia
                      <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li>
                            <a class="page-scroll" href="/admin/administradors/{{$id}}/materias/create">Registrar materia</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="/admin/administradors/{{$id}}/materias">Lista de materias</a>
                        </li>
                        
                        <li>
                            <a class="page-scroll" href="/admin/administradors/{{$id}}/grupos/create">Crear Grupo para materia</a>
                        </li>
                      </ul>
                    </li>

                  <li>
                      <a class="page-scroll" href="/admin/administradors/{{$id}}/reporte">Reporte General</a>
                  </li>
	                <li>
	                    <a class="page-scroll" href="/logout">Salir</a>
	                </li>
	            </ul>
	           
	        </div>
	    </div>
	</nav>
	@section('sidebar')
		            
    @show
		        
    <div class="container">
        @yield('content')
    </div>
    @include('layouts.footer')
   </body>
</html> 