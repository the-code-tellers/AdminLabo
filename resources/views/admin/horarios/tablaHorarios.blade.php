@extends('admin.template.adminTemplate')

@section('title','Tabla Horarios')

@section('sidebar')
  @parent
@endsection

@section('content')
<br>
<h2><b>Horarios de las Sesiones</b></h2>
<br>
<div class="col-sm-6">
  <a class="btn btn-success" href='/admin/administradors/{{$adminId}}/horarios/create'>
    <i class="material-icons">&#xE147;</i> <span>Agregar Horario </span></a>

  <a class="btn btn-success" href='/admin/administradors/{{$adminId}}/sesions/create'>
    <i class="material-icons">&#xE147;</i> <span>Agregar Sesión</span></a>
</div>
<br>
<br>
@include('tablaHorarios.materiaDropdown')
@include ('tablaHorarios.tablaHorarios')
@endsection