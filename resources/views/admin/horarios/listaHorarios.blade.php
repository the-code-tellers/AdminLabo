@extends('admin.template.adminTemplate')

@section('title', ' Crear Horarios')

@section('sidebar')
    @parent
@endsection

@section('content')
{{$id = 1}}
<button style="text-align: center;" class="btn btn-success" type="submit" href= "/admin/administradors/{{$id}}/horarios/create">
Crear Horario</button>
@endsection