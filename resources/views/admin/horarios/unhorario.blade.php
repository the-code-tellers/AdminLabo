 @if(!(empty($horarios[$dia])))
  <p hidden>{{$horarioDias = $horarios[$dia]-> sortBy('hora_entrada')}}</p>
  <div class="col-sm-2">
  <div><p><b>{{$dia}}</b></p></div>
  @foreach( $horarioDias as $horario)
    <div class="row">
    <div class="col-sm-12" style="border: 1px solid #ccc !important; border-radius: 16px; padding: 16px">    
      <b>Entrada:</b> {{(new \Carbon\Carbon($horario->hora_entrada))->toTimeString()}} <br>
      <b>Salida:</b> {{(new \Carbon\Carbon($horario->hora_salida))->toTimeString()}}
    </div>
  </div>
  @endforeach
  </div>  
@else
  <div class="col-sm-2">
  <div><p><b>{{$dia}}</b></p></div>
    <div class="row">
    <div class="col-sm-12" style="border: 1px solid #ccc !important; border-radius: 16px; padding: 16px">    
    </div>
  </div>
  </div>  
@endif


