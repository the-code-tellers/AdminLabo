@extends('admin.template.adminTemplate')

@section('title', ' Crear Horarios')

@section('sidebar')
    @parent
@endsection

@section('content')
<br>
<br> 
<h2><b>Registrar Horario Disponible para Laboratorio</b></h2>

@include('layouts.mensajesDeErrorInfo')
<p>Los horarios que se registran a continuación, son horarios disponibles en el laboratorio, tomar en cuenta que los mismos no definen más que la disponibilidad y no así la creación de una sesión.</p>
<div class="container-table">
  <div class="row" style=" border: 1px solid #ccc !important; border-radius: 16px; padding: 16px">
        <div class="col-sm-6">
          <form method="POST" action="/admin/administradors/{{$adminId}}/horarios">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">   
              <div class="form-group">
                <h4><span class="label label-default">Día:</span></h4>
                <select name="dia" class="form-control">
                  <optgroup label="Dias habiles">
                    @foreach($dias as $dia)
                    <option @if(old('dia') == $dia) selected @endif> {{$dia}} </option>
                    @endforeach
                  </optgroup>
                </select> 
                <h4><span class="label label-default">Hora entrada:</span></h4>
                <input name="hora_entrada" class="form-control" type="time" name="usr_time" value = "{{ old('hora_entrada','')}}">
                <h4><span class="label label-default">Hora salida:</span></h4>
                <input name="hora_salida" class="form-control" type="time" name="usr_time" value = "{{ old('hora_salida','')}}">
                <h4><span class="label label-default">Laboratorio:</span></h4>
                <input value="1" class="form-control" type="number" min="1" max="1" disabled="true">
                <br>
                <div class="col-md-16 text-center"> 
                    <button style="text-align: center;" class="btn btn-success" type="submit">Crear Horario</button>
                </div>
              </div>
          </form>
        </div>
    <div class="col-sm-6" >
      @if(count($horarios) != 0 )
      <div>
        @foreach($dias as $dia)
         @include('admin.horarios.unhorario')
        @endforeach
      </div>
      @else
      <div class="alert alert-info" role="alert" id="alert">
        <label>No hay horarios disponibles registrados para mostrar.</label>
      </div>
      @endif
    </div>
  </div>
</div>
<br>
@endsection