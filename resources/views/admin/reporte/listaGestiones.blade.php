@extends('admin.template.adminTemplate')

@section('title','Lista Gestiones')

@section('sidebar')
  @parent
@endsection

@section('content')
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
						<h2><b>Lista Gestiones</b></h2>
						 @include('layouts.mensajesDeErrorInfo')
					</div>
                 </div>
			</div>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Num. de Gesti&oacuten</th>
                        <th>Año</th>
                        <th>Fecha Inicio</th>
						<th>Fecha de Culminaci&oacuten</th>
                    </tr>
                </thead>
                <tbody>
				  @foreach($gestions as $gestion)
                    <tr>
						<td>{{$gestion ->numero_gestion}}</td>
                        <td>{{$gestion ->anho }}</td>
						<td>{{$gestion ->fecha_inicio}}</td>
						<td>{{$gestion  ->fecha_fin}}</td>
                        <td>	
						<button type="submit" class="btn btn-default">
						<a href="/admin/administradors/{{$id}}/reporte/{{$gestion->id}}" ><i class="material-icons" >library_books</i>Reporte</a>
						</button>
                        </td>
                    </tr>
				  @endforeach
                </tbody>
            </table>
			{!!$gestions->render()!!}
    </div>
 
</div>
@endsection