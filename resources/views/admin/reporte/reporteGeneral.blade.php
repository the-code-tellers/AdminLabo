@extends('admin.template.adminTemplate')

@section('title','Reporte General')

@section('sidebar')
  @parent
@endsection

@section('content')
<br>
<br>
<h2><b>Reporte  {{$gestion->numero_gestion}} - {{$gestion->anho}}</b></h2>
<br>
@if(count($grupoMaterias) == 0)
 <button class="btn btn-info" onclick="window.print()" disabled>
    <a  type="button" style="color:white;"> 
    <i class="material-icons">print</i> 
    <span>Imprimir</span>
    </a>
</button>
<br>
<br>
   <div class="alert alert-danger">
        <ul>  <li><label>No existen un reporte para esta gestión.</label></li>
        </ul>
    </div>
@else
 <button class="btn btn-info" onclick="window.print()">
    <a  type="button" style="color:white;"> 
    <i class="material-icons">print</i> 
    <span>Imprimir</span>
    </a>
</button>
<br>
<br>

  <div class="row" style=" border: 1px solid #ccc !important; border-radius: 16px; padding: 16px">
@foreach($grupoMaterias as $grupoMateria)

<div class="row">
    <div class="col-sm-4"><b>Materia:</b> {{$grupoMateria->nombre_materia}}</div>
</div>
  <div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-8"><b>Grupo:</b>{{$grupoMateria->grupo->numero_grupo}}</div>
  </div>
  <div class="row">
    <div class="col-sm-1" ></div>
    <div class="col-sm-1" ></div>
    <div class="col-sm-8"><b>Docente:</b>  {{$grupoMateria->docente->persona->apellido_paterno}} {{$grupoMateria->docente->persona->apellido_materno}} {{$grupoMateria->docente->persona->nombre}}</div>
  </div>
  <div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-1"></div>
    <div class="col-sm-8"><b>Auxiliar:</b> {{$grupoMateria->auxiliar->persona->apellido_paterno}} {{$grupoMateria->auxiliar->persona->apellido_materno}} {{$grupoMateria->auxiliar->persona->nombre}} </div>
  </div>
  <div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-1"></div>
    <div class="col-sm-8"><b>Sesiones:</b></div>
  </div>

  <div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-1"></div>
    <div class="col-sm-1"></div>
    <div class="col-sm-8">
    	  <table class="table table-striped table-hover">
                <thead>
                    <tr>
                    	<th>Nombre Auxiliar</th>
                    	<th>Día</th>
                        <th>Hora Entrada </th>
                        <th>Hora Salida</th>
                    </tr>
                </thead>
                <tbody>
                	@foreach($sesions as $sesion)
			    		@if($sesion->grupomateria_id == $grupoMateria->id)
			 			 <tr>
			 			 	<td>{{$sesion->auxiliar->persona->apellido_paterno}} {{$sesion->auxiliar->persona->apellido_materno}} {{$sesion->auxiliar->persona->nombre}} </td>
			 			 	<td>{{$sesion->horario->dia}}</td>
			 			 	<td>{{Carbon\Carbon::parse($sesion->horario->hora_entrada)
			 			 		->toTimeString()}}</td>
			 			 	<td>{{Carbon\Carbon::parse($sesion->horario->hora_salida)
			 			 		->toTimeString()}}</td>
			 			 </tr>
			 			@endif
			    	@endforeach
                </tbody>
            </table>
    </div>
  </div>
@endforeach
</div>
@endif
<br>
<br>
@endsection