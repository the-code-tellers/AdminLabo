@extends('admin.template.adminTemplate')

@section('title','Lista Docentes')

@section('sidebar')
  @parent
@endsection

@section('content')
<br>
<br>
<title>lista Docente</title>
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
						<h2><b>Lista Docentes</b></h2>
                        <br>
                        <div class="col-sm-6">
                          <a class="btn btn-success" href='/admin/administradors/{{$adminId}}/docentes/create'>
                            <i class="material-icons">&#xE147;</i> <span>Agregar Nuevo Docente</span></a>
                        </div>
                        <br>
					</div>
                </div>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Apellido Paterno</th>
                        <th>Apellido Materno</th>
						<th>Nombre</th>
                        <th>Correo</th>
                    </tr>
                </thead>
                <tbody>
                    <p hidden>{{$num = 1}}</p>
				  @foreach($docentes as $docente)
                    <tr>
                        <td>{{$num++}}</td>
                        <td>{{$docente -> persona->apellido_paterno}}</td>
						<td>{{$docente -> persona->apellido_materno}}</td>
						<td>{{$docente -> persona->nombre}}</td>
                        <td>{{$docente -> persona->correo}}</td>
                    </tr>
				  @endforeach
                </tbody>
            </table>
			{!! $docentes->render()!!}
    </div>	
</div>
@endsection
