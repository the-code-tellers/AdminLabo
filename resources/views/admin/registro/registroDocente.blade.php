@extends('admin.template.adminTemplate')

@section('title','Registrar Docente')

@section('sidebar')
  @parent
@endsection

@section('content')
<br>
<br>
<form method= 'POST'  action="/admin/administradors/{{$adminId}}/docentes" style="max-width:500px;margin:auto" enctype="multipart/form-data">
	{{ csrf_field() }} 
  <h2><b>Registrar Docente</b></h2>
   @include('layouts.mensajesDeErrorInfo')
   <br>
   <div "col-md-3">
  <div class="input-container">
    <h4><span class="label label-default">Alias</span></h4>
    <i class="fa fa-user icon"></i>
    <input class="form-control" type="text" placeholder="Alias" name="alias" value = "{{ old('alias','')}}">
  </div>
  
  <div class="input-container">
    <h4><span class="label label-default">Nombre</span></h4>
    <i class="fa fa-user icon"></i>
    <input class="form-control" type="text" placeholder="Nombre" name="nombre" value = "{{ old('nombre','')}}">
  </div>
  
  <div class="input-container">
    <h4><span class="label label-default">Apellido Paterno</span></h4>
    <i class="fa fa-user icon"></i>
    <input class="form-control" type="text" placeholder="Apellido Paterno" name="apellido_paterno" value = "{{ old('apellido_paterno','')}}">
  </div>
  
  <div class="input-container">
    <i class="fa fa-user icon"></i>
    <h4><span class="label label-default">Apellido Materno</span></h4>
    <input class="form-control" type="text" placeholder="Apellido Materno" name="apellido_materno" value = "{{ old('apellido_materno','')}}">
  </div>

  <div class="input-container">
    <i class="fa fa-envelope icon"></i>
    <h4><span class="label label-default">Correo</span></h4>
    <input class="form-control" type="text" placeholder="Correo" name="correo" value = "{{ old('correo','')}}">
  </div>
  
  
  <div class="input-container">
    <i class="fa fa-key icon"></i>
    <h4><span class="label label-default">Contraseña</span></h4>
    <input class="form-control" type="password" placeholder="Password" name="contrasenha">
  </div>
</div>
  <br>
  <button type="submit" class="btn btn-success">Registrar</button>
</form>
<br>
@endsection

