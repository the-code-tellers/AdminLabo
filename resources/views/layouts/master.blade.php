<!DOCTYPE html>
<html lang = "en">
@include('layouts.head')
<body>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="page-scroll">
                <a class="navbar-brand page-scroll" href="#myPage"><img src="/plugins/bootstrap/images/lab_logo.png" alt="umss logo"></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>

                    <li>
                        <a class="page-scroll" href="/">Inicio</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="/login">Login</a>
                    </li>
                    <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Registrarse
                      <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li>
                            <a class="page-scroll" href="/estudiante/estudiantes/create">Estudiante</a>
                        </li>
                      </ul>
                    </li>
                    
                </ul>
            </div>
        </div>
    </nav>

    @section('sidebar')
    @show
        @yield('content')
    </div>
    @include('layouts.footer')
    </body>
</html> 