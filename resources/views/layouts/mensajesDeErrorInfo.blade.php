@if (session('info'))
<div class="alert alert-info" role="alert" id="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
  <label>{{ session('info') }}</label>

</div>
@endif
<br>
@if (count($errors) > 0)
    <div class="alert alert-danger">
    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <ul>
            @foreach ($errors->all() as $error)
                <li><label>{{ $error }}</label></li>
            @endforeach
        </ul>
    </div>
@endif