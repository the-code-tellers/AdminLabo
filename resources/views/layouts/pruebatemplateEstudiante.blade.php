@extends('estudiante.template.estudianteTemplate')

@section('title', 'Page Title')

@section('sidebar')
    @parent
@endsection

@section('content')
<br>
    <div class="container-table">
      <div class="row" style=" border: 1px solid #ccc !important; border-radius: 16px; padding: 16px">
        <div "col-md-3">
          <form method="POST" action="/admin/administradors/{{$adminId}}/horarios">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">   
              <div class="form-group">
                <h2><span class="label label-primary">Dia:</span></h1>
                <select name="dia" class="form-control" type="time" name="usr_time">
                  <optgroup label="Dias habiles">
                      <option> Lunes </option>
                      <option> Martes </option>
                      <option> Miercoles </option>
                      <option> Jueves </option>
                      <option> Viernes </option>
                      <option> Sabado </option>
                  </optgroup>
                </select> 
                <h2><span class="label label-primary">Hora entrada:</span></h1>
                <input name="hora_entrada" class="form-control" type="time" name="usr_time">
                <h2><span class="label label-primary">Hora salida:</span></h1>
                <input name="hora_salida" class="form-control" type="time" name="usr_time">
                <h2><span class="label label-primary">Laboratorio:</span></h1>
                <input value="1" class="form-control" type="number" min="1" max="1" disabled="true">
                <br>
                <div class="col-md-16 text-center"> 
                    <button style="text-align: center;" class="btn btn-success" type="submit">Crear Horario</button>
                </div>
              </div>
          </form>
        </div>
      </div>
    </div>
@endsection