<head>
  <meta charset="UTF-8">
    <title>Sis Lab - @yield('title')</title>
  <link rel ="icon" href = "/plugins/bootstrap/images/lab_logo.png">  
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/owl.carousel.css')}}">
    <!--<link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/animate.css')}}"> -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/owl.theme.default.min.css')}}">
    <!--Otros links-->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

 <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/style.css')}}">
</head>