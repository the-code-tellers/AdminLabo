@extends('layouts.master')

@section('title','Login')

@section('sidebar')
  @parent
@endsection

@section('content')  
<style type="text/css">
    .parallax {
  /* The image used */
  background-image: url("/plugins/bootstrap/images/Central1-MatematicasUMSS.jpg");

  /* Set a specific height */
  min-height: 100px; 

  /* Create the parallax scrolling effect */
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}
</style>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<div> 
    <div class="row">
    <div class="col-sm-4" align="center">
        <img src="/plugins/bootstrap/images/tecno_logo.jpg" width="150" height="150">
    </div>
    <div class="col-sm-4 " align="center" >
        <h1><b>SISTEMA DE ADMINISTRACION DE SESIONES DE LABORATORIO</b></h1>   
    </div>
    <div class="col-sm-4" align="center">
         <img src="/plugins/bootstrap/images/cs_logo.jpg" width="150" height="150">
    </div>
    </div>
    <br>
    <br>
<div class ="container">
    Como es sabido en la actualidad la informática es ubícua, esta característica
    hace que muchas personas quieran tener sus negocios, tareas administrativas;
    sistematizadas en un sistema computacional. Muchas de ellas emprenden este
    trabajo de forma individual, pensando que es una tarea en la que se puede ensamblar una y otra parte y conseguir lo que se busca, esta es una idea muy
    recurrente hoy en día (gracias la desarrollo de herramientas, frameworks, CMS,
    etc.); lo que hace que el trabajo de los profesionales que se dedican a generar
    soluciones informáticas sea más desafiante, más pertinente, con mayor argumentación y que sean el resultado de un proceso fino de diseño e implementación.
</div>
<br>
<br>
<div class ="container">
   En resumen, mostrar la diferencia entre un profesional de grado de licenciatura
    con un buen emprendedor y amante de la informática.
    Una rama de la Informática es el desarrollo de sistemas de información, la
    misma que ha sido desarrollada de manera general en el apoyo a actividades del
    quehacer empresarial/institucional. El desarrollo de estos sistemas dependiendo
    de la complejidad y alcance han generado gran avance en las áreas subyascentes
    como el estudio de los sistemas de información, los procesos de desarrollo y la
    ingeniería de software. La posibilidad de tener sistemas que se desempeñen utilizando la gran red de computadoras ha ocasionado que la informática incursione
    en las complejidades de las aplicaciones web.
</div>
<br>
<br>
<div class ="container">
    La misión más importante de la universidad es la formación de profesionales
    idóneos en sus áreas de competencia. Por lo que se plantea un problema que
    permita mejorar, adecuar e implantar un producto software, con estándares de
    calidad y que permitan a las grupo empresas tener una experiencia enriquecedora en su desempeño empresarial.
</div>
</div>
<br>
<br>
<div class="parallax"></div>
<br>
@endsection