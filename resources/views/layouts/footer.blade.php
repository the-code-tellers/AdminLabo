    <style>
        footer {
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: #2d2d30;
            color: #f5f5f5;
            padding: 32px;
        }
        footer a {
            color: #f5f5f5;
        }
        footer a:hover {
            color: #777;
            text-decoration: none;
        }
      /*    footer {
            background-color: #2d2d30;
            color: #f5f5f5;
            padding: 32px;
          }
          footer a {
            color: #f5f5f5;
          }
          footer a:hover {
            color: #777;
            text-decoration: none;
          }*/
    </style>
    <footer>
        <br><br>
        <div class ="row">
            <div class ="col-sm-6">
                <h4><strong>CONTACTO</strong></h4>
                <p><span class="glyphicon glyphicon-map-marker"></span> Cochabamba, Bolivia</p>
                <p><span class=" glyphicon glyphicon-user"></span> Administrador: 
                 @if(!empty(App\Administrador:: first())) 
                 {{App\Administrador:: first()->persona -> apellido_paterno}}
                 {{App\Administrador:: first()->persona -> apellido_materno}} 
                 {{App\Administrador:: first()->persona -> nombre}}
                 </p>
                <p><span class="glyphicon glyphicon-envelope"></span> Correo: {{App\Administrador:: first()->persona -> correo}}</p>
                 @else
                 <p>No hay contacto</p>
                 @endif
                <p><img src="/plugins/bootstrap/images/cs_logo.jpg" class="img-circle" alt="CS logo" width="30" height="30">
                    <a href="http://www.cs.umss.edu.bo" data-toggle="tooltip" title="Visit cs">www.cs.umss.edu.bo</a></p>
                <p><img src="/plugins/bootstrap/images/websis_logo.png" class="img-circle" alt="WEBSIS logo" width="30" height="30">
                    <a href="http://websis.umss.edu.bo/" data-toggle="tooltip" title="Visit cs">websis.umss.edu.bo</a></p>
            </div>
            <div class ="col-sm-6">
                <h4><strong>EMPRESA</strong></h4>
                <p><img src="/plugins/bootstrap/images/TheCode_logo.png" class="img-circle" alt="CodeTellers logo" width="30" height="30"> <strong>The Code Tellers&#174;</strong></p>
                <p><span class="glyphicon glyphicon-map-marker"></span> Av.Gualberto Villarroel Esq.Portales #284 Cochabamba, Bolivia</p>
                <p><span class="glyphicon glyphicon-phone"></span> Telefono: +591-69535257 - +591-70355278</p>
                <p><span class="glyphicon glyphicon-envelope"></span> Correo: thecodetellers@gmail.com</p>
            </div>
        </div>
        <div class ="row">
            <p class ="text-center">última actualización {{Carbon\Carbon::now()}}</p>
        </div>

    </footer>