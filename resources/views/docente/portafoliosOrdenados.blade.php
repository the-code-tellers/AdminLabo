@extends('docente.template.docenteTemplate')

@section('title','Lista Estudiantes')

@section('sidebar')
  @parent
@endsection

@section('content')
<p hidden>
@if(empty($_COOKIE['cookieGestion']))
  {{setcookie('cookieMateriaDocenteId', $gestiones[0] -> id)}}
  {{setcookie('cookieGestion', $gestiones[0] -> id )}}
  {{$_COOKIE['cookieMateriaDocenteId'] = $gestiones[0] -> id}}
  {{$_COOKIE['cookieGestion'] = $gestiones[0] -> id}}
@endif

</p>
  <div class="container">
    <br>


      <h2><b> Bienvenido a su Lista de estudiantes:</b></h2> 
      <h4>  {{ $docente -> persona -> nombre }} {{   $docente -> persona ->apellido_paterno }} {{ $docente ->persona-> apellido_materno}}</h4>   
      <br>
      @include('layouts.mensajesDeErrorInfo')
       <form action="/docente/docentes/{{$idDocente}}/portafolio/" method="POST"
                          autocomplete="off">
                          {{csrf_field()}}
          <button type="submit" class="btn btn-default" >
             <a href= " /docente/docentes/{{$idDocente}}/gestion/{{$_COOKIE['cookieGestion']}}/grupomateria/{{$_COOKIE['cookieMateriaDocenteId']}}/reporte"> <i class="material-icons" >library_books</i>Reporte</a>
          </button>
          <div class="form-group" >

          <label for="sel1">Seleccione una gestion:</label>
          <select class="form-control" id="gestion" name="gestion"
          onchange="actualizarCookie('cookieGestion', 'gestion', true)">

          <optgroup label="gestion">
            <option disabled selected value>Elija una gestión</option>
            @foreach ($gestiones as $gestion)
              <option value={{$gestion->id}}
               @if($_COOKIE['cookieGestion'] == $gestion->id)  selected="selected" @endif>
               Gestion:{{ $gestion -> numero_gestion }}-{{ $gestion -> anho}}</option>
            @endforeach                        
          </optgroup>
         </select>

         <br>
         <p hidden>
         {{$materiasDocente = App\GrupoMateria::  where('gestion_id', '=', $_COOKIE['cookieGestion'])
                                  -> where('docente_id', '=', $idDocente)
                                  -> get() }}
                                  </p>
            @if (count($materiasDocente ) == 0)
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
              <ul> No existen materias en esta gestion.
              </ul>
          </div>
           @else   
          <label for="sel1">Seleccione una materia:</label>
          <select class="form-control" id="materia" name="materia"
          onchange="actualizarCookie('cookieMateriaDocenteId', 'materia', true)">
          <optgroup label="materia">
           
             <option disabled selected value>Elija una materia</option>
            @foreach ($materiasDocente as $grupoMateria)
              <option value={{$grupoMateria->id}}
              @if($_COOKIE['cookieMateriaDocenteId'] == $grupoMateria->id ) selected="selected" @endif> 
                
              {{ $grupoMateria -> materia -> nombre_materia }} Grupo:{{ $grupoMateria -> grupo -> numero_grupo }}
            </option>
            @endforeach                        
          </optgroup>
         </select>

         <p hidden> {{
           $sesionesExistentes = App\Sesion:: where('grupomateria_id', '=', $_COOKIE['cookieMateriaDocenteId'])
                        -> get()
         }}

         </p>
         <p hidden>
          @foreach($sesionesExistentes as $sesionExistente)
           
        {{  $portafolios = App\SesionProducto:: where('sesion_id', '=', $sesionExistente->id)
          -> distinct()
          -> select(['portafolio_id'])-> get()
          }}
          @endforeach
        
         </p>
          @if (count($sesionesExistentes) == 0)
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
              <ul> No existen Portafolios para esta materia.
              </ul>
          </div>
          @else
           @if (count($portafolios) == 0)
            <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
              <ul> No existen alumnos inscritos en esta materia.
              </ul>
            </div>
            @else
          <table class="table table-bordered">
            <thead>
              <tr>
                 <th>Portafolio </th>
                 <th>Apellido Paterno </th>
                 <th>Apellido Materno</th>
                 <th>Nombre</th>
              </tr>
            </thead>
          <tbody>
              @foreach($portafolios as $portafolio)
              
                    <p hidden>{{$estudiantes=  App\Estudiante::where('id', '=', $portafolio->portafolio_id  )-> get()}}  </p>
                  
                    <tr>
                      @foreach ($estudiantes as $estudiante)
                      <td>
                      <p hidden>{{$materiaIdEst = $_COOKIE['cookieMateriaDocenteId']}} </p>
                      <a href="/docente/docentes/{{$idDocente}}/estudiante/{{$estudiante->id}}/portafolio/{{$materiaIdEst}}"> Portafolio</a>
                     
                     </td>
                     <td>{{$estudiante -> persona -> apellido_paterno}}  </td>
                     <td>{{$estudiante -> persona -> apellido_materno}} </td>
                     <td>{{$estudiante -> persona -> nombre}} </td>
                     
                     @endforeach
                     
                    </tr>
              
              @endforeach

          </tbody>
          </table>
            @endif
          @endif
         @endif
      </div>
    </form>
  </div>

  <script type="text/javascript">
  function actualizarCookie(nombreCookie, elementIdd, siReload ) {
    document.cookie = nombreCookie + '=' + document.getElementById(elementIdd).value;
    if(siReload) {
      location.reload();
    }
  }
</script>
@endsection
