@extends('docente.template.docenteTemplate')

@section('title','Crear Sesiones')

@section('sidebar')
  @parent
@endsection

@section('content')

<p hidden>
@if(empty($_COOKIE['cookieGrupoMateriaDocSes']))
  {{setcookie('cookieGrupoMateriaDocSes', $grupoMaterias[0] -> id)}}
  {{setcookie('cookieNameDocSes', $horarios[0]->dia)}}
  {{$perrito = $horarios[0]->dia}}
  {{setcookie('cookieAuxiliarDocSes', $auxiliares[0] -> id)}}
  {{setcookie('cookieGestionDocSes', $gestiones[0] -> id)}}
  {{ setcookie('cookieHorarioDocSes', $horarios[0] -> id) }}
  {{$_COOKIE['cookieGrupoMateriaDocSes'] = $grupoMaterias[0] -> id}}
  {{$_COOKIE['cookieNameDocSes']= $horarios[0]->dia}}
  {{$_COOKIE['cookieAuxiliarDocSes']= $auxiliares[0] -> id}}  
  {{$_COOKIE['cookieGestionDocSes']= $gestiones[0] -> id}}
  {{$_COOKIE['cookieHorarioDocSes']= $horarios[0] -> id }}
@endif
@if(App\Gestion::find($_COOKIE['cookieGestionDocSes']) == null))
  {{setcookie('cookieGestionDocSes', $gestiones[0] -> id)}}
  {{$_COOKIE['cookieGestionDocSes']= $gestiones[0] -> id}}
@endif
@if(App\GrupoMateria::find($_COOKIE['cookieGrupoMateriaDocSes']) == null)
  {{setcookie('cookieGrupoMateriaDocSes', $grupoMaterias[0] -> id)}}
  {{$_COOKIE['cookieGrupoMateriaDocSes']= $grupoMaterias[0] -> id}}
@endif
</p>
<br>
<br>
<div>
<h1 onload="inicio()"> <b>Crear Sesiones por Gestión</b> </h1>
@include('layouts.mensajesDeErrorInfo')
<p>Crea sesiones automáticamante para toda la gestión seleccionada. El número de la sesiones son asignadas semanalmente.</p>
<p>Los horarios disponibles son aquellos que están libres para el laboratorio 1, que es el único espacio que funciona para estas actividades.</p>
<br>
<p>Las materias para las cuales puede crear sesiones se muestran aquí y son únicamente las que el administrador le asignó.</p>
<br>
<p> Las sesiones pueden compartirse por varios docentes, por instrucción del docente al estudiante en la inscripción.</p>
<br>
<form method="POST" action="/docente/docentes/{{$docenteId}}/sesions">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="form-group">
    <h4><span class="label label-default">Materia y Grupo</span></h4>
    <select name="materia" id="materia" class="form-control" style="width:320px;"
            onchange="a('cookieGrupoMateriaDocSes','materia',true);">
      <optgroup label="Materias por nombre">  
        @if (!(empty($grupoMaterias)))   
          @foreach ($grupoMaterias as $grupoMateria)
            <option value="{{$grupoMateria-> id}}"
             @if($_COOKIE['cookieGrupoMateriaDocSes']== $grupoMateria-> id)
                    selected="selected"
                    @endif>
              {{ $grupoMateria -> materia -> nombre_materia }} 
              Grupo: {{ $grupoMateria -> grupo -> numero_grupo }}
            </option>
          @endforeach
        @endif
      </optgroup>
    </select>
    <br>
      <div class="container">
        <h4><b>Agregar Sesión</b></h4>
        <div class="row">                        
          <div class="col-md-3">
            <h4><span class="label label-default">Día</span></h4>
            <select name="horarioDia" id="horarioDia" class="form-control" style="width:300px;"
                    onchange="a('cookieNameDocSes','horarioDia', true)">
              <optgroup label="Horarios disponibles">
                  @foreach ($dias as $horarioDia)
                    <option value="{{$horarioDia}}" @if(isset($_COOKIE['cookieNameDocSes']) && $_COOKIE['cookieNameDocSes']== $horarioDia )
                    selected="selected"
                    @endif>
                       {{ $horarioDia }}
                    </option>                                  
                  @endforeach
              </optgroup>
            </select>
            <p hidden>{{ $perrito = ($_COOKIE['cookieNameDocSes'])? $_COOKIE['cookieNameDocSes'] : 'Lunes' }} </p>
            <input type="hidden" name="dia" value="{{$perrito}}">
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <h4><span class="label label-default">Horarios disponibles</span></h4>
            <select  id ="horario" name="horario" class="form-control" style="width:300px;"
            onchange="a('cookieHorarioDocSes','horario', true);">
              <optgroup label="Horarios disponibles">
              @if (!(empty($horarios)))
                {{ $horarioActual = App\Horario::all()-> where('dia', $perrito)->first() }}
                @if ($horarioActual)
                  {{ setcookie('cookieHorarioDocSes', $horarioActual -> id) }}
                @endif
                @foreach ($horarios as $horario)
                  @if($perrito == $horario->dia)
                    <option value="{{$horario-> id}}" @if($_COOKIE['cookieHorarioDocSes'] == 
                    $horario-> id)  selected @endif>
                      {{$horario-> dia}} : {{ Carbon\Carbon::parse($horario -> hora_entrada)->format('H:i:s') }} - {{Carbon\Carbon::parse($horario -> hora_salida)->format('H:i:s') }}
                    </option>
                  @endif
                @endforeach
              @endif
              </optgroup>
            </select>
          </div>    
        </div>
        <div class = "row">
          <div class="col-md-3">
            <h4><span class="label label-default">Auxiliar</span></h4>
            <select name="auxiliar" id="auxiliar" class="form-control" style="width:300px;"
                    onchange="a('cookieAuxiliarDocSes','auxiliar', false);">
              <optgroup label="Auxiliares">
                @foreach ($auxiliares as $auxiliar)                           
                  <option value="{{$auxiliar-> id}}">
                     {{ $auxiliar -> persona -> nombre }} {{ $auxiliar -> persona -> apellido_paterno }} {{ $auxiliar -> persona -> apellido_materno }}                          
                  </option>
                @endforeach
              </optgroup>
            </select>
          </div>
        </div>
        <div class ="row">
          <div class="col-md-3">
            <h4><span class="label label-default">Gestión</span></h4>
            <select name="gestion" id="gestion" class="form-control" style="width:300px;"
                    onchange="a('cookieGestionDocSes','gestion', false);">
              <optgroup label="Auxiliares">
                @if(!empty($_COOKIE['cookieGrupoMateriaDocSes']))
                <p hidden> {{$gestionGrupoMateria = App\GrupoMateria::find($_COOKIE['cookieGrupoMateriaDocSes'])->gestion}} </p>
                <option value="{{$gestionGrupoMateria-> id}}"> 
                         Gestión {{$gestionGrupoMateria -> numero_gestion }} - {{$gestionGrupoMateria->anho}} 
                </option>
                @endif
              </optgroup>
            </select>
          </div>
        </div>
        <div class ="row">
          <div class="col-md-3">
            <h4><span class="label label-default">Cupo</span></h4>
              <input class="form-control" name="cupo" type="number" min=5 max=50 style="width:125px;">
          </div>
        </div>
        <br>
        <button class="btn btn-success" type="submit">Generar Sesiones</button>
      </div>
  </div>
</form>
</div>
<script type="text/javascript">
  function a(nombreCookie, elementIdd, siReload ) {
    document.cookie = nombreCookie + '=' + document.getElementById(elementIdd).value;
    if(siReload) {
      location.reload();
    }
  }
</script>
@endsection