@extends('docente.template.docenteTemplate')

@section('title','Page Title')

@section('sidebar')
  @parent
@endsection

@section('content')

<table class="table table-striped">
    <thead>
        <tr>
          <td>Nombre</td>
          <td>Apellido Paterno</td>
          <td>Apellido Materno</td>
          <td>Correo</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody
    @foreach($shares as $share)
        <tr>
            <td>{{$share->id}}</td>
            <td>{{$share->share_name}}</td>
            <td>{{$share->share_price}}</td>
            <td>{{$share->share_qty}}</td>
            <td><a href="{{ route('docentes.edit',$docente->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
         </tr>
         @endforeach
    </tbody>
</table>
@endsection