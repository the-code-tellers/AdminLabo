<!DOCTYPE html>
<html lang = "en">
@include('layouts.head')
<body>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
<p hidden>	{{$id = App\Persona::find(\Auth::user()->id)->docente->id}}</p>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">
	<nav class="navbar navbar-default navbar-fixed-top">
	    <div class="container">
	        <div class="navbar-header page-scroll">
	            <a class="navbar-brand page-scroll" href="#myPage"><img src="/plugins/bootstrap/images/lab_logo.png" alt="umss logo"></a>
	        </div>
	        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	            <ul class="nav navbar-nav navbar-right">
	                <li class="hidden">
	                    <a href="#page-top"></a>
	                </li>
	                <li>
	                    <a class="page-scroll" href="/docente/docentes/{{$id}}">Inicio</a>
	                </li>
	                <li>
	                    <a class="page-scroll" href="/docente/docentes/{{$id}}/edit">Inf. Personal</a>
	                </li>
					        <li>
	                    <a class="page-scroll" href="/docente/docentes/{{$id}}/horario">Horarios</a>
	                </li>
                  <li>
	                    <a class="page-scroll" href="/docente/docentes/{{$id}}/sesions/create">Registrar Sesiones</a>
	                </li>
	                <li>
	                    <a class="page-scroll" href="/docente/docentes/{{$id}}/portafolio">Mis Estudiantes</a>
	                </li>
					

	               <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Mis Archivos
                      <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li>
                            <a class="page-scroll" href="/docente/docentes/{{$id}}/archivos">A subir</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="/docente/docentes/{{$id}}/archivos/mostrarArchivos/">Mostrar</a>
                        </li>
                      </ul>
                  </li>

	                <li>
	                    <a class="page-scroll" href="/logout">Salir </a>
	                </li>
	            </ul>
	        </div>
	    </div>
	</nav>
	@section('sidebar')
		            
    @show

		        
    <div class="container">
        @yield('content')
    </div>
    @include('layouts.footer')
	</body>
</html> 