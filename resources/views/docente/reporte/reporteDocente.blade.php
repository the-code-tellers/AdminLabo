@extends('docente.template.docenteTemplate')

@section('title','Lista Estudiantes')

@section('sidebar')
  @parent
@endsection

@section('content')

<div class="container">
	<br>
	 <button class="btn btn-info" onclick="window.print()">
	    <a  type="button" style="color:white;"> 
	    <i class="material-icons">print</i> 
	    <span>Imprimir</span>
	    </a>
	</button>
	<br>
    <br>
      <h2><b> Reporte de la Gestion: {{$gestion->numero_gestion}}-{{$gestion->anho}}</b></h2> 
      <h4><b> Materia: {{$grupomateria->materia->nombre_materia}} Grupo-{{$grupomateria->grupo->numero_grupo}} </b></h4>
      <h4><b> Docente :{{ $docente -> persona -> nombre }} {{   $docente -> persona ->apellido_paterno }} {{ $docente ->persona-> apellido_materno}}  </b></h4> 
      <h4><b> Lista de Estudiantes y sus Actividades:  </b></h4>
    <br>
	@if (count($portafolios) == 0)
            <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
              <ul> No existen estudiantes registrados en este grupo.
              </ul>
            </div>
    @else


	<table class="table table-striped table-hover">
	    <thead>
	        <tr>
	            <th>#</th>
	            <th>Apellido Paterno</th>
	            <th>Apellido Materno</th>
				<th>Nombre</th>
	            <th>Actividades Realizadas</th>
	            <th>Fecha de la sesion</th>
	            <th>Fecha y Hora de Entrega</th>
	        </tr>
	    </thead>
	    <tbody>
	        <p hidden>{{$num = 1}}</p>
		  @foreach($portafolios as $portafolio)
		    <p hidden>{{$estudiantes=  App\Estudiante::where('id', '=', $portafolio->portafolio_id  )-> get()}}  </p>
					<tr>
                     @foreach ($estudiantes as $estudiante)
                     <p hidden>{{$arregloSesionesProducto = App\SesionProducto::where('portafolio_id','=',$estudiante->id)->groupBy('sesion_id')-> get()}}</p>
                     	<td>{{$num++}}</td>
	                     <td>{{$estudiante -> persona -> apellido_paterno}}  </td>
	                     <td>{{$estudiante -> persona -> apellido_materno}} </td>
	                     <td>{{$estudiante -> persona -> nombre}} </td>
	                     <td>   
	                     @foreach ($arregloSesionesProducto as $arregloSesionProducto)
	                     	@foreach($productos as $producto)
	                     		@if( $arregloSesionProducto->id == $producto -> sesionproducto_id)
	                     		  @if($producto->referencia != NULL)
	                     			{{$producto->referencia}} 
	                     			<br>
	                     		  @endif	
	                     		@endif
	                     	@endforeach
	                     @endforeach   
	                     </td>
	                     <td>
	                     @foreach ($arregloSesionesProducto as $arregloSesionProducto)
	                     	@foreach($productos as $producto)
	                     		@if( $arregloSesionProducto->id == $producto -> sesionproducto_id)
	                     		  @if($producto->referencia != NULL)
	                     			{{$arregloSesionProducto->sesion->fecha}} 
	                     			<br>
	                     		  @endif	
	                     		@endif
	                     	@endforeach
	                     @endforeach
	                     </td>
	                     <td>   
	                     @foreach ($arregloSesionesProducto as $arregloSesionProducto)
	                     	@foreach($productos as $producto)
	                     		@if( $arregloSesionProducto->id == $producto -> sesionproducto_id)
	                     		  @if($producto->hora_publicacion != NULL)
	                     			{{$producto->hora_publicacion}} 
	                     			<br>
	                     		  @endif	
	                     		@endif
	                     	@endforeach
	                     @endforeach   
	                     </td>
                     @endforeach
                    </tr>
		  @endforeach
	    </tbody>
	</table>
	@endif

</div>



@endsection