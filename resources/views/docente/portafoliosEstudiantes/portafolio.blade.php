@extends('docente.template.docenteTemplate')

@section('title','Portafolio')

@section('sidebar')
  @parent
@endsection

@section('content')
<p hidden> 

</p>
<br>
<br>
<div class="container mt-5">
      <div class="row">
        <div class="col-12 col-md-6 offset-3">
            <div class="card">
 <h2><b>Portafolio Del Estudiante</b></h2>
  <div class="card-body">
    <label class="col-md- 6">Nombre Estudiante :  {{ $estudiante -> nombre }} 
    {{ $estudiante -> apellido_paterno }} {{ $estudiante -> apellido_materno}}
    </label>  
    <br>

        </div>
             <br> 
             <table class="table table-bordered">
              <thead>
                    <tr>
                        <th>Sesion</th>
                        <th>Fecha</th>
                    </tr>


                </thead>
              <tbody>
              
                  @foreach  ($arregloSesionesProducto as $sesionProducto)
                  <tr>
                    <td>{{$sesionProducto -> sesion ->numero_sesion}}</td>
                    <td>
                      <a href="/docente/docentes/{{$docId}}/estudiante/{{$estudId}}/portafolio/{{$materiaId}}/sesion/{{$sesionProducto -> id}}/down">Fecha:{{$sesionProducto -> sesion -> fecha}}</a>
                    </td>
                  </tr>
                   
                  @endforeach
              </tbody>
          </table>
          
        
   </div>
  

  </div>
  </div>
  </div>
  </div>
  </div>
  <script type="text/javascript">
  function actualizarCookie(nombreCookie, elementIdd, siReload ) {
    document.cookie = nombreCookie + '=' + document.getElementById(elementIdd).value;
    if(siReload) {
      location.reload();
    }
  }
</script>
@endsection
