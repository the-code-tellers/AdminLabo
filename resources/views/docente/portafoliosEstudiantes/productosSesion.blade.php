@extends('docente.template.docenteTemplate')


@section('title','Actividades del Estudiante')

@section('sidebar')
  @parent
@endsection

@section('content')
<?php
	use App\Producto;
	$directorio = storage_path('ArchivoProductos/');
	$practicaEnlace;
?>
<div class="container mt-5">
      <div class="row">
        <div class="col-12 col-md-6 offset-3">
            <div class="card">
 <h2 ><b>Actividades de la Sesión<b> </h2>
 <label><b>Nombre Estudiante :  {{ $estudiante -> nombre }} {{ $estudiante -> apellido_paterno }} {{ $estudiante -> apellido_materno}}
 <b>
 </label>   
<div class = "container">
	
	 @include('layouts.mensajesDeErrorInfo')
	
          
			@foreach($productos as $producto)
				@if (($producto -> sesionproducto_id) == $idSesionProducto)
					<div class="col-md-8" >
						<label>Hora y fecha de publicacion: {{$producto->hora_publicacion}} </label>
						<br>
						<div>
							<?php
                    $practicaEnlace = $producto->referencia;
					
               ?>
							 </div>
				    </div>				
					
				@endIf
			@endforeach
		
    <div class="col-md-8" >
		<form class="d-inline-flex" method="GET" action="docente/docentes/{{$docId}}/estudiante/{{$estudId}}/portafolio/{{$materiaId}}/sesion/" accept-charset="UTF-8" enctype="multipart/form-data">
			{{csrf_field()}}
			{{method_field('GET')}}			
			<?php
			    $coincidencias = array();
                  if($dir = opendir($directorio)){
                  while( $archivo = readdir($dir) ){
                        if( $practicaEnlace === $archivo && $archivo != '.' && $archivo != '..' ){
						   array_push($coincidencias,$archivo);						
                        }
                    } 
				if(count($coincidencias)!=0){
                      foreach($coincidencias as $coincidencia){
                         echo 
                           "<tr>
                    		    <td>
								 	<a  href='/docente/docentes/{{$docId}}/estudiante/{{$estudId}}/portafolio/{{$materiaId}}/sesion/{$idSesionProducto}/down/{$coincidencia}/'>$coincidencia</a>	
								</td>
							 </tr>";
                      }
                      
                    } else {
                      echo
                      '<div class="alert alert-info">No existe aún actividad publicada.</div>';
                    }   
				}
      ?>
		</form>
    </div>
	
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
	 function cerrarInfo(cerrarCuadroInfo){
     var elementoOpcionNoId = document.getElementById(cerrarCuadroInfo);
     elementoOpcionNoId.style.visibility = "hidden";
    }
</script>
@endsection