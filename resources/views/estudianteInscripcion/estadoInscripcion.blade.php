@extends('estudiante.template.estudianteTemplate')

@section('title','Estado Inscripcion')

@section('sidebar')
  @parent
@endsection

@section('content')
<div class="container mt-5">
	<div class="row">
        <div class="col-12 col-md-6 offset-3">
            <div class="card">
                <h2><b>Estado de la Inscripción</b> </h2>
                <br>
				 <div class="d-flex justify-content-center">
                              <button class="btn btn-success">
                                <a href="/estudiante/estudiantes/{{$idEstud}}/inscripcion/create" type="button" style="color:white;"> Inscribirse a una materia </a>
                              </button>
                               <br>
                               <br>
                 </div> 
                 @include('layouts.mensajesDeErrorInfo')
					<table class="table table-striped table-hover">
						<thead>
						<tr>
							<th>Código de la Materia</th>
							<th>Nombre de la Materia Inscrita</th>
							<th>Numero de Grupo</th>
						</tr>
						</thead>
						<tbody>
							@foreach($materiasConGrupo as $materiaConGrupo)
							<tr>
								<td>{{$materiaConGrupo->codigo_materia}} </td>
								<td>{{$materiaConGrupo->nombre_materia}}</td>
								<td>{{$materiaConGrupo->numero_grupo}}</td>
								<td>
									<form class="d-inline-flex" method="POST" action="/estudiante/estudiantes/{{$idEstud}}/inscripcion/{{$materiaConGrupo->grupomateria_id}}">
										{{csrf_field()}}
										{{method_field('DELETE')}}			
										<button type="submit" class="btn btn-default">
										<i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
										</button>
									</form>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
            		
             </div>    
        </div>
	</div>
</div>



@endsection

