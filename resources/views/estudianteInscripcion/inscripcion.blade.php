@extends('estudiante.template.estudianteTemplate')

@section('title','Incripción')

@section('sidebar')
  @parent
@endsection

@section('content')
<p hidden>
@if(empty($_COOKIE['cookieMateriaId']))
  {{setcookie('cookieMateriaId', $materias[0] -> id)}}
  {{$_COOKIE['cookieMateriaId'] = $materias[0] -> id}}
  {{$_COOKIE['cookieMateriaId'] = $materias[0] -> id}}
  {{setcookie('cookieGrupoMateriaDocHora', $grupoMaterias[0] -> id)}}
  {{$_COOKIE['cookieGrupoMateriaDocHora'] = $grupoMaterias[0] -> id}}
@endif
    {{$materiaGrupoId=$_COOKIE['cookieMateriaId']}}
    {{$variable = 0}}
</p>

<p hidden> {{
    $listaHorarios = App\GrupoMateria:: join('sesions','grupo_materias.id','=','sesions.grupoMateria_id') 
     -> join('horarios', 'sesions.horario_id','=','horarios.id') 
     -> where('materia_id', '=', $materiaGrupoId)
     -> distinct()
     -> select(['horario_id'])
     -> addSelect('dia')
     -> addSelect('grupo_id')
     -> addSelect('sesions.auxiliar_id') 
     -> addSelect('docente_id')
     -> addSelect('materia_id')
     -> addSelect('hora_entrada')
     -> addSelect('hora_salida')
     ->get() 
   }}
</p>
<!--revisar horario por grupo  -->
 <div class="container mt-5">
      <div class="row">
        <div class="col-12 col-md-6 offset-3">
            <div class="card">
                <h2><b>Inscripción a la materia y a las sesiones del grupo</b> </h2>
                 @include('layouts.mensajesDeErrorInfo')
                <div class="card-body">
                      <form action="/estudiante/estudiantes/{{$idEstud}}/inscripcion"
                          method="POST"
                          autocomplete="off">
                          {{csrf_field()}}
                        <div class="form-group">
                          <label >Materia</label>
                           <select class="form-control" id="materia" name="materia"
                            onchange="actualizarCookie('cookieMateriaId', 'materia', true)">
                               @foreach ($materias as $materia)
                                    <option value={{$materia->id}} @if($_COOKIE['cookieMateriaId']== $materia-> id)  selected="selected"@endif>
                                      {{ $materia -> nombre_materia }}
                                    </option>
                               @endforeach                       
                            </select>                             
                        </div>
                        <div class="form-group">
                            <label for="category">Grupo</label>
                            <p hidden>{{$grupoMateriaArribaId = ""}}</p>
                             <select class="form-control" id="grupo" name="grupo" onchange="seleccionarGrupoMateria()">
                                  <p hidden>{{$indice = true}}</p>
                                  @foreach ($grupoMaterias as $grupoMateria)
                                    @if($grupoMateria->materia->id == $_COOKIE['cookieMateriaId'] )
                                      @if(!($grupoMateria->sesiones->isEmpty()))
                                        <option 
                                              data-cupo= "{{$grupoMateria->sesiones->first()->cupo}}"
                                              data-dia= "{{$grupoMateria->sesiones->first()->horario->dia}}"
                                              data-horarioentrada= "{{(new \Carbon\Carbon($grupoMateria->sesiones->first()->horario->hora_entrada))->toTimeString()}}" 
                                              data-horariosalida= "{{(new \Carbon\Carbon($grupoMateria->sesiones->first()->horario->hora_salida))->toTimeString()}}"
                                              data-docente="{{$grupoMateria->docente->persona->nombre}}
                                                            {{$grupoMateria->docente->persona->apellido_paterno}}"
                                              data-gestion= "{{$grupoMateria->gestion->numero_gestion}} - {{$grupoMateria->gestion->anho}}"
                                              value= {{$grupoMateria->id}}>
                                          {{$grupoMateria->grupo->numero_grupo}}
                                        </option>                                      
                                      @endif 
                                      <p hidden>
                                      @if($indice)
                                        {{$grupoMateriaArriba = $grupoMateria}}                                    
                                        {{$indice = false}}
                                      @endif
                                      </p>
                                    @endif
                                  @endforeach
                            </select>                                                
                        </div>

                        <div>                       
                            <div class="form-group">
                              @if(!empty($grupoMateriaArriba)  && !($grupoMateriaArriba->sesiones->isEmpty()))                                 
                                <label> Gestión : </label>
                                <label >
                                  <span id="textogestion"> {{$grupoMateriaArriba->gestion->numero_gestion}} - 
                                        {{$grupoMateriaArriba->gestion->anho}} </span>
                                </label>                                             
                                <br>
                                <label> Docente : </label>
                                <label>
                                 <span id="textodocente"> {{$grupoMateriaArriba->docente->persona->nombre}} 
                                                          {{$grupoMateriaArriba->docente->persona->apellido_paterno}} </span>
                                </label>                                             
                                <br>
                                <label> Día:                                
                                  <span id="textodia"> {{$grupoMateriaArriba->sesiones->first()->horario->dia}} </span>
                                </label>
                                <br>
                                <label> Horario</label>
                                <br>
                                <label> Entrada:
                                  <span id="textoentrada"> {{(new \Carbon\Carbon($grupoMateriaArriba->sesiones->first()->horario->hora_entrada))->toTimeString()}} </span>
                                </label>
                                <br>
                                <label> Salida:
                                  <span id="textosalida"> {{(new \Carbon\Carbon($grupoMateriaArriba->sesiones->first()->horario->hora_salida))->toTimeString()}} </span>
                                </label>
                                <br>
                                <br>
                                <br>
                                
                              @endif
                              @if(!empty($grupoMateriaArriba) && !($grupoMateriaArriba->sesiones->isEmpty()))                                
                                <p hidden>{{$cupo = $grupoMateriaArriba->sesiones->first()->cupo}} </p>
                                @if($cupo > 0)                                     
                                    <span id="textocupo" class= "alert alert-success"> La materia tiene cupo para {{$cupo}} más. </span>
                                    <input id="cupo" name="cupo" type="hidden" value={{$cupo}}>
                                @else
                                    <span id="textocupo" class= "alert alert-danger" > No hay cupos disponibles para esta materia. </span>
                                    <input id="cupo" name="cupo" type="hidden" value="0">
                                @endif
                              @else
                                <span class= "alert alert-danger"> No existen sesiones en esta selección. </span>
                              @endif
                            </div>
                        </div>


                        <div class="row form-group">
                          <div class="col-xs-6 col-xs-offset-3 col1">
                            <label> Desea inscribirse a las sesiones de este grupo ?</label>
                            <div class="checkbox">
                              <input name= "checkboxInscripcion" type="checkbox" id="checkboxInscripcion" value="true" onchange="checkboxPresionado('checkboxInscripcion','labelInscripcion','contenidoOpcionNo')" checked><label id="labelInscripcion">Si</label>
                            </div>

                            <div id="contenidoOpcionNo" style="visibility:hidden; display:none">
                              <div class="form-group" >
                                <label> Elija al grupo cuyas sesiones desea inscribirse</label>
                                <label for="category">Grupo</label>
                                  <select class="form-control"  id="grupoOpcionDos" name="grupoOpcionDos" onchange="seleccionarGrupoMateriaAbajo()">
                                  
                                    <p hidden>{{$indiceA = true}}</p>
                                    @foreach ($grupoMaterias as $grupoMateria)
                                      @if($grupoMateria->materia->id == $_COOKIE['cookieMateriaId'] )
                                        @if(!($grupoMateria->sesiones->isEmpty()))
                                          <option 
                                                data-cupoA= "{{$grupoMateria->sesiones->first()->cupo}}"
                                                data-diaA= "{{$grupoMateria->sesiones->first()->horario->dia}}"
                                                data-horarioentradaA= "{{(new \Carbon\Carbon($grupoMateria->sesiones->first()->horario->hora_entrada))->toTimeString()}}" 
                                                data-horariosalidaA= "{{(new \Carbon\Carbon($grupoMateria->sesiones->first()->horario->hora_salida))->toTimeString()}}"
                                                data-docenteA= "{{$grupoMateria->docente->persona->nombre}}
                                                              {{$grupoMateria->docente->persona->apellido_paterno}}"
                                                data-gestionA= "{{$grupoMateria->gestion->numero_gestion}} - {{$grupoMateria->gestion->anho}}"
                                                value= {{$grupoMateria->id}}>
                                            {{$grupoMateria->grupo->numero_grupo}}
                                          </option>                                      
                                        @endif 
                                        <p hidden>
                                        @if($indiceA)
                                          {{$grupoMateriaAbajo = $grupoMateria}}                                    
                                          {{$indiceA = false}}
                                        @endif
                                        </p>
                                      @endif
                                    @endforeach
                                        
                                  </select>
                                <div>                      
                                  <div class="form-group">
                                    @if(!empty($grupoMateriaAbajo)  && !($grupoMateriaAbajo->sesiones->isEmpty()))                                 
                                      <label> Gestión : </label>
                                      <label >
                                        <span id="textogestionA"> {{$grupoMateriaAbajo->gestion->numero_gestion}} - 
                                              {{$grupoMateriaAbajo->gestion->anho}} </span>
                                      </label>                                             
                                      <br>
                                      <label> Docente : 
                                       <span id="textodocenteA"> {{$grupoMateriaAbajo->docente->persona->nombre}} 
                                                                {{$grupoMateriaAbajo->docente->persona->apellido_paterno}} </span>
                                      </label>                                             
                                      <br>
                                      <label> Día:                                
                                        <span id="textodiaA"> {{$grupoMateriaAbajo->sesiones->first()->horario->dia}} </span>
                                      </label>
                                      <br>
                                      <label> Horario</label>
                                      <br>
                                      <label> Entrada:
                                        <span id="textoentradaA"> {{(new \Carbon\Carbon($grupoMateriaAbajo->sesiones->first()->horario->hora_entrada))->toTimeString()}} </span>
                                      </label>
                                      <br>
                                      <label> Salida:
                                        <span id="textosalidaA"> {{(new \Carbon\Carbon($grupoMateriaAbajo->sesiones->first()->horario->hora_salida))->toTimeString()}} </span>
                                      </label>
                                      <br>
                                      <br>                                      
                                      <br>
                                    @endif
                                    @if(!empty($grupoMateriaAbajo) && !($grupoMateriaAbajo->sesiones->isEmpty()))                                
                                      <p hidden>{{$cupoA = $grupoMateriaAbajo->sesiones->first()->cupo}} </p>
                                      @if($cupo > 0)                                     
                                          <span id="textocupoA" class= "alert alert-success"> La materia tiene cupo para {{$cupoA}} más. </span>
                                          <input id="cupoA" name="cupoA" type="hidden" value={{$cupoA}}>
                                      @else
                                          <span id="textocupoA" class= "alert alert-danger" > No hay cupos disponibles. </span>
                                          <input id="cupoA" name="cupoA" type="hidden" value="0">
                                      @endif
                                    @else
                                      <span class= "alert alert-danger"> No existen sesiones. </span>
                                    @endif
                                  </div>
                                </div>

                              </div>
                            </div>
                              <div class="d-flex justify-content-center">
                                <button type="submit" id= "BotonInscribirse"class="btn btn-success" >
                                    <i class="fas fa-save"></i> Incribirse
                                </button>
                                <button type="button" class="btn btn-danger">
                                   <a href= "/estudiante/estudiantes/{{$idEstud}}/inscripcion/" style="color:white;"  > Cancelar </a> 
                                </button>
                              </div>
                          </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
   console.log('abc');
  function actualizarCookie(nombreCookie, elementIdd, siReload ) {
    console.log(document.getElementById(elementIdd).value);
    document.cookie = nombreCookie + '=' + document.getElementById(elementIdd).value;
    if(siReload) {
      location.reload();
    }
  }

  function actualizarCookie2(nombreCookie, elementIdd, siReload ) {
    var cod = document.getElementById("grupoOpcionDos").value;
   
  }

  function checkboxPresionado(elementIdd, labelId,contenidoOpcionNoId){
     var elementoCheckBox = document.getElementById(elementIdd);
     var elementoLabelCheckBox = document.getElementById(labelId);
     var elementoOpcionNoId = document.getElementById(contenidoOpcionNoId);
     if (elementoCheckBox.checked) {
      elementoLabelCheckBox.innerHTML = "Si";
      elementoCheckBox.value="true";
      elementoOpcionNoId.style.visibility = "hidden";
      elementoOpcionNoId.style.display= "none";

     } else {
      elementoLabelCheckBox.innerHTML = "No";
      elementoCheckBox.value="false";
      elementoOpcionNoId.style.visibility = "visible";
      elementoOpcionNoId.style.display = "block";
     }
  }
  
  function seleccionarGrupoMateria() {
    var elementoTextoCupo = document.getElementById('textocupo');
    var elementoCupo = document.getElementById('cupo');
    var elementoSeleccion = document.getElementById('grupo');
    var elementoOpcion = elementoSeleccion.options[elementoSeleccion.selectedIndex];
    var cupo = elementoOpcion.getAttribute('data-cupo');
    console.log(cupo);
    if(cupo > 0) {
      elementoTextoCupo.innerHTML = 'La materia tiene cupo para '+ cupo +' más.';
      elementoTextoCupo.className = "alert alert-success";
    } else {
      elementoTextoCupo.innerHTML = 'No quedan cupos disponibles para la selección.';
      elementoTextoCupo.className = "alert alert-danger";
    }
    elementoCupo.value= cupo;
    actualizarTextoDeGrupoMateria(elementoOpcion);
    
  }
  
  function actualizarTextoDeGrupoMateria(elementoOpcion) {
    var gestion = elementoOpcion.getAttribute('data-gestion');
    var docente = elementoOpcion.getAttribute('data-docente');
    var horaEntrada = elementoOpcion.getAttribute('data-horarioentrada');
    var horaSalida = elementoOpcion.getAttribute('data-horariosalida');
    var dia = elementoOpcion.getAttribute('data-dia');
    
    var elementoTextoGestion = document.getElementById('textogestion');
    var elementoTextoDocente = document.getElementById('textodocente');
    var elementoTextoEntrada = document.getElementById('textoentrada');
    var elementoTextoSalida = document.getElementById('textosalida');
    var elementoTextoDia = document.getElementById('textodia');
    
    elementoTextoGestion.innerHTML = gestion;
    elementoTextoDocente.innerHTML = docente;
    elementoTextoEntrada.innerHTML = horaEntrada;
    elementoTextoSalida.innerHTML = horaSalida;
    elementoTextoDia.innerHTML = dia;
  }
  
  function seleccionarGrupoMateriaAbajo() {
    var elementoTextoCupo = document.getElementById('textocupoA');
    var elementoCupo = document.getElementById('cupoA');
    var elementoSeleccion = document.getElementById('grupoOpcionDos');
    var elementoOpcion = elementoSeleccion.options[elementoSeleccion.selectedIndex];
    var cupo = elementoOpcion.getAttribute('data-cupoA');
    console.log(cupo);
    if(cupo > 0) {
      elementoTextoCupo.innerHTML = 'El grupo tiene cupo para '+ cupo +' más.';
      elementoTextoCupo.className = "alert alert-success";
    } else {
      elementoTextoCupo.innerHTML = 'No hay cupos disponibles.';
      elementoTextoCupo.className = "alert alert-danger";
    }
    elementoCupo.value= cupo;
    actualizarTextoDeGrupoMateriaAbajo(elementoOpcion);
    
  }
  
  function actualizarTextoDeGrupoMateriaAbajo(elementoOpcion) {
    var gestion = elementoOpcion.getAttribute('data-gestionA');
    var docente = elementoOpcion.getAttribute('data-docenteA');
    var horaEntrada = elementoOpcion.getAttribute('data-horarioentradaA');
    var horaSalida = elementoOpcion.getAttribute('data-horariosalidaA');
    var dia = elementoOpcion.getAttribute('data-diaA');
    
    var elementoTextoGestion = document.getElementById('textogestionA');
    var elementoTextoDocente = document.getElementById('textodocenteA');
    var elementoTextoEntrada = document.getElementById('textoentradaA');
    var elementoTextoSalida = document.getElementById('textosalidaA');
    var elementoTextoDia = document.getElementById('textodiaA');
    
    elementoTextoGestion.innerHTML = gestion;
    elementoTextoDocente.innerHTML = docente;
    elementoTextoEntrada.innerHTML = horaEntrada;
    elementoTextoSalida.innerHTML = horaSalida;
    elementoTextoDia.innerHTML = dia;
  }
</script>
@endsection