@extends('estudiante.template.estudianteTemplate')

@section('title','Inicio Estudiante')


@section('sidebar')
  @parent
@endsection

@section('content')
<style>
	div.background {
    background: url(/plugins/bootstrap/images/logo_opaco.png);
    background-repeat: none;
    background-attachment: scroll;
    background-position: center;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    -o-background-size: cover;
    text-align: center;
    color: #fff;
    width: 450px;
    height: 450px;
    margin-left: auto;
    margin-right: auto;
    }

	div.welcome-letter{
    font-family: "Playfair Display",sans-serif;
    color: black;
    font-style: bold;
    font-size: 22px;
    line-height: 22px;
    margin-bottom: 25px;
	}

	div.intro-heading {
    color: black;
    text-transform: uppercase;
    font-weight: 700;
    font-size: 50px;
    line-height: 50px;
    margin-bottom: 25px;
    opacity: 100;
	}

</style>
@include('layouts.mensajesDeErrorInfo')
    <div class="background">
        <div class="welcome-letter">
            <br><br><br><br>
            <div class="intro-lead-in">Bienvenido a </div>
            <div class="intro-heading">Sis-Lab</div>
            <div class="intro-heading">{{App\Estudiante::find($id)->persona->nombre}}</div>
        </div>
    </div >

@endsection