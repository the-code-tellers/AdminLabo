@extends('estudiante.template.estudianteTemplate')

@section('title','Editar Información')
@section('sidebar')
  @parent
@endsection

@section('content')

<div class="container">
<br>
	<h2><b> Mi información </b></h2>
</div>
@include('layouts.mensajesDeErrorInfo')
<div style=" border: 1px solid #ccc !important; border-radius: 16px; padding: 16px">
<form method="POST" action="/estudiante/estudiantes/{{ $estudianteId }}">
				{{method_field('PUT')}}
        {{ csrf_field() }}
				<div class="modal-body">			

						<div class="form-group">
							<label>Nombre</label>
							<input type="text" class="form-control" name="nombre" value="{{ $estudiante->persona->nombre}}">
						</div>
						<div class="form-group">
							<label>Apellido Paterno</label>
							<input type="text" class="form-control" name="apellido_paterno" value="{{ $estudiante->persona->apellido_paterno}}">
						</div>
						<div class="form-group">
							<label>Apellido Materno</label>
							<input type="text" class="form-control" name="apellido_materno" value="{{ $estudiante->persona->apellido_materno}}">
						</div>
						<div class="form-group">
							<label>Email</label>
							<input type="email" class="form-control" name="correo" value="{{ $estudiante->persona->correo}}">
						</div>
							</div>
        <button type="submit" class="btn btn-primary">Actualizar Información</button>
</form>
</div>
				<br>
@endsection