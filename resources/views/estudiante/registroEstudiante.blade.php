@extends('layouts.master')

@section('title','Registro estudiante')

@section('sidebar')
  @parent
@endsection

@section('content')
<br>
<br>
<br>
<br>
<br>
<br>
<form method= 'POST'  action="/estudiante/registrado" style="max-width:500px;margin:auto">
	{{ csrf_field() }} 
  <h2><b>Registrar Estudiante</b></h2>
   @include('layouts.mensajesDeErrorInfo')
  <div >
    <h4><span class="label label-default">Alias</span></h4>
    <input class="form-control" type="text"  id="alias" name="alias" value = "{{ old('alias','')}}" placeholder="Alias">
  </div>
  
  <div class="input-container">
    <h4><span class="label label-default">Nombre</span></h4>
    <input class="form-control" type="text" id="nombre" name="nombre" value = "{{ old('nombre','')}}" placeholder="Nombre" >
  </div>
  
  <div class="input-container">
    <h4><span class="label label-default">Apellido Paterno</span></h4>
    <input class="form-control" type="text" placeholder="Apellido Paterno" name="apellido_paterno"
    value = "{{ old('apellido_paterno','')}}">
  </div>
  
  <div class="input-container">
    <h4><span class="label label-default">Apellido Materno</span></h4>
    <input class="form-control" type="text" placeholder="Apellido Materno" name="apellido_materno"
    value = "{{ old('apellido_materno','')}}">
  </div>

  <div class="input-container">
    <h4><span class="label label-default">Correo</span></h4>
    <input class="form-control" type="text" placeholder="Correo" name="correo" value = "{{ old('correo','')}}">
  </div>
  
  <div class="input-container">
    <h4><span class="label label-default">Código SIS</span></h4>
    <input class="form-control" type="text" placeholder="Codigo SIS" name="codigo_sis" value = "{{ old('codigo_sis','')}}">
  </div>

  
  <div class="input-container">
    <h4><span class="label label-default">Contraseña</span></h4>
    <input class="form-control" type="password" placeholder="Password" name="contrasenha">
  </div>
<br>
  <button type="submit" class="btn btn-success">Registrar</button>
</form>
<br>
@endsection

