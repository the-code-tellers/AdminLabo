<!DOCTYPE html>
<html lang = "en">
@include('layouts.head')
<body>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
<p hidden>	{{$id = App\Persona::find(\Auth::user()->id)->estudiante->id}} </p>
	<nav class="navbar navbar-default navbar-fixed-top">
	    <div class="container">
	        <div class="navbar-header page-scroll">
	            <a class="navbar-brand page-scroll" href="#myPage"><img src="/plugins/bootstrap/images/lab_logo.png" alt="umss logo"></a>
	        </div>
	            <ul class="nav navbar-nav navbar-right">
	                <li class="hidden">
	                    <a href="#page-top"></a>
	                </li>
	                <li>
	                    <a class="page-scroll" href="/estudiante/estudiantes/{{$id}}">Inicio</a>
	                </li>
	                <li>
	                    <a class="page-scroll" href="/estudiante/estudiantes/{{$id}}/horario">Horarios </a>
	                </li>
	                <li>
	                    <a class="page-scroll" href="/estudiante/estudiantes/{{$id}}/edit">Inf. Personal</a>
	                </li>
	                <li class="dropdown">
			          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Actividades
			          <span class="caret"></span></a>
			          <ul class="dropdown-menu">
			            <li><a href="/estudiante/estudiantes/{{$id}}/portafolio">Mi Portafolio</a></li>
			            <li><a href="/estudiante/estudiantes/{{$id}}/archivos/mostrarArchivos/">Prácticas a realizar</a></li>
			          </ul>
			        </li>
	                <li>
	                    <a class="page-scroll" href="/estudiante/estudiantes/{{$id}}/inscripcion/">Inscripción</a>
	                </li>
	                <li>
	                    <a class="page-scroll" href="/logout">Salir</a>
	                </li>
	            </ul>
	    </div>
	</nav>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	@section('sidebar')
		            
    @show
		        
    <div class="container">
        @yield('content')
    </div>

	@yield('scripts')
	@include('layouts.footer')
	</body>
</html> 