@extends('estudiante.template.estudianteTemplate')

@section('title','Practicas Subidas')

@section('sidebar')
  @parent
@endsection

@section('content')
<p hidden> 
@if(empty($_COOKIE['cookieGrupoMateriaPracticaMostrar2']) || empty ($_COOKIE['cookieGestionMostrar2']))
  {{setcookie('cookieGestionMostrar2', $gestiones2[0] -> id)}}
  {{setcookie('cookieGrupoMateriaPracticaMostrar2', $grupoMaterias2[0] -> id)}}
  {{setcookie('cookieSesionMostrar2', $sesions2[0] -> id)}}
  {{$_COOKIE['cookieGestionMostrar2'] = $gestiones2[0] -> id}}
  {{$_COOKIE['cookieGrupoMateriaPracticaMostrar2'] = $grupoMaterias2[0] -> id}}
  {{$_COOKIE['cookieSesionMostrar2'] = $sesions2[0] -> id}}
@endif
</p>
<?php
use App\PracticaLaboratorio;
$directorio = storage_path('guiaPractica');
$archivoNombre;
?>

<div class="container">
  <div class="panel-heading"></div>
        <div class="panel-body">
          <form method= "GET"  action="/estudiante/estudiantes/{{$estudianteId}}/archivos/mostrarArchivos/" accept-charset="UTF-8" enctype="multipart/form-data">

               <h2><b>Guias Prácticas Existentes</b></h2>
                 
                  <h4><span class="label label-default">Gestión</span></h4>
              <select name="gestion" id="gestion" class="form-control" onchange="actualizarCookie('cookieGestionMostrar2','gestion',true);">
                <optgroup label="Gestiones por año">  
                  @if (!(empty($gestiones2)))
        
                  @foreach ($gestiones2 as $gestion)
                   <option value="{{$gestion-> id}}"
                      @if($_COOKIE['cookieGestionMostrar2'] == $gestion->id)
                        selected="selected"
                      @endif>
                      {{ $gestion -> numero_gestion }} {{ ' - '.$gestion -> anho }} 
                   </option>
                  @endforeach
                  @endif
                </optgroup>
            </select>  


                 <h4><span class="label label-default">Grupo - Materia</span></h4>
                <select name="materia" id="materia" class="form-control" onchange="actualizarCookie('cookieGrupoMateriaPracticaMostrar2','materia',true);">
                <optgroup label="Materias por nombre">  
                  @if (!(empty($grupoMaterias2)))
                   <option disabled selected value>Grupo-Materias Disponibles</option>
                  @foreach ($grupoMaterias2 as $grupoMateria)
                  {{$gestion = $grupoMateria->gestion_id}}
                   @if($_COOKIE['cookieGestionMostrar2'] == $gestion)
                   <option value="{{$grupoMateria-> id}}"
                      @if($_COOKIE['cookieGrupoMateriaPracticaMostrar2']== $grupoMateria->id)
                        selected="selected"
                      @endif>
                      {{ $grupoMateria -> materia -> nombre_materia }} 
                        Grupo: {{ $grupoMateria -> grupo -> numero_grupo }}
                               {{ '--- '.$grupoMateria -> docente -> persona -> nombre}}
                               {{ $grupoMateria -> docente -> persona -> apellido_paterno}}
                               {{ $grupoMateria -> docente -> persona -> apellido_materno}}
                   </option>
                  @endif 
                  @endforeach
                  @endif
                </optgroup>
            </select>   
     <h4><span class="label label-default">Sesi&oacuten - Fecha</span></h4>
    <select name="sesion" id="sesion" class="form-control" onchange="actualizarCookie('cookieSesionMostrar2','sesion',true);">
      <optgroup label="Sesiones disponibles">  
        @if (!(empty($sesions2)))    
          @foreach ($sesions2 as $sesion)
          @if($_COOKIE['cookieGrupoMateriaPracticaMostrar2'] == $sesion->grupomateria_id)
            <option value="{{$sesion-> id}}"
             @if($_COOKIE['cookieSesionMostrar2']== $sesion->id)
                    selected="selected"
              @endif>
              Sesion: {{ $sesion -> numero_sesion }} {{'--- '.$sesion -> fecha}}
            </option>
            @endif
          @endforeach
        @endif
      </optgroup>
    </select>  
            <?php
                  $coincidencias = array();
                  if($dir = opendir($directorio)){
                  $practicasLaboratorios = PracticaLaboratorio:: all();
                  while( $archivo = readdir($dir) ){
                   foreach($practicasLaboratorios as $practicaLaboratorio){
                    $sesionIdPractica = $practicaLaboratorio->sesion_id ;
                    $practicaEnlace = $practicaLaboratorio->enlace;
                        if( $sesionIdPractica == $_COOKIE['cookieSesionMostrar2'] && $practicaEnlace == $archivo && $archivo != '.' && $archivo != '..' ){
                          $archivoNombre = $archivo;
                          array_push($coincidencias,$archivo);
                        }
                      }
                    }  
                     if(count($coincidencias)!=0){
                      foreach($coincidencias as $coincidencia){
                         echo 
                          " <tr>
                          <td align=center width=100 height=150> <a href= '/estudiante/estudiantes/{{$estudianteId}}/archivos/mostrarArchivos/{$coincidencia}' onclick='return archivoNombre()'>
                           <img src='/iconoArchivo/icono1.png' title='$coincidencia' width=170 height=210 >
                            </a>
                          </td>
                          </tr> \n";
                      }
                      
                    } else {
                      echo
                      '<div class="alert alert-info">No se han subido archivos en el grupo y sesión correspondientes</div>';  
                    }     
                  }
            ?>
          </form>
          <p id="archivoDescargar"></p>
        </div>
      </div>
</div>
 <script type="text/javascript">
  function actualizarCookie(nombreCookie, elementIdd, siReload ) {
    console.log("Ejecuta");
    document.cookie = nombreCookie + '=' + document.getElementById(elementIdd).value;
    if(siReload) {
      location.reload();
    }
  }
</script>

<script>
function archivoNombre() {
  document.getElementById("archivoDescargar").innerHTML = $archivoNombre;
}
</script>

@endsection