@extends('estudiante.template.estudianteTemplate')

@section('title','Mostrar Practicas')

@section('sidebar')
  @parent
@endsection

@section('content')
<div class="row">
  <div class="col-md-10 col-md-offset-1">
    <div style=" border: 1px solid #ccc !important; border-radius: 16px; padding: 16px">
      <h2><b>Guias Prácticas Existentes</b></h2>
        <div class="panel-body" >
          @include('layouts.mensajesDeErrorInfo')
        </div>
      </div>
    </div>
</div>
<br>
<br>
<br>
<br><br>
@endsection