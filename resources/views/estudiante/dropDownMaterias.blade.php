@extends('estudiante.template.estudianteTemplate')

@section('title','Tabla Horarios')

@section('sidebar')
  @parent
@endsection

@section('content')
<br>
<h2><b>Horarios de las Sesiones</b></h2>
@include('tablaHorarios.materiaDropdown')
@include ('tablaHorarios.tablaHorarios')
@endsection