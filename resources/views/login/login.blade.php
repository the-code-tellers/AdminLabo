<?php
session_start();
?>
@extends('layouts.master')

@section('title','Login')

@section('sidebar')
  @parent
@endsection

@section('content')  
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="container" > <!--container -->
		<div class="d-flex justify-content-center h-95 w-100">
			<div class="card">
				<div class="card-header">
					 <h2><b>Ingresar</b></h2>
					  @include('layouts.mensajesDeErrorInfo')
				</div>
				<div class="card-body">

                <?php if (isset($_SESSION['email'])) { ?>
                      Ya estas dentro del sistema
                    <?php } else { ?>

					<form  method="POST" action="/login" >
				      	{{ csrf_field() }} 
						<div class="input-group form-group">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="fas fa-user"></i></span>
							</div>
			
							 <input type="email" class="form-control" placeholder="correo electronico" name= "email" id="email">
						</div>
						<div class="input-group form-group">
						<div class="input-group-prepend">
								<span class="input-group-text"><i class="fas fa-key"></i></span>
							</div>
							<input type="password" class="form-control" placeholder="password" name="password" id="password">
						</div>
						
					<!--	<div class="row align-items-center remember">
							<input type="checkbox" name= "remember">Recuérdame
						</div> -->

						<div class="form-group">
							<input type="submit" value="Login" class="btn btn-success float-right login_btn">
						</div>
					</form>
					 <?php } ?>
				</div>
			<!--	<div class="card-footer">
			
					<div class="d-flex justify-content-center">
						<a href="#">Olvidó su contraseña?</a>
					</div>
				</div> -->
			</div> 
		</div>
	</div>
	<br>
@endsection