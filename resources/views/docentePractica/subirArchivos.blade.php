@extends('docente.template.docenteTemplate')

@section('title','Subir Practicas')

@section('sidebar')
  @parent
@endsection

@section('content')
<p hidden> 
@if(empty($_COOKIE['cookieGrupoMateriaPractica'])|| empty($_COOKIE['cookieSesion']) || empty ($_COOKIE['cookieGestionSubir']) )
  {{setcookie('cookieGrupoMateriaPractica', $grupoMaterias[0] -> id)}}
  {{setcookie('cookieSesion', $sesions[0] -> id)}}
  {{setcookie('cookieGestionSubir', $gestions[0] -> id)}}
  {{$_COOKIE['cookieGrupoMateriaPractica'] = $grupoMaterias[0] -> id}}
  {{$_COOKIE['cookieSesion'] = $sesions[0] -> id}}
  {{$_COOKIE['cookieGestionSubir'] = $gestions[0] -> id}}

@endif
</p>
<div class="row">
  <div class="col-md-10 col-md-offset-1">
    <div class="panel panel-default">
      <h2><b>Publicar Archivo de Guía Práctica</b></h2>
      @include('layouts.mensajesDeErrorInfo')
        <div class="panel-body">

          <form method="POST"  action="/docente/docentes/{{$docenteId}}/archivos/archivoSubido" accept-charset="UTF-8" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
             
              <h4><span class="label label-default">Gestión</span></h4>
              <select name="gestion" id="gestion" class="form-control" onchange="actualizarCookie('cookieGestionSubir','gestion',true);">
                <optgroup label="Gestiones por año">  
                  @if (!(empty($gestions)))
        
                  @foreach ($gestions as $gestion)
                   <option value="{{$gestion-> id}}"
                      @if($_COOKIE['cookieGestionSubir']== $gestion->id)
                        selected="selected"
                      @endif>
                      {{ $gestion -> numero_gestion }} {{ ' - '.$gestion -> anho }} 
                   </option>
                  @endforeach
                  @endif
                </optgroup>
            </select>   

            

             <h4><span class="label label-default">Grupo-Materia</span></h4>
              <select name="materia" id="materia" class="form-control" onchange="actualizarCookie('cookieGrupoMateriaPractica','materia',true);">
                <optgroup label="Materias por nombre">  
                  @if (!(empty($grupoMaterias)))
                  <option disabled selected value>Grupo-Materias Disponibles</option>
                  @foreach ($grupoMaterias as $grupoMateria)
                  {{$gestion = $grupoMateria->gestion_id}}
                  @if($_COOKIE['cookieGestionSubir'] == $gestion)
                   <option value="{{$grupoMateria-> id}}"
                      @if($_COOKIE['cookieGrupoMateriaPractica']== $grupoMateria->id)
                        selected="selected"
                      @endif>
                      {{ $grupoMateria -> materia -> nombre_materia }} 
                        Grupo: {{ $grupoMateria -> grupo -> numero_grupo }}
                   </option>
                   @endif
                  @endforeach
                  @endif
                </optgroup>
            </select>   
     <h4><span class="label label-default">Sesi&oacuten - Fecha</span></h4>
    <select name="sesion" id="sesion" class="form-control" onchange="actualizarCookie('cookieSesion','sesion',false);">
      <optgroup label="Sesiones disponibles">  
        @if (!(empty($sesions)))    
          @foreach ($sesions as $sesion)
          @if($_COOKIE['cookieGrupoMateriaPractica'] == $sesion-> grupomateria_id)
            <option value="{{$sesion-> id}}"
             @if($_COOKIE['cookieSesion']== $sesion->id)
                    selected="selected"
              @endif>
              Sesion: {{ $sesion -> numero_sesion }} {{' --- '.$sesion -> fecha }}
            </option>
            @endif
          @endforeach
         @else
            <div class="alert alert-info" role="alert" id="alert" id="cerrarCuadroInfo" >
              <label>No existe una sesión para la grupo materia seleccionada</label>
            </div>
        @endif

      </optgroup>
    </select>   
    
            <div class="form-group-control">
              <label class="col-md-4 control-label">Nuevo Archivo</label>
              <div class="col-md-6">
                <input type="file" class="form-control" id="file" name="file" >
              </div>
            </div>
 
            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">Enviar</button>
              </div>
            </div>
          </form>




        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
  function actualizarCookie(nombreCookie, elementIdd, siReload ) {
    console.log("Ejecuta");
    document.cookie = nombreCookie + '=' + document.getElementById(elementIdd).value;
    if(siReload) {
      location.reload();
    }
  }
</script>

@endsection