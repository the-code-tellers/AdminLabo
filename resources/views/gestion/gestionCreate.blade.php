@extends('admin.template.adminTemplate')

@section('title','Registrar gestión')

@section('sidebar')
  @parent
@endsection

@section('content')
<br>
<br>
<div class="container mt-5">
    <div class="row">
        <div class="col-12 col-md-6 offset-3">
            <div class="card">
                <h2> <b>
                    Crear Gestión 
                  </b>
                </h2>
                 @include('layouts.mensajesDeErrorInfo')
                <div class="card-body">
                    <form action="/admin/administradors/{{$idAdmin}}/gestiones"
                          method="POST"
                          autocomplete="off">
                          {{csrf_field()}}
                        <div class="form-group">
                            <h4><span class="label label-default" for="name">Fecha Inicio</span></h4>
							               <input type="date"  date-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" maxlength="100"  name= "fecha_inicio"required >

                        </div>
                        <div class="form-group">
                            <h4><span class="label label-default"  for="category">Fecha Fin</span></h4>
                            <input type="date"  date-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy"maxlength="100" name= "fecha_fin" required/>

                        </div>
                        <div class="form-group">
                          <h4><span class="label label-default" for="numero_gestion">Número de Gestión</span></h4>
                            <input type="number"
                                   class="form-control"
                                   name="numero_gestion" id="" placeholder="Numero de Gestion" 
                                   min= "1"
                                   max= "4" required>
                        </div>
                        <div class="d-flex justify-content-center">
                          <button type="submit" class="btn btn-success">
                                <i class="fas fa-save"></i> Crear
                            </button>
                        
                        <button type="button" class="btn btn-danger">
                                   <a href="/admin/administradors/{{$idAdmin}}/gestiones" style="color:white;"  > Cancelar </a> 
                         </button>
                       </div>
                    </form>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection