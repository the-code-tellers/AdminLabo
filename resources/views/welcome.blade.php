@extends('layouts.master')

@section('content')
    <header>
        <div>
            <div class="intro-text">
                <div class="intro-lead-in">Bienvenido a </div>
                <div class="intro-heading">Sis-Lab</div>
                <a href="/masinfo" class="page-scroll btn btn-xl">Más info</a>
            </div>
        </div>
    </header> 
@endsection