@extends('estudiante.template.estudianteTemplate')

@section('title','Portafolio')

@section('sidebar')
  @parent
@endsection

@section('content')
<div class="container mt-5">
      <div class="row">
        <div class="col-12 col-md-6 offset-3">
            <div class="card">
             <h2><b>Portafolio Del Estudiante</b></h2>
              <div class="card-body">
                <label class="col-md- 6">Nombre Estudiante :  {{ $estudiante -> nombre }} {{ $estudiante -> apellido_paterno }} {{ $estudiante -> apellido_materno}}
                </label>

                  <div class="container">
                      @include('layouts.mensajesDeErrorInfo')  
                  </div>
                <br>

              </div>
            </div>
        </div>
      </div>
</div>
<br>
<br>
<br>
<br>
@endsection
