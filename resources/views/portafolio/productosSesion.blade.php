@extends('estudiante.template.estudianteTemplate')


@section('title','Actividades del Estudiante')

@section('sidebar')
  @parent
@endsection

@section('content')
<?php
	use App\Producto;
	$directorio = storage_path('ArchivoProductos/');
	$practicaEnlace;
?>
<div class="container mt-5">
      <div class="row">
        <div class="col-12 col-md-6 offset-3">
            <div class="card">
  <h2 ><b>Actividad de la clase de fecha<b> </h2>
  <h2 ><b>{{$sesionProductoFecha}}<b> </h2>
 <label><b>Nombre Estudiante :  {{ $estudiante -> nombre }} {{ $estudiante -> apellido_paterno }} {{ $estudiante -> apellido_materno}}
 <b>
 </label>   
<div class = "container">
	
	 @include('layouts.mensajesDeErrorInfo')
	
          
			@foreach($productos as $producto)
				@if (($producto -> sesionproducto_id) == $idSesionProducto)
					<div class="col-md-8" >
						<label>Hora y fecha de publicacion de la actividad/tarea presentada: {{$producto->hora_publicacion}} </label>
						<br>
						<div>
							<?php
                    $practicaEnlace = $producto->referencia;
					
               ?>
							 </div>
				    </div>				
					
				@endIf
			@endforeach
	
		
    <div class="col-md-8" >
		<form class="d-inline-flex" method="GET" action="/estudiante/estudiantes/{{$estudId}}/portafolio/{{$idSesionProducto}}/producto/{{$idSesionProducto}}/" accept-charset="UTF-8" enctype="multipart/form-data">
			{{csrf_field()}}
			{{method_field('GET')}}			
			<?php
			    $coincidencias = array();
                  if($dir = opendir($directorio)){
                  while( $archivo = readdir($dir) ){
                        if( $practicaEnlace === $archivo && $archivo != '.' && $archivo != '..' ){
						   array_push($coincidencias,$archivo);						
                        }
                    } 
				if(count($coincidencias)!=0){
                      foreach($coincidencias as $coincidencia){
                         echo 
                          "<tr>
                    		    <td>
								 	<a  href='/estudiante/estudiantes/{$estudId}/portafolio/{$idSesionProducto}/producto/{$coincidencia}/'>$coincidencia</a>	
								</td>
							 </tr>";
                      }
                      
                    } else {
                      echo
                      '<div class="alert alert-info">No existe aún actividad publicada, si usted tiene más de una actividad a presentar se recomienda comprimirla en un archivo .zip .</div>';
                    }   
				}
      ?>
		</form>
    </div>


    <form method="POST"  action="/estudiante/estudiantes/{{$estudId}}/portafolio/{{$idSesionProducto}}/producto" 
	accept-charset="UTF-8" enctype="multipart/form-data">
	   {{ csrf_field() }}
            <div class="form-group">
            	<div class="col-md-8">
                <label>Nuevo Archivo</label>
                  <input type="file" class="form-control" id="file" name="file" >
                </div>
            </div> 
			  
            <div class="form-group">
                <div class="col-sm-6">
                <button type="submit" class="btn btn-success">
                     <i class="fas fa-save"></i> Subir Tarea
                </button>
            	</div>
            </div> 
			 
    </form> 
	
    <div class="col-sm-6" >
		<form class="d-inline-flex" method="POST" action="/estudiante/estudiantes/{{$estudId}}/portafolio/{{$idSesionProducto}}/producto/{{$idSesionProducto}}">
			{{csrf_field()}}
			{{method_field('DELETE')}}			
			<button type="submit" class="btn btn-danger">
				<i class="fas fa-trash-alt"></i> Eliminar
			</button>
			
		</form>
	</div> 

</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
	 function cerrarInfo(cerrarCuadroInfo){
     var elementoOpcionNoId = document.getElementById(cerrarCuadroInfo);
     elementoOpcionNoId.style.visibility = "hidden";
    }
</script>
@endsection