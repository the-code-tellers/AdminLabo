@extends('estudiante.template.estudianteTemplate')

@section('title','Portafolio')

@section('sidebar')
  @parent
@endsection

@section('content')
<p hidden> 
@if(empty($_COOKIE['cookieMateriaPortafolio']))
  {{setcookie('cookieMateriaPortafolio',$arregloSesionesProducto [0] -> id)}}
  {{$_COOKIE['cookieMateriaPortafolio'] = $arregloSesionesProducto [0] -> id}}

@endif
</p>
<div class="container mt-5">
      <div class="row">
        <div class="col-12 col-md-6 offset-3">
            <div class="card">
 <h2><b>Portafolio Del Estudiante</b></h2>
  <div class="card-body">
    <label class="col-md- 6">Nombre Estudiante :  {{ $estudiante -> nombre }} {{ $estudiante -> apellido_paterno }} {{ $estudiante -> apellido_materno}}
    </label>  
    <br>

        <div class="form-group">
          @if ($materiasConGrupo->isEmpty())
                   <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    <ul> No se inscribio a ninguna materia aun.
                    </ul>
                  </div>
          @endif

          <label>Seleccione una materia:</label>
              <select class="form-control" id="materia" name="materia" 
              onchange="actualizarCookie('cookieMateriaPortafolio', 'materia', true)">
              <optgroup label="materia">
                <option></option>
                 @foreach ($materiasConGrupo as $materiaConGrupo)
                    <option value={{$materiaConGrupo-> grupomateria_id}}
                     @if($_COOKIE['cookieMateriaPortafolio'] == $materiaConGrupo->grupomateria_id)  selected="selected" @endif> 

                    {{$materiaConGrupo->nombre_materia }}
                    Grupo: {{$materiaConGrupo->numero_grupo}}  
                  </option>
                  @endforeach
              </optgroup>
                                  
             </select>
        
        

            <label> Materia :  </label>
            <label>
                @foreach ($materiasConGrupo  as $materiaConGrupo)
                    @if($materiaConGrupo->grupomateria_id == $_COOKIE['cookieMateriaPortafolio'] )
                        {{$materiaConGrupo->nombre_materia}}
                        Grupo: {{$materiaConGrupo->numero_grupo}} 
                    @endif
                @endforeach
            </label>                                             
             <br> 
             <table class="table table-bordered">
              <thead>
                    <tr>
                        <th>Sesion</th>
                        <th>Dia de la clase en laboratorio</th>
                    </tr>
                </thead>
              <tbody>
                  @foreach  ($arregloSesionesProducto as $sesionProducto)
                   @if( ($sesionProducto -> sesion -> grupomateria_id) == $_COOKIE['cookieMateriaPortafolio'] )
                  <tr>
                    <td>{{$sesionProducto -> sesion ->numero_sesion}}</td>
                    <td>
                      <a href="/estudiante/estudiantes/{{$estudId}}/portafolio/{{$sesionProducto -> id}}/sesionproducto">Fecha:{{$sesionProducto -> sesion -> fecha}}</a>
                    </td>
                  </tr>

                  @endif
                  @endforeach
              </tbody>
          </table>
          
        
   </div>
  

  </div>
  </div>
  </div>
  </div>
  </div>
  <script type="text/javascript">
  function actualizarCookie(nombreCookie, elementIdd, siReload ) {
    document.cookie = nombreCookie + '=' + document.getElementById(elementIdd).value;
    if(siReload) {
      location.reload();
    }
  }
</script>
@endsection
