<style>
  tr {
    display:block;
    float:left;
  }
  th,td{
    padding: 5px;
    border: 1px solid black;
    border-collapse:collapse;
    display:block;
  }
  th {
    width: 180px;
    background-color:#696969;
    color: white;
  }
  td{
    height: 200px;
    width: 180px;
    background-color:#f8f8f8;
  }
  table { empty-cells: show;
    border-collapse:collapse;
  }
</style>
    <br>
    <p hidden>{{
      $materiaId = $_COOKIE['grupoMateriaHorarioCookie']}}
    </p>
    <p hidden>{{
      $gestionId = $_COOKIE['cookieGestionHorarios']}}
    </p>
<p hidden> {{
    $listaHorarios = App\GrupoMateria:: join('sesions','grupo_materias.id','=','sesions.grupoMateria_id') 
     -> where('gestion_id', '=', $gestionId)
     -> join('horarios', 'sesions.horario_id','=','horarios.id') 
     -> where('materia_id', '=', $materiaId)
     -> distinct()
     -> select(['horario_id'])
     -> addSelect('dia')
     -> addSelect('grupo_id')
     -> addSelect('sesions.auxiliar_id') 
     -> addSelect('docente_id')
     -> addSelect('materia_id')
     -> addSelect('hora_entrada')
     -> addSelect('hora_salida')
     ->get()-> groupBy('dia')  
   }}
</p>
@if(count($listaHorarios) == 0)
  <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <ul> No existen Sesiones para esta materia.
        </ul>
    </div>
@else
<table style="width:100%">
<tr>
  <th> Lunes </th>
      @if(!(empty($listaHorarios['Lunes'])))
      <p hidden>{{$lunes = $listaHorarios['Lunes']-> sortBy('hora_entrada')}}</p>
        @foreach ($lunes as $unLunes)
          <!--Lunes-->
        <td><h4>{{$unLunes->materia->nombre_materia}}</h4><br>
        Grupo: {{$unLunes->grupo->numero_grupo}} <br>
        Docente: {{$unLunes->docente->persona->nombre}} <br>
        Auxiliar: {{$unLunes->auxiliar->persona->nombre}}<br>
        Entrada: {{(new \Carbon\Carbon($unLunes->hora_entrada))->toTimeString()}} <br>
        Salida: {{(new \Carbon\Carbon($unLunes->hora_salida))->toTimeString()}} </td></td>
        @endforeach
        @endif
      </tr>

      <tr>
      <th> Martes </th>
      @if(!(empty($listaHorarios['Martes'])))
      <p hidden>{{$martes = $listaHorarios['Martes']-> sortBy('hora_entrada')}}</p>
        @foreach ($martes as $unMartes)
          <!--Martes-->
        <td><h4>{{$unMartes->materia->nombre_materia}}</h4><br>
        Grupo: {{$unMartes->grupo->numero_grupo}} <br>
        Docente: {{$unMartes->docente->persona->nombre}} <br>
        Auxiliar: {{$unMartes->auxiliar->persona->nombre}}<br>
        Entrada: {{(new \Carbon\Carbon($unMartes->hora_entrada))->toTimeString()}} <br>
        Salida: {{(new \Carbon\Carbon($unMartes->hora_salida))->toTimeString()}} </td></td>
        @endforeach
        @endif
      </tr>
      <tr>
         <th>Miercoles</th>
         @if(!(empty($listaHorarios['Miercoles'])))
         <p hidden>{{$miercoles = $listaHorarios['Miercoles']-> sortBy('hora_entrada')}}</p>
        @foreach ($miercoles as $unMiercoles)
          <!--Miercoles-->
        <td> <h4>{{$unMiercoles->materia->nombre_materia}}</h4><br>
        Grupo: {{$unMiercoles->grupo->numero_grupo}} <br>
        Docente: {{$unMiercoles->docente->persona->nombre}} <br>

        Auxiliar: {{$unMiercoles->auxiliar->persona->nombre}}<br>
        Entrada: {{(new \Carbon\Carbon($unMiercoles->hora_entrada))->toTimeString()}} <br>
        Salida: {{(new \Carbon\Carbon($unMiercoles->hora_salida))->toTimeString()}} </td>
        @endforeach
        @endif
      </tr>
      
      <tr>
      <th> Jueves </th>
      @if(!(empty($listaHorarios['Jueves'])))
      <p hidden> {{$listaHorarios['Jueves'] ->first()}}</p>
      <p hidden>{{$jueves = $listaHorarios['Jueves']->sortBy('hora_entrada')}}</p>
        @foreach ($jueves as $unJueves)
          <!--Jueves-->
        <td><h4>{{$unJueves->materia->nombre_materia}}</h4><br>
        Grupo: {{$unJueves->grupo->numero_grupo}} <br>
        Docente: {{$unJueves->docente->persona->nombre}} <br>
        Auxiliar: {{$unJueves->auxiliar->persona->nombre}}<br>
        Entrada: {{(new \Carbon\Carbon($unJueves->hora_entrada))->toTimeString()}} <br>
        Salida: {{(new \Carbon\Carbon($unJueves->hora_salida))->toTimeString()}} </td></td>
        @endforeach
        @endif
      </tr>

      <tr>
      <th> Viernes </th>
      @if(!(empty($listaHorarios['Viernes'])))
      <p hidden>{{$viernes = $listaHorarios['Viernes']-> sortBy('hora_entrada')}}</p>
        @foreach ($viernes as $unViernes)
          <!--Viernes-->
        <td><h4>{{$unViernes->materia->nombre_materia}}</h4><br>
        Grupo: {{$unViernes->grupo->numero_grupo}} <br>
        Docente: {{$unViernes->docente->persona->nombre}} <br>
        Auxiliar: {{$unViernes->auxiliar->persona->nombre}}<br>
        Entrada: {{(new \Carbon\Carbon($unViernes->hora_entrada))->toTimeString()}} <br>
        Salida: {{(new \Carbon\Carbon($unViernes->hora_salida))->toTimeString()}} </td></td>
        @endforeach
        @endif
      </tr>

      <tr>
      <th> Sabado </th>
      @if(!(empty($listaHorarios['Sabado'])))
      <p hidden>{{$sabados = $listaHorarios['Sabado']-> sortBy('hora_entrada')}}</p>
        @foreach ($sabados as $unSabado)
          <!--Sabado-->
        <td><h4>{{$unSabado->materia->nombre_materia}}</h4><br>
        Grupo: {{$unSabado->grupo->numero_grupo}} <br>
        Docente: {{$unSabado->docente->persona->nombre}} <br>
        Auxiliar: {{$unSabado->auxiliar->persona->nombre}}<br>
        Entrada: {{(new \Carbon\Carbon($unSabado->hora_entrada))->toTimeString()}} <br>
        Salida: {{(new \Carbon\Carbon($unSabado->hora_salida))->toTimeString()}} </td></td>
        @endforeach
        @endif
      </tr>
  </table>
@endif
<br>
<br>
<br>
<br>
