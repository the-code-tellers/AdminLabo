<p hidden>
@if(!isset($_COOKIE['grupoMateriaHorarioCookie']))
  {{setcookie('grupoMateriaHorarioCookie', $grupoMaterias[0] -> materia_id)}}
  {{$_COOKIE['grupoMateriaHorarioCookie'] = $grupoMaterias[0] -> materia_id}}
@endif
@if(empty($_COOKIE['cookieGestionHorarios']))
  {{setcookie('cookieGestionHorarios', $gestiones[0] -> id )}}
  {{$_COOKIE['cookieGestionHorarios'] = $gestiones[0] -> id}}
@endif
</p>
<script type="text/javascript">
  function a(nombreCookie, elementIdd, siReload ) {
    document.cookie = nombreCookie + '=' + document.getElementById(elementIdd).value;
    if(siReload) {
      location.reload();
    }
  }
</script>
@include('layouts.mensajesDeErrorInfo')
<form>
    <div class="row">
         <label for="sel1">Seleccione una gestion:</label>
          <select class="form-control" id="gestion" name="gestion"
          onchange="a('cookieGestionHorarios', 'gestion', true)">
          <optgroup label="gestion">
            <option disabled selected value>Elija una gestión</option>
            @foreach ($gestiones as $gestion)
              <option value={{$gestion->id}}
               @if($_COOKIE['cookieGestionHorarios'] == $gestion->id)  selected @endif>
               Gestion:{{ $gestion -> numero_gestion }}-{{ $gestion -> anho}}</option>
            @endforeach                        
          </optgroup>
         </select>

        <p hidden> {{$materiasGestion = App\GrupoMateria::  where('gestion_id', '=', $_COOKIE['cookieGestionHorarios'])
                                  -> groupBy('materia_id')
                                  -> get() }} </p>
        
        <label for="sel1">Seleccione una materia:</label>
        <select name="materia" id="materia" class="form-control" onchange="a('grupoMateriaHorarioCookie', 'materia', true ) " >
            <optgroup label="Materias por nombre">  
            <option disabled selected value> Elija una materia</option>
            @if (!(empty($materiasGestion)))
              @foreach ($materiasGestion as $grupoMateria)
                @if($_COOKIE['grupoMateriaHorarioCookie'] == $grupoMateria-> materia_id)
                  <option value="{{$grupoMateria-> materia_id}}" selected>
                      {{ $grupoMateria->materia-> nombre_materia }}  
                  </option>
                @else
                  <option value="{{$grupoMateria-> materia_id}}">
                      {{ $grupoMateria ->materia ->nombre_materia }} 
                  </option>
                @endif
              @endforeach
            @endif
            </optgroup>
        </select>
    </div>
  <br>
</form>